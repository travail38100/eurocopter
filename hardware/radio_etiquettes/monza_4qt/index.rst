


.. index::
   pair: Tags; Monza 4QT


.. _monza_4qt:
.. _tags_uhf_impinj:

======================================
Tags UHF Impinj Monza 4QT
======================================

.. seealso::

   - :ref:`tags_monza4QT`
   - http://www.impinj.com/Monza_4_RFID_Tag_Chips.aspx


.. contents::
   :depth: 3


Datasheet
=========

:download:`Télécharger la datasheet concernant le tag <IPJ_Monza4Datasheet_20101101.pdf>`.


Les tags (Impinj Monza 4 QT)
============================

.. seealso::

   - http://www.impinj.com/Monza_4_RFID_Tag_Chips.aspx

.. figure:: impinj_4_QT.png
   :align: center


Les étiquettes préconisées sont montées avec des puces Impinj Monza 4 QT.

Ces tags sont constitués de:

- Un code :term:`epc` public sur 96 bits
- :term:`TID` + numéro de série unique
- Un code epc privé sur 128 bits
- Une mémoire utilisateur de 512 bits

Plan mémoires
=============

.. toctree::
   :maxdepth: 3

   plan_memoires



Impinj’s Monza 4 tag chips feature
==================================


- True3D™ antenna technology—two fully independent antenna ports enable
  the creation of tags without blind spots
- QT™ technology, featuring public and private data profiles that support
  confidentiality of business-sensitive data and assure consumers of privacy
- Memory options to support large user-memory:

  -  512 bits with block permalock
  -  or EPC-memory (496 bits) applications

- Gen 2 compliant custom features that facilitate inventory of hard-to-read
  tags and rapid access of serial numbers
- Industry’s best read and write sensitivity combined with excellent
  interference rejection to yield outstanding read and write reliability
- EPCglobal UHF Gen 2 and ISO 18000-6C compliant







