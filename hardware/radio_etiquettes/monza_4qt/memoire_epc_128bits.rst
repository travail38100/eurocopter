

.. index::
   pair: Privée; Mémoire

.. _memoire_privee_128bits:

============================
Mémoire EPC 128 bits privée
============================

.. seealso::

   - :ref:`ReadTag`
   - :ref:`WriteTag`


.. contents::
   :depth: 3

Introduction
============


Pour passer de la mémoire EPC private à ma mémoire EPC publique il faut
une instruction propriétaire qui est ?


.. figure:: memoire_ipc_private.png
   :align: center



Organisation de la mémoire
==========================


::
       LLLLLRRNNNNNNNNN

       LLLLL

           - 5 bits
           - Longueur en word (16 bits) de PC (Protocol Control) + code EPC (Electronic Product Code)
           - LLLLL = 0 => 16 bits (pour PC)
           - LLLLL = 1 => 32 bits   => EPC = 16 bits (2 octets)
           - LLLLL = 2 => 48 bits   => EPC = 32 bits (4 octets)
           - LLLLL = 3 => 64 bits   => EPC = 48 bits (6 octets)
           - LLLLL = 4 => 64 bits   => EPC = 48 bits (6 octets)

           - ...
           - LLLLL = 8 => 144 bits  => EPC = 128 bits (16 octets)
             C'est le code qui nous intéresse


