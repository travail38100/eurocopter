

.. index::
   pair: Tags; Monza4QT


.. _les_tags:

==========================
Les puces RFID (tags)
==========================

.. seealso::

   - :ref:`tags_uhf_impinj`
   - :term:`Puce RFID`


.. contents::
   :depth: 3

.. _tags_monza4QT:

Les tags (Impinj Monza4QT)
============================

.. seealso::

   - http://www.impinj.com/Monza_4_RFID_Tag_Chips.aspx
   - :ref:`api_lecture_ecriture_monza4QT`


Les étiquettes préconisées sont montées avec des puces Impinj Monza4QT.

Ces tags sont constitués de:

- Un code :term:`epc` :ref:`public sur 96 bits <memoire_publique_pot>`
- :term:`TID` + numéro de série unique (sur 48 bits)
- Un :ref:`code epc privé sur 128 bits <memoire_privee_pot>`
- Une :ref:`mémoire utilisateur de 512 bits <memoire_publique_pot>`
- un mot de passe sur 64 bits (nouveau)


Le :ref:`code epc public <memoire_publique_pot>` peut être lu par n’importe quel lecteur,
il ne contient pas d’informations confidentielles.

L’accès au code epc privé et à la mémoire utilisateur est protégé par mot de passe.



.. _profils:

Profils de données privé/public
================================

::

    The Private/Public profile capability, available in Monza 4QT tag chips,
    provides two memory configurations (i.e., profiles) in a single chip—one
    Private and one Public. A Monza 4QT chip only exposes a single profile
    at a time.


Le tag possède 2 types de profil:

- profil privé
- profi public


Le passage du profil privé au profil public se fait au moyen de la commande



Profil privé (private data profile)
===================================

.. figure:: profil_prive.png
   :align: center


Profil public (public data profile)
===================================

At any point in the supply chain, for example at point-of-sale, users have the
ability to switch QT tags to the Public profile.

Figure 2-5 illustrates this profile.

Once switched, the tag conceals its 128 bit EPC (EPC_Private), User Memory,
16 bit TID header, and 48 bit serial number.

The tag exposes its Public EPC in EPC memory, remapped from its prior location
in TID memory. When the tag is singulated, it sends this 96 bit public EPC.
The only other information available to a reader is the 32 bit base TID.

All other private memory contents appear non-existent to a reader reading the tag.


.. figure:: profil_public.png
   :align: center

   Figure 2-5


La commande QT
==============

.. toctree::
   :maxdepth: 3

   commandeQT/index



