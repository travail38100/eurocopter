



.. _commande_QT_bas_niveau:

==============================================================
Commande QT bas niveau (pour basculer entre private et public)
==============================================================

.. contents::
   :depth: 3

Introduction
============


Pour passer d'une organisation mémoire à l'autre, il est nécessaire de passer
une commande propriétaire dont le format est le suivant:


.. figure:: commande_QT.png
   :align: center


The Private/Public profile features of the Monza 4QT tag chip are controlled by
the QT command. Tags may be switched from Private profile to Public profile and
back again, using the QT command.

This QT command can be protected by a Short-Range Feature, by the tag’s access
password, or by both.

Le bit 14 QT_MEM
================

L'information qui nous intéresse est le bit 14 QT_MEM:


- QT_MEM == 1 =>  Tag uses Public Memory
- QT_MEM == 0 =>  Tag uses Private Memory


.. figure:: details_commande_QT.png
   :align: center


Private Memory Monza 4QT
=========================

Les mémoires en mode privé.

.. figure:: memories_private_mode_2.9.png
   :align: center


Public Memory Monza 4QT
=========================

Les mémoires en mode public.

.. figure:: memories_public_mode_2.10.png
   :align: center


Extended User Memory Option
============================

Impinj offers a version of Monza 4 (Monza 4QT) with 512 bits of user memory,
128 bits of EPC memory, and a serialized TID.

The extended User memory option supports
applications where users cannot count on a database connection.

The 512 bits of User memory enables a portable, but private database to travel
with the tag.


.. figure:: monza_memory_options_2.8.png
   :align: center



Réponse du Tag
==============

.. figure:: tag_response_2.5.png
   :align: center


The tag response to the QT Command with Read/Write =1 uses the extended preamble.

Note that a reader should not presume that a tag has properly executed a
QT Write command unless and until it receives the response shown in Table 2-5
from the tag.


Public/Private Profile Protection
=================================

Short-Range feature (QT_SR, bit 15)
-----------------------------------

To secure the Private profile tag data, Monza 4QT chips offer a Short-Range
feature.

The Short-Range feature adds a layer of physical security by preventing readers
farther than roughly one meter from the tag from switching the tag from Public
to Private (or vice versa).

When Short Range is enabled, the tag reduces its sensitivity in the OPEN and
SECURED states by about 15 dB.

The tag has normal sensitivity during singulation.

However, before transitioning to the OPEN or SECURED states, the tag checks the
RF power level—if it is above the short-range threshold then the tag will enter
the OPEN or SECURED state, otherwise the tag will reset back to the READY state.

The QT command is only available when a tag is in the SECURED state, so this
power check effectively prevents the tag from accepting a QT command at long
range.


The Short Range feature is controlled by the QT command.

The specific bit that controls the Short Range mode is the QT_SR bit.

Peek feature (Persistence, bit 19)
----------------------------------


What would happen if a Public tag is switched to Private by an authorized user,
for example to read User memory, and inadvertently left in the Private mode?

In this situation, the tag could compromise its Private data.

To help prevent this situation, Monza 4QT tag chips offer a Peek feature.

With Peek, a reader can temporarily switch a Public tag to Private, access the
Private information, then when the chip loses power it will automatically revert
to its Public profile.

Peek is controlled by the persistence bit in the QT command—to implement a Peek,
**set the Persistence bit to 0 in the QT command**.






