

.. index::
   pair: API ; Bascule privé/public
   pair: Monza4QTRead; LireTagEtatMemoire

.. _api_commande_QT:

================================================================================
API commande QT pour basculer de private à public (Monza4QTRead, Monza4QTWrite)
================================================================================


.. contents::
   :depth: 3


Introduction
============

The Private/Public profile capability, available in Monza 4QT tag chips, provides
two memory configurations (i.e., profiles) in a single chip—one Private and
one Public.

At any point in the supply chain, for example at point-of-sale, users have the
ability to switch QT tags to the Public profile.

Once switched, the tag conceals its 128 bit EPC (EPC_Private), User Memory,
16 bit TID header, and 48 bit serial number.
The tag exposes its Public EPC in EPC memory, remapped from its prior location
in TID memory. When the tag is singulated, it sends this 96 bit public EPC.
The only other information available to a reader is the 32 bit base TID.
All other private memory contents appear non-existent to a reader reading the tag


Lecture de l'état de la mémoire (Monza4QTRead)
==============================================

.. seealso::

   - :ref:`api_lecture_ecriture_monza4QT`


LireEtatMemoire
----------------

::

    /// <summary>
    /// Lecture de l'état mémoire.
    /// </summary>
    private void LireEtatMemoire()
    {
        string message = "";
        try
        {
            bool pubmem = true;
            CTagMonza4QT.LireTagEtatMemoire(LecteurNordic, ref pubmem, this);
            if (pubmem)
            {
                message = string.Format("La mémoire est publique (.");
            }
            else
            {
                message = string.Format("La mémoire est privée.");
            }
            MessageBox.Show(message);
        }
        catch (Exception ex)
        {
            message = string.Format("Exception:{0}", ex.Message);
            MessageBox.Show(message);
        }
    }



LireTagEtatMemoire
------------------

.. seealso::

   - :ref:`Monza4QTRead`


::


       /// <summary>
        /// Lecture de l'état mémoire du tag
        /// </summary>
        static public void LireTagEtatMemoire(NurApi lecteurNordic, ref bool pubmem, RFIDEncodagePabanForm form)
        {
            try
            {
                byte[] byMask = new byte[1];

                byMask[0] = 0x00;

                // tag.ReadTag => Lecture de la mémoire EPC 16 octets (128 bits)
                // ==============================================================
                bool reduce = false;
                lecteurNordic.Monza4QTRead(0 // passwd
                                          , ref reduce
                                          , ref pubmem
                                          , NurApi.BANK_EPC // sBank
                                          , 0 // sAddress
                                          , 1  // sMaskBitLength
                                          , byMask);  //byte[] sMask);

            }
            catch (Exception ex)
            {
                string message = string.Format("LireTagEtatMemoire() Monza4QTRead() exception:{0}", ex.Message);

                // Rethrow the inner exception
                throw new Exception(message, ex);
            }

        } // static private byte[] LireTagEtatMemoire()



