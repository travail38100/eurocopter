
.. index::
   pair: BARTEC; Lecteur ATEX
   pair: BARTEC; Documentation
   pair: BARTEC; MOTOROLA MC9190G
   pair: BARTEC; MC9190ex-NI
   pair: BARTEC; TPP-MC9190-UHF-Z2-22


.. _lecteur_bartec_9190:

======================================================================================
Lecteur BARTEC MC9190ex-NI (alias MOTOROLA MC9190G alias Tectus TPP-MC9190-UHF-Z2-22)
======================================================================================


.. seealso::

   - :ref:`lecteur_bartec_9200`

.. contents::
   :depth: 3



Présentation générale
=====================

.. figure:: lecteur_bartec.png
   :align: center
   

Ce PDA est un modèle spécial RFId-UHF-ATEX.
Il est constitué d’une base Motorola modifiée par Bartec.
La société Bartec a intégré le lecteur RFId et a passé l’agrément ATEX 
avec les protections de conception produit catégorie 3 pour les gaz et 
avec protection par enveloppe pour les poussières :

- II 3G Ex nA IIC T6
- II 3D Ex tD A22 IP64 T80°C

Quand le PDA n’est pas utilisé il est conseillé de le replacer dans son 
socle afin d’assurer le rechargement des accumulateurs.

Le socle de rechargement est interdit en zone ATEX.

Les accumulateurs sont du type Lithium ions 7.4V/2200mAh, la durée de la 
charge est inférieure à 4h. 

Le socle permet également le transfert des informations du PC vers le 
PDA et du PDA vers PC.



Documentation sur le site BARTEC
================================

.. seealso::

   - http://www.bartec.de
   - http://www.bartec.de/automation-download/mobileE.htm#MC9190ex
   - http://www.bartec.de/homepage/eng/20_produkte/130_automatisierung/s_20_130_100_51.asp?ProdID=880
   - http://www.bartec.de/ProdCatalogue/Assets/Datasheets/lng_2/321302_1F.pdf


.. figure:: pageweb_bartec.png
   :align: center



Documentation sur le site de Motorola
=====================================


.. seealso::

   - http://support.symbol.com
   - http://support.symbol.com/support/browse.do?WidgetName=BROWSE_PRODUCT&TaxoName=SG_SupportGoals&BROWSE_PRODUCT.isProductTaxonomy=true&BROWSE_PRODUCT.NodeId=SG_MC9190_G_1_2&BROWSE_PRODUCT.thisPageUrl=%2Fproduct%2Fproducts.do&id=m4&BROWSE_PRODUCT.TaxoName=SG_SupportGoals&NodeType=leaf&NodeName=MC9190-G&document=DT_SOFTWARE_1_1&BROWSE_PRODUCT.NodeType=leaf&NodeId=SG_MC9190_G_1_2&AppContext=AC_ProductPage&param_document=sp
   
   
.. figure:: pageweb_motorola_bartec_9190ex_ni.png
   :align: center


Infos Tectus  
=============

.. seealso::

   -  http://www.tec-tus.de/en/reader/atex-zone-2-22-certified-ex-industrial-pda-uhf-rfid-reader-tpp-mc9190-ex-uhf-z2-22

.. toctree::
   :maxdepth: 5
   
   
   tectus/index



Divers 
=============

.. toctree::
   :maxdepth: 5
      
   dev_tools/index
   official_doc/index
   RFID_demo/index
   software_versions/index
   tests/index
   tools/index
   
   
