
.. index::
   pair: EMDK; Motorola


.. _motorola_emdk_2.8:

===================
Motorola EMDK 2.8
===================

.. contents::
   :depth: 3

Adresse de téléchargement
==========================

.. seealso::

   - http://support.symbol.com/support/search.do?cmd=displayKC&docType=kc&externalId=14015&sliceId=&dialogID=553972043&stateId=1%200%20553966406
   

.. figure:: emdk_2.8.png
   :align: center
   

Description
===========


- New device support for MC92N0 CE7.0 and MC92N0 WM6.5
- **New support for Windows 8 PC installation**. 
  EMDK for .NET can now be installed on:
  
  * Windows XP, 
  * Windows Vista, 
  * Windows 7 
  * and Windows 8.
  
  
- Updated SensorSample1 to provide access to all the sensors available on the 
  device. 
  The previous versions provided access to the accelerometer, tilt angle and 
  orientation sensors only.
  
  
Installation du EMDK
====================

.. figure:: install_emdk_2.8.png
   :align: center
   
   

  
