
.. index::
   pair: Motorola; EMDK
   pair: Enterprise Mobility Developer Kit; EMDK


.. _motoroal_emdk:

==================================================
Motorola EMDK (Enterprise Mobility Developer Kit)
==================================================

.. toctree::
   :maxdepth: 4
   
   2.8/index
   
   
