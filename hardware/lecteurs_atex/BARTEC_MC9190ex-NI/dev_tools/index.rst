

.. index::
   pair: Development; Tools

.. _uhf_reader_development_tools:

======================================
Development tools for the UHF reader
======================================

.. toctree::
   :maxdepth: 3
   
   emdk/index
   windows_mobile_SDK/index
   
   
   
   
