
.. index::
   pair: Windows Mobile; SDK


.. _windows_mobile_sdk:

====================
Windows Mobile SDK
====================


.. seealso::

   - http://msdn.microsoft.com/fr-fr/windowsmobile/
   - http://www.microsoft.com/en-us/download/details.aspx?id=17284
 

.. figure:: page_windows_sdk.png
   :align: center
      
   
.. contents::
   :depth: 3
   
      
Introduction
============

La plateforme Windows Mobile permet aux développeurs de réaliser en toute 
simplicité des applications à destination de périphériques mobiles et d'offrir 
une expérience riche à plusieurs dizaines de millions d'utilisateurs. 

Retrouvez tous nos articles techniques, tutoriels vidéo, ou encore 
téléchargements utiles pour réaliser votre première application Windows Mobile 
ou améliorer vos applications existantes.


Development documentation
=========================

.. seealso::  

   - http://fr.wikipedia.org/wiki/Windows_Mobile#D.C3.A9veloppement
   
Les applications natives pour Windows Mobile sont réalisées à partir de la suite 
Visual Studio auquel on ajoute un Software Developpement Kit (SDK) spécifique 
à la version de Windows Mobile ciblée.



Tutorial for the development
============================

.. seealso::

   - http://msdn.microsoft.com/en-us/library/dd721907.aspx
   
   
Getting Started with Building Windows Mobile Solutions with Visual Studio and 
Windows Mobile 6 SDK 


   
      
