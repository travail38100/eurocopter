
.. index::
   pair: Demo; RFID (BARTEC)


.. _demo_RFID_bartec:

==========================================================
La demonstration RFID pour le lecteur BARTEC MC9190ex-NI
==========================================================


.. seealso::

   - http://www.bartec-group.com/
   - http://www.bartec.de/automation-download/mobileE.htm#MC9190ex


.. figure:: pageweb_bartec_demo.png
   :align: center
      

.. contents::
   :depth: 3
   
   
README
======

.. seealso::

   - http://automation.bartec.de/DataRoot/mobile/MC9190ex-NI/RFID/readme_eng.pdf

The whole RFID SDK (Software Development Kit) for the MC9190ex RFID series is 
available as download_.

- Demo in Open Source
- Whole DLL and Drivers
- Manual for the use of the Demo Application
- DLL description

All documents and files are available as free download. 

All source and firmware files are available in open source.


.. _download:  http://www.bartec.de/automation-download/mobileE.htm#MC9190ex
