
.. index::
   pair: BARTEC; Tests
   pair: Deploy; Windows Mobile
   
   

.. _bartec_test:

===========================================================================
Tests du lecteur BARTEC MC9190ex-NI (Windows Mobile 6 Professional Device)
===========================================================================


.. figure:: deploy_on_windows_mobile.png
   :align: center

.. contents::
   :depth: 3
   
   
.. toctree::
   :maxdepth: 3
   
   cs_audio/index
   
   
