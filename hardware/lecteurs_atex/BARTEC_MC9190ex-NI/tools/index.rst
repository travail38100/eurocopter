
.. index::
   pair: BARTEC; Synchronisation
   
   

.. _bartec_tool:

============================================
Outils pour lecteur BARTEC MC9190ex-NI
============================================


.. contents::
   :depth: 3
   
   
Outil de communication entre le Bartec et le PC
===============================================


.. seealso::

   - :ref:`installation_synchro_eurocopter`
   
