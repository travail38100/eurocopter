
.. index::
   pair: TECTUS; TPP-MC9190-UHF-Z2-22
   pair: TECTUS; BARTEC*
   pair: Windows ; Embedded Handheld 6.5
   pair: Windows ; Mobile 6.5
   pair: ARM; XScale
   pair: ARM; v5
   pair: PXA320; ARM
   

.. _lecteur_bartec_9190_tectus:

===============================================
Informations TECTUS  (TPP-MC9190-UHF-Z2-22)
===============================================


.. seealso::

   - http://www.tec-tus.de/en/reader/atex-zone-2-22-certified-ex-industrial-pda-uhf-rfid-reader-tpp-mc9190-ex-uhf-z2-22



.. contents::
   :depth: 3


.. figure:: lecteur_atex_tectus.png
   :align: center


Contact
=======

Udo dodge
---------

Udo Doege de Tectus:  u.doege@tec-tus.de

Description
===========

The ATEX Zone 2 / 22 certified EX Industrial PDA with UHF RFID Reader 
TPP-MC9190-EX-UHF-Z2-22 is a robust device for reliably scanning barcodes and 
reading ATEX certified UHF RFID tags in hazardous areas. 

The scan trigger is positioned in such a way that barcodes and RFID tags can be 
captured very conveniently. 

The integrated radio module enables real time data access to your host system. 

The TPP-MC9190-EX-UHF-Z2-22 combines the strength of Microsoft‘s Pocket PC 
platform with the power of the Intel®XScaleTM PXA320 processor with 806 MHz. 

A further highlight is the large easy-to-read 1/4 VGA color display with 
touchscreen technology. 

RFID system integrators and users use the ATEX / IECEx Zone 2 / 21 certified 
EX Industrial UHF PDA in applications like mobile maintenance, the inventory 
registering, logistics and further applications where TECTUS ATEX certified 
UHF RFID TAGs help to improve processes and to achieve cost savings.


Specifications
==============

Integrated RFID Reader
---------------------- 

- Nominal read/write range UHF external module: up to approx. 30 - 50 cm 
- UHF external module and external antenna: up to approx. 1.5 m (approx. 59 inch) 
  with EPC Gen 2 Tag with Dog Bone Tag Antenna (UPM Raflatac)
- Antenna integrated in the module or external on the module

Frequency range
++++++++++++++++

- 865.6 up to 867.5 MHz (Europe)
- 902.0 up to 928.0 MHz (US
   
UHF reader version  (50/ID ISCUM02/PRHD102)
+++++++++++++++++++++++++++++++++++++++++++

- 50/ID ISCUM02/PRHD102


CPU XSCALE ARM PXA320 Processor
===============================

.. seealso::

   - http://en.wikipedia.org/wiki/Marvell_Technology_Group
   - http://www.marvell.com/application-processors/pxa-family/

- Marvell PXA320 Processor/806 MHz


PXA3xx
------

In August 2005 Intel announced the successor to Bulverde, codenamed Monahans.

They demonstrated it showing its capability to play back high definition encoded 
video on a PDA screen.

The new processor was shown clocked at 1.25 GHz but Intel said it only offered 
a 25% increase in performance (800 MIPS for the 624 MHz PXA270 processor vs. 
1000 MIPS for 1.25 GHz Monahans). 

An announced successor to the 2700G graphics processor, code named Stanwood, has 
since been canceled. Some of the features of Stanwood are integrated into Monahans. 
For extra graphics capabilities, Intel recommends third-party chips like the 
NVIDIA GoForce chip family.

In November 2006, Marvell Semiconductor officially introduced the Monahans 
family as Marvell **PXA320**, PXA300, and PXA310.

PXA320 is currently shipping in high volume, and is scalable up to 806 MHz. 

PXA300 and PXA310 deliver performance "scalable to 624 MHz", and are 
software-compatible with PXA320.




XSCALE family
-------------

.. seealso::

   - http://en.wikipedia.org/wiki/XScale

The XScale, a microprocessor core, is originally a Intel's implementation of 
the **ARMv5** architecture, and consists of several distinct families: IXP, IXC, IOP, 
**PXA** and CE (see more below). 

Intel sold the PXA family to Marvell Technology Group in June 2006. 
And Marvell then extended the brand to include processors with other 
microarchitecture like Cortex microarchitecture.

The XScale architecture is based on the ARMv5TE ISA without the floating point 
instructions. XScale uses a seven-stage integer and an eight-stage memory 
superpipelined microarchitecture. It is the successor to the Intel StrongARM 
line of microprocessors and microcontrollers, which Intel acquired from DEC's 
Digital Semiconductor division as part of a settlement of a lawsuit between the 
two companies. Intel used the StrongARM to replace its ailing line of outdated 
RISC processors, the i860 and i960.

All the generations of XScale are 32-bit ARMv5TE processors manufactured with 
a 0.18 µm or 0.13 µm (as in IXP43x parts) process and have a 32 kB data cache 
and a 32 kB instruction cache. First and second generation XScale cores also 
have a 2 kB mini-data cache. Products based on the 3rd generation XScale 
have up to 512 kB unified L2 cache.


Operation System Windows Embedded Handheld -> Windows Mobile 6.5 
====================================================================

.. seealso::

   - http://fr.wikipedia.org/wiki/Windows_Mobile#Windows_Mobile_6.5
   - http://en.wikipedia.org/wiki/Windows_Embedded#Windows_Embedded_Handheld_6.5

- Windows Mobile 6.5

.. warning:: The real OS is Windows Embedded Handheld 6.5

Lien avec Windows CE
-------------------------

.. seealso::

   - http://fr.wikipedia.org/wiki/Windows_CE#Lien_avec_Windows_Mobile

   
L'offre Windows Mobile repose sur une version de Windows CE agrémentée d'un 
ensemble de fonctionnalités. 

De plus, l'architecture matérielle pouvant faire fonctionner une version 
Windows Mobile est déterminée par Microsoft. 
Ceci signifie que l'offre Windows Mobile peut être vue comme la fourniture d'un 
OS avec un ensemble de fonctionnalités mais aussi comme la spécification d'une 
architecture matérielle. 

**Ainsi l'offre Mobile a toujours fonctionné sur processeur ARM jusqu'à présent**.

En 1999 sort sur le marché le premier Pocket PC. 
Cet appareil est basé sur un OS Windows CE 3.0. Cette sortie marque le début de 
l'expansion de l'OS Windows CE via l'offre Mobile avec pour chaque génération 
de l'offre Windows Mobile une version de Windows CE sous-jacente :

- SmartPhone 2002 (Windows Mobile 2002) : Windows CE 3.0
- Windows Mobile 2003 : Windows CE 4.20
- Windows Mobile 2003 SE : Windows CE 4.21
- Windows Mobile 5.0 : Windows CE 5.1
- Windows Mobile 6.0 : Windows CE 5.2
- Windows Mobile 6.1 : Windows CE 5.2

Les versions Windows Mobile 5.0 et 6.x reposent donc sur une version dérivée de 
Windows CE 5.0. 

Les versions 5.1 et 5.2 sont réservées à l'offre Windows Mobile. 






