

.. _doc_lecteur_bartec_9190:

=========================================================
Documentation officielle "BARTEC" du lecteur MC9190ex-NI
=========================================================

.. seealso::

   - http://www.bartec-group.com/
   - http://www.bartec.de/automation-download/mobileE.htm#MC9190ex


.. contents::
   :depth: 3

Introduction
=============

The documentation set for the MC9190-G is divided into guides that provide 
information for specific user needs.


2 MC9190-G user guides
=======================

.. seealso::

   - http://automation.bartec.de/DataRoot/mobile/MC9190ex-NI/manuals/E_REV2_B1-A280-7D0001_User_Manual_MC9190ex.pdf
   - http://automation.bartec.de/DataRoot/mobile/MC9190ex-NI/manuals/motorola/E_User_Guide_REVA_Jan2011.pdf


- :download:`Visualiser E_REV2_B1-A280-7D0001_User_Manual_MC9190ex.pdf <E_REV2_B1-A280-7D0001_User_Manual_MC9190ex.pdf>`
- :download:`Visualiser E_User_Guide_REVA_Jan2011.pdf <E_User_Guide_REVA_Jan2011.pdf>`


MC9190-G Integrator Guide
=========================

.. seealso::

   - http://automation.bartec.de/DataRoot/mobile/MC9190ex-NI/manuals/motorola/E_Integrator_Guide_REVA_Feb2011.pdf

Describes how to set up the MC9190-G and the accessories.

- :download:`Visualiser E_User_Guide_REVA_Jan2011.pdf <E_Integrator_Guide_REVA_Feb2011.pdf>`




   
