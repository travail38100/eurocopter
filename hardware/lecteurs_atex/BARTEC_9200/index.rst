
.. index::
   pair: BARTEC; Lecteur ATEX
   pair: BARTEC; MOTOROLA MC9200


.. _lecteur_bartec_9200:

======================================================================================
Lecteur BARTEC MC9200 
======================================================================================


.. seealso::

   - :ref:`lecteur_bartec_9190`
   

.. figure:: BARTEC_9200.jpg
   :align: center
   

.. contents::
   :depth: 3



Documentation sur le site BARTEC
================================

.. seealso::

   - http://www.bartec.de
   - http://www.bartec.de/homepage/eng/20_produkte/130_automatisierung/s_20_130_20.asp?NID=169


New MC92N0ex Enhanced for Worker Productivity


Because manufacturing and warehouse customers are under increased pressure to 
create lean, real-time operations to drive productivity up and errors down, 
they need to equip work teams with robust and reliable wireless Barcode and 
RFID scanning capabilities that make quality control simple, fast and extremely 
accurate.

A powerful extension to the successful MC9090ex series of rugged handhelds for 
scan and key intensive applications, the MC92N0ex is based around the latest 
mobility platform architecture and the well known gun and brick form factors. 

This preserves customers’ existing application investments by enabling easy 
and cost-effective porting of applications from other mobile computers.

In addition to a faster processor, memory configurations and included 
Windows CE7 and Windows Embedded Handheld (WEHH 6.5.3) operating system 
compatibility options for enhanced operational excellence have been increased.

The MC92N0ex not only increases worker productivity but seamlessly integrates 
with all existing infrastructure and is backward compatible with existing 
MC9090ex accessories and peripherals.


Processeur
----------

:Processor: TI Cortex-A9 OMAP4430
:Memory: 312.19 MB




Divers 
=============

.. toctree::
   :maxdepth: 5
      
   tests/index
   
   
