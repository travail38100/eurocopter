


.. index::
   pair: Lecteur; Nordic


.. _demo_lecteur_nordic:

==========================================
Démonstration du lecteur Nordic
==========================================



.. contents::
   :depth: 3


Installation
============


NORDIC ID RFID DEMO SOFTWARE DISCLAIMER

You are about to use Nordic ID RFID Demo Software ("Software").
It is explicitly stated that Nordic ID does not give any kind of warranties,
expressed or implied, for this Software.

Software is provided "as is" and with all faults.

Under no circumstances is Nordic ID liable for any direct, special, incidental
or indirect damages or for any economic consequential damages to you or to any
third party.

The use of this software indicates your complete and unconditional understanding
of the terms of this disclaimer.









