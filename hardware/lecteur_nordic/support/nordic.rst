

.. index::
   pair: Support; Nordic
   pair: Nicolas ; Haran



.. _support_nordic:

=======================================
Support Nordic pour le développement
=======================================


.. contents::
   :depth: 3



Envoi à Patrick (15 Jan 2013 08:51:36)
======================================

::

    Sujet:  Fwd: RE: eurocopter
    Date :  Tue, 15 Jan 2013 08:51:36 +0100
    De :    Frederic GAUTIER <frederic.gautier@id3.eu>
    Répondre à :    frederic.gautier@id3.eu
    Organisation :  id3 Semiconductors
    Pour :  VERGAIN Patrick <patrick.vergain@id3.eu>



Réponse de marc (14 Jan 2013 17:17:28)
======================================

::

    Sujet:  RE: eurocopter
    Date :  Mon, 14 Jan 2013 17:17:28 +0100
    De :    Marc Lavorel <marc.lavorel@orcanthus.com>
    Pour :  <frederic.gautier@id3.eu>


Salut,

Je peux te mettre en relation avec mon contact (c’est un français mais il
est basé à la maison mère en Finlande)

Nicolas.haran@nordicid.com

A+

Marc


Courriel de Frédéric à Marc (lundi 14 janvier 2013 17:05)
=========================================================

::


    De : Frederic GAUTIER [mailto:frederic.gautier@id3.eu]
    Envoyé : lundi 14 janvier 2013 17:05
    À : Marc LAVOREL
    Objet : eurocopter



Hello,

Au sujet des problèmes de verrouillage de la mémoire epc par le lecteur
Sampo;

As-tu un contact technique chez Nordic a qui on peut poser la question, si
oui tu peux me transmettre son adresse mail ?

FG





