

.. index::
   pair: API ; StartInventoryStream

.. _StartInventoryStream:

======================================================================
StartInventoryStream
======================================================================


.. toctree::
   :maxdepth: 3


Introduction
============

In inventory stream, NUR module continuously reading tags and provides reading
result to TagStorage.

Refer to “InventoryStream_VB” and “InventoryStream_CSharp” sample applications for details.


NurApiDotNet.NurApi.StartInventoryStream()
===========================================

::

    /// <summary>
    /// Le lancement/arrêt de l'inventaire.
    /// </summary>
    private void btn_inventaire_Click(object sender, EventArgs e)
    {
        // This button toggles inventory stream on/off
        try
        {
            if (!UHFNordic.IsConnected())
            {
                MessageBox.Show("Le lecteur Nordic n'est pas connecté au PC.", "Error");
                return;
            }

            if (!inventory_stream_running)
            {
                //Start
                UHFNordic.ClearTags();
                UHFNordic.StartInventoryStream();
                btn_inventaire.Text = "Arreter l'inventaire des tags"; ;
                inventory_stream_running = true;
            }
            else
            {
                //Stop
                UHFNordic.StopInventoryStream();
                btn_inventaire.Text = "Lancer l'inventaire des tags";
                inventory_stream_running = false;
            }
        }
        catch (Exception ex)
        {
            MessageBox.Show(ex.ToString(), "Error");
            inventory_stream_running = false;
            btn_inventaire.Text = "lancer_inventaire";
        }
    }



