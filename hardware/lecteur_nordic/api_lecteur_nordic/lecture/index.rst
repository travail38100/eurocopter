

.. index::
   pair: API ; ReadTag
   pair: API ; GetEpcString
   pair: API ; BANK_USER

.. _lecture_api_nordic:

================================
Lecture des tags
================================

.. seealso::

   - :ref:`Tag`
   - :ref:`BinToHexString`

.. contents::
   :depth: 5


ClearTags
=========

- public void ClearTags()

FetchTags
=========

- public :ref:`NurApi.TagStorage <TagStorage>` FetchTags(bool includeMeta)


FetchTags() Parameter
----------------------

includeMeta
++++++++++++

Ff true, meta data of tag included.


::Type: Boolean


Tag.GetEpcString
================

Get EPC as formatted hex string.


- public string GetEpcString()


Return Value
------------

EPC code as Hex string.


.. _ReadTag:

Tag.ReadTag
===========

Read Tag's memory.

::

    public byte[] ReadTag(uint passwd
                        , bool secured
                         , byte bank
                         , uint address
                         , int byteCount);


ReadTag()  Parameters
---------------------

passwd
++++++

Password.

:Type: UInt32


secured
+++++++

Set true if secured tag


:Type: Boolean

rdBank
++++++

Memory bank to read:

- 0=BANK_PASSWD
- 1=BANK_EPC
- 2=BANK_TID
- 3=BANK_USER


:Type: Byte


rdAddress
++++++++++

Word address for read operation.

:Type: UInt32

rdByteCount
+++++++++++

Number of bytes to read. This must divisible by two.


:Type: Int32


ReadTag() return Value
----------------------

Array of bytes from tag memory.


.. _SimpleInventory:

SimpleInventory
=================


- public :ref:`NurApi.InventoryResponse <InventoryResponse>` SimpleInventory()


Return Value
------------

Inventory result data as NurApi.InventoryResponse


Code source
===========

Fichier MainForm.cs Fonction btnLireMemoireUtilisateur512bits_Click()
----------------------------------------------------------------------

.. literalinclude: ../../../../../../applications/paban/PC_EncodagePaban/MainForm.cs
   :language: c
   :linenos:
   :lines: 481-495

Fichier memory/user_memory_512bits.cs LireMemoireUtilisateur512bits
--------------------------------------------------------------------

::

    .. literalinclude: ../../../../../../applications/paban/PC_EncodagePaban/memory/user_memory_512bits.cs
       :language: c
       :linenos:
       :lines: 427-459
       :encoding: UTF-8


ReadSingulatedTag
====================


.. toctree::
   :maxdepth: 3

   ReadSingulatedTag


StartInventoryStream
====================


.. toctree::
   :maxdepth: 3

   StartInventoryStream




