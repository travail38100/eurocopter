#include <NurApi.h>

#define USE_USB_AUTO_CONNECT 1

// Print error string and free API object
int ShowErrorAndExit(HANDLE hApi, int error)
{
    TCHAR errorMsg[128+1]; // 128 + NULL

    NurApiGetErrorMessage(error, errorMsg, 128);

    _tprintf(_T("NurApi error %d: [%s]\r\n"), error, errorMsg);

    // Free API object
    NurApiFree(hApi);

    return 1;
}

// Convert EPC byte array to string
void EpcToString(BYTE *epc, DWORD epcLen, TCHAR *epcStr)
{
    DWORD n;
    int pos = 0;
    for (n=0; n<epcLen; n++) {
        pos += _stprintf_s(&epcStr[pos], NUR_MAX_EPC_LENGTH-pos, _T("%02x"), epc[n]);
    }
    epcStr[pos] = 0;
}

// Perform inventory
int PerformInventory(HANDLE hApi)
{
    int rounds = 3;
    int error = 0;
    int tagsFound = 0, tagsMem = 0;
    int tagCount = 0;
    int idx = 0;
    HANDLE hStorage = NULL;
    HANDLE hTag = NULL;
    struct NUR_INVENTORY_RESPONSE invResp;
    struct NUR_TAG_DATA tagData;
    TCHAR epcStr[128];

    // Clear previously inventoried tags from memory
    error = NurApiClearTags(hApi);
    if (error != NUR_NO_ERROR)
    {
        // Failed
        return error;
    }

    _tprintf(_T("Perform inventory ."));

    while (rounds-- > 0)
    {
        _tprintf(_T("."));

        // Perform inventory with 500ms timeout, Q 3, session 0
        //error = NurApiInventory(hApi, 500, 3, 0, &invResp);

        // Perform simple inventory
        error = NurApiSimpleInventory(hApi, &invResp);
        if (error != NUR_NO_ERROR && error != NUR_ERROR_NO_TAG)
        {
            // Failed
            return error;
        }
    }

    _tprintf(_T(". %d tags found\r\n"), invResp.numTagsMem);

    // Fetch tags from module, including tag meta
    error = NurApiFetchTags(hApi, TRUE, NULL);
    if (error != NUR_NO_ERROR)
    {
        // Failed
        return error;
    }

    error = NurApiGetTagCount(hApi, &tagCount);
    if (error != NUR_NO_ERROR)
    {
        // Failed
        return error;
    }

    // Loop through tags
    for (idx = 0; idx < tagCount; idx++)
    {
        error = NurApiGetTagData(hApi, idx, &tagData);

        if (error == NUR_NO_ERROR)
        {
            EpcToString(tagData.epc, tagData.epcLen, epcStr);

            // Print tag info
            _tprintf(_T("Tag info:\r\n"));
            _tprintf(_T("  EPC: [%s]\r\n"), epcStr);
            _tprintf(_T("  EPC length: %d\r\n"), tagData.epcLen);
            _tprintf(_T("  RSSI: %d\r\n"), tagData.rssi);
            _tprintf(_T("  Timestamp: %u\r\n"), tagData.timestamp);
            _tprintf(_T("  Frequency: %u\r\n"), tagData.freq);
            _tprintf(_T("  PC bytes: %04x\r\n"), tagData.pc);
        }
        else
        {
            // Print error
            TCHAR errorMsg[128+1]; // 128 + NULL
            NurApiGetErrorMessage(error, errorMsg, 128);
            _tprintf(_T("NurApiTSGetTagData() returns error %d: [%s]\r\n"), error, errorMsg);
        }
    }

    return NUR_NO_ERROR;
}

int PrintModuleVersion(HANDLE hApi)
{
    struct NUR_READERINFO info;
    int error;

    error = NurApiGetReaderInfo(hApi, &info, sizeof(info));
    if (error == NUR_NO_ERROR)
    {
        _tprintf(_T("Module Version: [%d.%d-%c]\r\n\r\n"), info.swVerMajor, info.swVerMinor, info.devBuild);
    }
    return error;
}

void NURAPICALLBACK MyNotificationFunc(HANDLE hApi, DWORD timestamp, int type, LPVOID data, int dataLen)
{
    switch (type)
    {
    case NUR_NOTIFICATION_LOG:
        {
            const TCHAR *logMsg = (const TCHAR *)data;
            _tprintf(_T("LOG: %s\r\n"), logMsg);
        }
        break;

    case NUR_NOTIFICATION_MODULEBOOT:
        _tprintf(_T("Module booted\r\n"));
        break;

    case NUR_NOTIFICATION_TRCONNECTED:
        _tprintf(_T("Transport connected\r\n"));
        break;

    case NUR_NOTIFICATION_TRDISCONNECTED:
        _tprintf(_T("Transport disconnected\r\n"));
        break;

    default:
        _tprintf(_T("Unhandled notification: %d\r\n"), type);
        break;
    }
}

int _tmain(int argc, _TCHAR* argv[])
{
    int error = 0;

    HANDLE hApi = INVALID_HANDLE_VALUE;

    // Create new API object
    hApi = NurApiCreate();
    if (hApi == INVALID_HANDLE_VALUE)
    {
        return 1;
    }

    // Set notification callback
    NurApiSetNotificationCallback(hApi, MyNotificationFunc);

    // Set full log level, w/o data
    // NurApiSetLogLevel(hApi, NUR_LOG_ALL & ~NUR_LOG_DATA);

    NurApiSetLogLevel(hApi, NUR_LOG_ERROR);

#ifdef USE_USB_AUTO_CONNECT
    NurApiSetUsbAutoConnect(hApi, TRUE);
    error = NurApiIsConnected(hApi);
#else
    // Connect API to module through serial port
    error = NurApiConnectSerialPort(hApi, 10, NUR_DEFAULT_BAUDRATE);
#endif
    if (error != NUR_NO_ERROR)
    {
        // Handle error some way, in this example we just print error out and exit.
        return ShowErrorAndExit(hApi, error);
    }

    error = NurApiPing(hApi, NULL);
    if (error != NUR_NO_ERROR)
    {
        return ShowErrorAndExit(hApi, error);
    }

    // API ready

    error = PrintModuleVersion(hApi);
    if (error != NUR_NO_ERROR)
    {
        return ShowErrorAndExit(hApi, error);
    }

    // Inventory
    error = PerformInventory(hApi);
    if (error != NUR_NO_ERROR)
    {
        return ShowErrorAndExit(hApi, error);
    }

    // Free API object
    NurApiFree(hApi);

    return 0;
}
