

.. index::
   pair: API ; ReadSingulatedTag
   pair: API; Exception




.. _ReadSingulatedTag:

=================
ReadSingulatedTag
=================


.. contents::
   :depth: 3


Description
===========

::


    public byte[] ReadSingulatedTag(
        uint passwd,
        bool secured,
        byte sBank,
        uint sAddress,
        byte[] sMask,
        byte rdBank,
        uint rdAddress,
        byte rdByteCount
    )


Parameters
===========

passwd
------

Password for secured operations.


:Type: UInt32



secured
-------

TRUE if operation is secured, otherwise FALSE.


:Type: Boolean


sBank
-----

Memory bank used for tag singulation

- 0=BANK_PASSWD
- 1=BANK_EPC
- 2=BANK_TID
- 3=BANK_USER


:Type: Byte


sAddress
--------

Singulation data address in bits.


:Type: UInt32


sMask
------

Mask data buffer.

:Type: array<Byte>[]


rdBank
------

Memory bank for read operation.

:Type: Byte


rdAddress
---------

Address for read operation.

:Type: UInt32


rdByteCount
-----------

Number of bytes to read. This must divisible by two.


:Type: Byte


Return Value
=============

Pointer to a buffer that received read data. Must be at least  rdByteCount bytes
long

Exception
=========


NurApiException Thrown when "API not created" or "NurApiReadSingulatedTag32" error



Exemple d'appel
===============

::

    /// <summary>
    /// Lecture de la zone UserMemmory 614 octets (=>512 bits)
    /// </summary>
    private void btnReadUserMemory512_Click(object sender, EventArgs e)
    {
        if (!UHFNordic.IsConnected())
        {
            MessageBox.Show("Le lecteur Nordic n'est pas connecté au PC.", "Error");
            return;
        }

        try
        {
            byte[] byUserMemory64 = new byte[64];
            byUserMemory64 = UHFNordic.ReadSingulatedTag(0
                                                     , false, NurApi.BANK_USER, 0, null, 0, 0, 64);
        }
        catch (NurApiException ex)
        {
            // Thrown when "API not created" or "NurApiReadSingulatedTag32" error
            string message = string.Format("Exception lors de la lecture de la memoire utilisateur : {0}", ex.Message);
            Log.Write(message, false);
        }
    }





Avec le logiciel de démo  NurApiReadSingulatedTag32 (fonction C)
=================================================================

Lecture des 64 octets (512 bits) de la mémoire utilisateur.


::

    int NurApiReadSingulatedTag32( HANDLE  hApi
                                 , DWORD  passwd
                                 , BOOL  secured
                                 , BYTE  sBank
                                 , DWORD  sAddress
                                 , int  sMaskBitLength
                                 , BYTE *  sMask
                                 , BYTE  rdBank
                                 , DWORD  rdAddress
                                 , int  rdByteCount
                                 , BYTE *  rdBuffer)


Read data from tag with specific singulation.

Tag can be singluted against desired memory bank and mask.

Parameters
----------

- hApi Handle to valid NurApi object instance.
- passwd Password for secured operations.
- secured TRUE if operation is secured, otherwise FALSE.
- sBank Memory bank used for tag singulation.
- sAddress Singulation data address in bits.
- sMaskBitLength Length of the mask data in bits.
- sMask Mask data buffer.
- rdBank Memory bank for read operation.
- rdAddress Word address for read operation.
- rdByteCount Number of bytes to read. This must divisible by two.
- rdBuffer Pointer to a buffer that received read data. Must be atleast rdByteCount bytes long.

Returns
-------

Zero when succeeded, On error non=zero error code is returned.

Remarks
--------

This function uses 32=bit tag data addressing.


Exemple d'appel
---------------

::

    NurApiReadSingulatedTag32(0, 0, 0, 0, 0, 0, 3, 0, 64, 15c8cb0)



Analyse des paramètres
++++++++++++++++++++++

============== ====================================================  ============
Paramètre      Commentaire                                           Valeur
============== ====================================================  ============
passwd         Password for secured operations = 0                   0
secured        TRUE if operation is secured, otherwise FALSE = 0.    0
sBank          Memory bank used for tag singulation = 0              0 = BANK_PASSWD
sAddress       Singulation data address in bits.                     0
sMaskBitLength Length of the mask data in bits.                      0
sMask          Mask data buffer.                                     0
rdBank         Memory bank for read operation.                       3 (BANK_USER)
rdAddress      Word address for read operation.                      0
rdByteCount    Number of bytes to read. This must divisible by two.  64
rdBuffer       Pointer to a buffer that received read data= 15c8cb0  15c8cb0
============== ====================================================  ============



