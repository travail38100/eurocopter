

.. index::
   pair: API ; SetUsbAutoConnect


====================
Connexion au lecteur
====================

SetUsbAutoConnect
=================

::

    SetUsbAutoConnect(bool useAutoConnect)

