

.. index::
   pair: String ; API

.. _string_nur_api:

======================================================================
Fonctions sur les chaînes de l'API NurApi
======================================================================


.. contents::
   :depth: 3



.. _BinToHexString:

string BinToHexString(byte[] buf)
=================================

Converts binary data to Hex string.



string BinToHexString(byte[] buf, int max)
==========================================

Parameters
-----------

buf
+++

Buffer of binary data

:Type: array<Byte>[]


max
++++

max number of bytes to be converted


:Type: Int32


string BinToHexString(byte[] buf, string delim)
===============================================


string BinToHexString(byte[] buf, string delim, int max)
========================================================



byte[] HexStringToBin(string value)
===================================



byte[] HexStringToBin(string value, string delim)
=================================================


