

.. index::
   pair: API ; WriteSingulatedTag


.. _WriteSingulatedTag:

==================
WriteSingulatedTag
==================

.. contents::
   :depth: 3


Description
===========

Write data to tag with specific singulation.

Tag can be singluted against desired memory bank and mask.



::

    public void WriteSingulatedTag(
        uint passwd,
        bool secured,
        byte sBank,
        uint sAddress,
        byte[] sMask,
        byte wrBank,
        uint wrAddress,
        byte[] wrBuffer
    )

Parameters
==========

passwd
------

Password for secured operations.


:Type: UInt32


secured
--------

TRUE if operation is secured, otherwise FALSE.


:Type: Boolean


sBank
-----

Memory bank used for tag singulation.

- 0=BANK_PASSWD
- 1=BANK_EPC
- 2=BANK_TID
- 3=BANK_USER


:Type: Byte


sAddress
--------


Singulation data address in bits.

:Type: UInt32

sMask
-----

Mask data buffer.

:Type: array<Byte>[]



wrBank
-------

Memory bank for write operation.


:Type: Byte


wrAddress
----------


Word Address for write operation.


:Type: UInt32

wrBuffer
--------

Data to write. Must be atleast wrByteCount bytes long.


:Type: array<Byte>[]


Exception
=========

NurApiException Thrown when "API not created" or "NurApiWriteSingulatedTag32" error


Avec le logiciel de démo NurApiWriteSingulatedTag32 (fonction C)
================================================================

Write data to tag with specific singulation.

Tag can be singluted against desired memory bank and mask.


::

    int NurApiWriteSingulatedTag32  ( HANDLE  hApi,
      DWORD  passwd,
      BOOL  secured,
      BYTE  sBank,
      DWORD  sAddress,
      int  sMaskBitLength,
      BYTE *  sMask,
      BYTE  wrBank,
      DWORD  wrAddress,
      int  wrByteCount,
      BYTE *  wrBuffer
     )


Parameters
----------

- hApi Handle to valid NurApi object instance.
- passwd Password for secured operations.
- secured TRUE if operation is secured, otherwise FALSE.
- sBank Memory bank used for tag singulation.
- sAddress Singulation data address in bits.
- sMaskBitLength Length of the mask data in bits.
- sMask Mask data buffer.
- wrBank Memory bank for write operation.
- wrAddress Word address for write operation.
- wrByteCount Number of bytes to write. This must divisible by two.
- wrBuffer Data to write. Must be atleast wrByteCount bytes long.

Returns
--------

Zero when succeeded, On error non=zero error code is returned.

Remarks
--------

This function uses 32=bit tag data addressing


Exemple d'appel
---------------

::

    NurApiWriteSingulatedTag32(0, 0, 0, 0, 0, 0, 3, 0, 64, 18fef04)


Analyse des paramètres
++++++++++++++++++++++

============== ======================================================  ============
Paramètre      Commentaire                                             Valeur
============== ======================================================  ============
passwd         passwd Password for secured operations                  0
secured        TRUE if operation is secured, otherwise FALSE = 0.      0
sBank          sBank Memory bank used for tag singulation              0 = BANK_PASSWD
sAddress       sAddress Singulation data address in bits.              0
sMaskBitLength Length of the mask data in bits.                        0
sMask          Mask data buffer.                                       0
wrBank         Memory bank for write operation.                        3 (BANK_USER)
wrAddress      Word address for write operation.                       0
wrByteCount    Number of bytes to write. This must divisible by two    64
wrBuffer       Data to write. Must be atleast wrByteCount bytes long   15c8cb0
============== ======================================================  ============





