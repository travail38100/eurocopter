

.. index::
   pair: API ; WriteTag


.. _ecriture_tags:

=================
Ecriture de tags
=================


.. seealso::

   - :ref:`Tag`

.. contents::
   :depth: 5



.. _WriteTag:

Tag.WriteTag
============

::

    public void WriteTag(uint passwd, bool secured, byte wrBank, uint wrAddress, byte[] wrBuffer);



WriteSingulatedTag
==================

.. toctree::
   :maxdepth: 3

   WriteSingulatedTag










