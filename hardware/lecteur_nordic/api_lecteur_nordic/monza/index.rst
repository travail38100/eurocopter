
.. index::
   pair: API ; Monza4QTRead
   pair: API ; Monza4QTWrite


.. _api_lecture_ecriture_monza4QT:

===============================
Lecture/ecriture de tags Monza
===============================

.. seealso::

   - :ref:`NurApiDotNet`
   - :ref:`tags_monza4QT`


.. contents::
   :depth: 3


.. _Monza4QTRead:

Monza4QTRead
============

::

    public void Monza4QTRead(uint passwd, ref bool reduce, ref bool pubmem, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask);



.. _Monza4QTWrite:

Monza4QTWrite
=============

::

    public void Monza4QTWrite(uint passwd, bool reduce, bool pubmem, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask);



.. _ChangementMemoire:

Fonction ChangementMemoire()
============================


::

    /// <summary>
    /// Passage de mémoire privée à mémoire publique
    /// </summary>
    static public void ChangementMemoire(NurApi lecteurNordic, bool mem_is_private, RFIDEncodagePabanForm form)
    {
        byte[] smask = new byte[1];

        smask[0] = 0x00;

        CPuissance puissance_precedente = CPuissance.GetPuissance(lecteurNordic.TxLevel);

        try
        {
            // on envoie une puissance de 63 mW car en dessous on a des problèmes de lecture
            CPuissance.EnvoyerPuissance("63 mW", lecteurNordic, form);

            try
            {
                lecteurNordic.Monza4QTWrite(0
                                             , false  // reduce
                                             , !mem_is_private  // passer en mémoire public ou privé
                                             , NurApi.BANK_EPC
                                             , 0
                                             , 1
                                             , smask);
            }
            catch (Exception ex)
            {
                string message = string.Format("Pb exception: Monza4QTWrite {0}", ex.Message);
                MessageBox.Show(message);
            }

            try
            {
                bool reduce = true;
                bool pubmem = true;
                lecteurNordic.Monza4QTRead(0
                                           , ref reduce
                                           , ref pubmem
                                           , NurApi.BANK_EPC
                                           , 0
                                           , 1
                                           , smask);

                if (mem_is_private == !pubmem)
                {
                    string info = string.Format("ChangementMemoire() le changement est correct.");
                    Log.Write(info, true);
                }

            }
            catch (Exception ex)
            {
                string message = string.Format("Pb exception: Monza4QTRead {0}", ex.Message);
                MessageBox.Show(message);
            }


            // on envoie la puissance précédente
            CPuissance.EnvoyerPuissance(puissance_precedente.StrPuissance, lecteurNordic, form);
        }
        catch (Exception ex)
        {
            CPuissance.EnvoyerPuissance(puissance_precedente.StrPuissance, lecteurNordic, form);

            string message = string.Format("Pb exception: {0}", ex.Message);
            MessageBox.Show(message);
        }
    }







