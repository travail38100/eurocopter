


.. index::
   pair: DotNet; NurApiDotNet


.. _NurApiDotNet:

======================================
NurApiDotNet.dll
======================================

::

    #region Assembly NurApiDotNet.dll, v2.0.50727
    // C:\Tmp\Samples\SimpleDotNetSamples\HelloNUR_CSharp\bin\Debug\NurApiDotNet.dll
    #endregion

    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Windows.Forms;

    namespace NurApiDotNet
    {
        public class NurApi
        {
            public const int ANTENNAID_1 = 0;
            public const int ANTENNAID_2 = 1;
            public const int ANTENNAID_3 = 2;
            public const int ANTENNAID_4 = 3;
            public const int ANTENNAID_AUTOSELECT = -1;
            public const int ANTENNAMASK_1 = 1;
            public const int ANTENNAMASK_2 = 2;
            public const int ANTENNAMASK_3 = 4;
            public const int ANTENNAMASK_4 = 8;
            public const int ANTENNAMASK_ALL = 15;
            public const int BANK_EPC = 1;
            public const int BANK_PASSWD = 0;
            public const int BANK_TID = 2;
            public const int BANK_USER = 3;
            public const int BC_CMD_BEEP = 5;
            public const int BC_CMD_GET_DEV_INFO = 1;
            public const int BC_CMD_SET_ETHCONFIG = 4;
            public const int BC_FILTER_OP_EQUAL = 0;
            public const int BC_FILTER_OP_HIGHER = 1;
            public const int BC_FILTER_OP_LOWER = 2;
            public const int BC_FILTER_TYPE_ADDRTYPE = 5;
            public const int BC_FILTER_TYPE_IP = 2;
            public const int BC_FILTER_TYPE_MAC = 1;
            public const int BC_FILTER_TYPE_MODE = 6;
            public const int BC_FILTER_TYPE_NAME = 9;
            public const int BC_FILTER_TYPE_NONE = 0;
            public const int BC_FILTER_TYPE_NURVER = 7;
            public const int BC_FILTER_TYPE_SERVERPORT = 4;
            public const int BC_FILTER_TYPE_STATUS = 8;
            public const int BC_FILTER_TYPE_VERSION = 3;
            public const int BR_1000000 = 3;
            public const int BR_115200 = 0;
            public const int BR_1500000 = 4;
            public const int BR_230400 = 1;
            public const int BR_38400 = 5;
            public const int BR_500000 = 2;
            public const int DEFAULT_BAUDRATE = 115200;
            public const int DEVSEARCH_LENGTH = 86;
            public const int FACTION_0 = 0;
            public const int FACTION_1 = 1;
            public const int FACTION_2 = 2;
            public const int FACTION_3 = 3;
            public const int FACTION_4 = 4;
            public const int FACTION_5 = 5;
            public const int FACTION_6 = 6;
            public const int FACTION_7 = 7;
            public const int GPIO_ACT_INVENTORY = 3;
            public const int GPIO_ACT_NONE = 0;
            public const int GPIO_ACT_NOTIFY = 1;
            public const int GPIO_ACT_SCANTAG = 2;
            public const int GPIO_EDGE_BOTH = 2;
            public const int GPIO_EDGE_FALLING = 0;
            public const int GPIO_EDGE_RISING = 1;
            public const int GPIO_TYPE_ANTCTL1 = 5;
            public const int GPIO_TYPE_ANTCTL2 = 6;
            public const int GPIO_TYPE_BEEPER = 4;
            public const int GPIO_TYPE_INPUT = 1;
            public const int GPIO_TYPE_OUTPUT = 0;
            public const int GPIO_TYPE_RFIDON = 2;
            public const int GPIO_TYPE_RFIDREAD = 3;
            public const int GPIO1 = 0;
            public const int GPIO2 = 1;
            public const int GPIO3 = 2;
            public const int GPIO4 = 3;
            public const int GPIO5 = 4;
            public const int INVTARGET_A = 0;
            public const int INVTARGET_AB = 2;
            public const int INVTARGET_B = 1;
            public const int LOCK_ACCESSPWD = 8;
            public const int LOCK_EPCMEM = 4;
            public const int LOCK_KILLPWD = 16;
            public const int LOCK_OPEN = 0;
            public const int LOCK_PERMALOCK = 3;
            public const int LOCK_PERMAWRITE = 1;
            public const int LOCK_SECURED = 2;
            public const int LOCK_TIDMEM = 2;
            public const int LOCK_USERMEM = 1;
            public const int LOG_ALL = 15;
            public const int LOG_DATA = 8;
            public const int LOG_ERROR = 2;
            public const int LOG_USER = 4;
            public const int LOG_VERBOSE = 1;
            public const int MAX_ANTENNAS = 4;
            public const int MAX_BITSTR_BITS = 1024;
            public const int MAX_CONFIG_REGIONS = 16;
            public const int MAX_CUSTOM_FREQS = 100;
            public const int MAX_EPC_LENGTH = 62;
            public const int MAX_FILTERS = 8;
            public const int MAX_GPIO = 7;
            public const int MAX_PATH = 1024;
            public const int MAX_REGION_NAME = 64;
            public const int MAX_SELMASK = 62;
            public const int NOTIFICATION_CLIENTCONNECTED = 14;
            public const int NOTIFICATION_CLIENTDISCONNECTED = 15;
            public const int NOTIFICATION_DEVSEARCH = 13;
            public const int NOTIFICATION_EASALARM = 16;
            public const int NOTIFICATION_HOPEVENT = 10;
            public const int NOTIFICATION_INVENTORYEX = 12;
            public const int NOTIFICATION_INVENTORYSTREAM = 11;
            public const int NOTIFICATION_IOCHANGE = 8;
            public const int NOTIFICATION_LOG = 1;
            public const int NOTIFICATION_MODULEBOOT = 5;
            public const int NOTIFICATION_NONE = 0;
            public const int NOTIFICATION_PERIODIC_INVENTORY = 2;
            public const int NOTIFICATION_PRGPRGRESS = 3;
            public const int NOTIFICATION_TRACETAG = 7;
            public const int NOTIFICATION_TRCONNECTED = 6;
            public const int NOTIFICATION_TRDISCONNECTED = 4;
            public const int NOTIFICATION_TRIGGERREAD = 9;
            public const int OPFLAGS_EN_HOPEVENTS = 1;
            public const int OPFLAGS_INVSTREAM_ZEROS = 2;
            public const int REGIONID_AUSTRALIA = 5;
            public const int REGIONID_BRAZIL = 4;
            public const int REGIONID_CUSTOM = 254;
            public const int REGIONID_EU = 0;
            public const int REGIONID_FCC = 1;
            public const int REGIONID_JA250MW = 7;
            public const int REGIONID_JA500MW = 8;
            public const int REGIONID_MALAYSIA = 3;
            public const int REGIONID_NEWZEALAND = 6;
            public const int REGIONID_PRC = 2;
            public const int RXDECODING_FM0 = 0;
            public const int RXDECODING_M2 = 1;
            public const int RXDECODING_M4 = 2;
            public const int RXDECODING_M8 = 4;
            public const int SELSTATE_ALL = 0;
            public const int SELSTATE_NOTSL = 2;
            public const int SELSTATE_SL = 3;
            public const int SESSION_S0 = 0;
            public const int SESSION_S1 = 1;
            public const int SESSION_S2 = 2;
            public const int SESSION_S3 = 3;
            public const int SESSION_SL = 4;
            public const int SETUP_ALL = 262143;
            public const int SETUP_ANTMASK = 256;
            public const int SETUP_INVENTORYTO = 1024;
            public const int SETUP_INVEPCLEN = 16384;
            public const int SETUP_INVQ = 32;
            public const int SETUP_INVROUNDS = 128;
            public const int SETUP_INVRSSIFILTER = 131072;
            public const int SETUP_INVSESSION = 64;
            public const int SETUP_INVTARGET = 8192;
            public const int SETUP_LINKFREQ = 1;
            public const int SETUP_OPFLAGS = 4096;
            public const int SETUP_READRSSIFILTER = 32768;
            public const int SETUP_REGION = 16;
            public const int SETUP_RXDEC = 2;
            public const int SETUP_SCANSINGLETO = 512;
            public const int SETUP_SELECTEDANTENNA = 2048;
            public const int SETUP_TXLEVEL = 4;
            public const int SETUP_TXMOD = 8;
            public const int SETUP_WRITERSSIFILTER = 65536;
            public const int STORE_ALL = 15;
            public const int STORE_BAUDRATE = 4;
            public const int STORE_GPIO = 2;
            public const int STORE_OPFLAGS = 8;
            public const int STORE_RF = 1;
            public const int TRACETAG_NO_EPC = 1;
            public const int TRACETAG_START_CONTINUOUS = 2;
            public const int TRACETAG_STOP_CONTINUOUS = 8;
            public const int TXMODULATION_ASK = 0;
            public const int TXMODULATION_PRASK = 1;

            public static IntPtr INVALID_HANDLE_VALUE;

            public NurApi();
            public NurApi(Control notificationReceiver);
            public NurApi(IntPtr clientHandle);

            public int AntennaMask { get; set; }
            public int BaudRate { get; set; }
            public bool Disposed { get; }
            public int InventoryEpcLength { get; set; }
            public int InventoryQ { get; set; }
            public int InventoryRounds { get; set; }
            public NurApi.RssiFilter InventoryRssiFilter { get; set; }
            public int InventorySession { get; set; }
            public int InventoryTarget { get; set; }
            public int InventoryTriggerTimeout { get; set; }
            public int LinkFrequency { get; set; }
            public int OpFlags { get; set; }
            public NurApi.RssiFilter ReadRssiFilter { get; set; }
            public int RealBaudRate { get; set; }
            public int Region { get; set; }
            public int RxDecoding { get; set; }
            public int ScanSingleTriggerTimeout { get; set; }
            public int SelectedAntenna { get; set; }
            public string Title { get; set; }
            public int TxLevel { get; set; }
            public int TxModulation { get; set; }
            public NurApi.RssiFilter WriteRssiFilter { get; set; }

            public event EventHandler<NurApi.BootEventArgs> BootEvent;
            public event EventHandler<NurApi.ClientInfoEventArgs> ClientConnectedEvent;
            public event EventHandler<NurApi.ClientInfoEventArgs> ClientDisconnectedEvent;
            public event EventHandler<NurApi.NurEventArgs> ConnectedEvent;
            public event EventHandler<NurApi.DevInfoEventArgs> DevInfoEvent;
            public event EventHandler<NurApi.NurEventArgs> DisconnectedEvent;
            public event EventHandler<NurApi.InventoryStreamEventArgs> InventoryExEvent;
            public event EventHandler<NurApi.InventoryStreamEventArgs> InventoryStreamEvent;
            public event EventHandler<NurApi.IOChangeEventArgs> IOChangeEvent;
            public event EventHandler<NurApi.LogEventArgs> LogEvent;
            public event EventHandler<NurApi.NXPAlarmStreamEventArgs> NXPAlarmStreamEvent;
            public event EventHandler<NurApi.PeriodicInventoryEventArgs> PeriodicInventoryEvent;
            public event EventHandler<NurApi.PrgEventArgs> PrgEvent;
            public event EventHandler<NurApi.TraceTagEventArgs> TraceTagEvent;
            public event EventHandler<NurApi.TriggerReadEventArgs> TriggerReadEvent;
            public event EventHandler<NurApi.UnknownNotifyEventArgs> UnknownNotifyEvent;

            public static NurApi.ClientInfoData ArrayToClientInfoData(byte[] ptr);
            public static NurApi.DevInfoData ArrayToInfoData(byte[] ptr);
            public static NurApi.PeriodicInventoryData ArrayToInvData(byte[] ptr);
            public static NurApi.InventoryStreamData ArrayToInventoryStreamData(byte[] ptr);
            public static NurApi.IOChangeData ArrayToIOChangeData(byte[] ptr);
            public static NurApi.NXPAlarmStreamData ArrayToNXPAlarmStreamData(byte[] ptr);
            public static NurApi.PrgProgressData ArrayToPrgProgressData(byte[] ptr);
            public static NurApi.TraceTagData ArrayToTraceTagData(byte[] ptr);
            public static NurApi.TriggerReadData ArrayToTriggerReadData(byte[] ptr);
            public void Beep();
            public void Beep(int frequency, int time, int duty);
            public static string BinToHexString(byte[] buf);
            public static string BinToHexString(byte[] buf, int max);
            public static string BinToHexString(byte[] buf, string delim);
            public static string BinToHexString(byte[] buf, string delim, int max);
            public static int BitBufferAddEBV32(byte[] buf, uint ebv, int curPt);
            public static int BitBufferAddValue(byte[] buf, uint Value32, int numBits, int curPt);
            public void BuildCustomHoptable(uint baseFreq, uint nChan, uint chSpace, uint chTime, uint pauseTime, uint lf, uint Tari, bool shuffle);
            public void ClearTags();
            public void Connect();
            public void ConnectIntegratedReader();
            public void ConnectSerialPort(int comNumber);
            public void ConnectSerialPort(int comNumber, int baudrate);
            public void ConnectSocket(string ip, int port);
            public void ConnectUsb(string devpath);
            public void ContCarrier(byte[] data, uint dataLen);
            public byte[] CustomCmd(int cmd, byte[] inbuffer);
            public int CustomCmd(int cmd, byte[] inbuffer, uint inbufferLen, ref byte[] outbuffer, ref uint bytesRet);
            public NurApi.CustomExchangeResponse CustomExchange(uint passwd, bool secured, NurApi.CustomExchangeParams cxcParams);
            public NurApi.CustomExchangeResponse CustomExchange(uint passwd, bool secured, ushort txLen, ushort rxLen, uint rxTimeout, bool asWrite, bool appendHandle, bool xorRN16, bool txOnly, bool noTxCRC, bool noRxCRC, bool txCRC5, bool rxLenUnk, bool rxStripHandle, byte[] bitBuffer);
            public NurApi.CustomExchangeResponse CustomExchangeByEPC(uint passwd, bool secured, byte[] epc, NurApi.CustomExchangeParams cxcParams);
            public NurApi.CustomExchangeResponse CustomExchangeByEPC(uint passwd, bool secured, byte[] epc, ushort txLen, ushort rxLen, uint rxTimeout, bool asWrite, bool appendHandle, bool xorRN16, bool txOnly, bool noTxCRC, bool noRxCRC, bool txCRC5, bool rxLenUnk, bool rxStripHandle, byte[] bitBuffer);
            public NurApi.CustomExchangeResponse CustomExchangeSingulated(uint passwd, bool secured, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, NurApi.CustomExchangeParams cxcParams);
            public NurApi.CustomExchangeResponse CustomExchangeSingulated(uint passwd, bool secured, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, ushort txLen, ushort rxLen, uint rxTimeout, bool asWrite, bool appendHandle, bool xorRN16, bool txOnly, bool noTxCRC, bool noRxCRC, bool txCRC5, bool rxLenUnk, bool rxStripHandle, byte[] bitBuffer);
            public byte[] CustomReadSingulatedTag(uint rdCmd, byte cmdBits, uint rdBank, byte bankBits, uint passwd, bool secured, byte sBank, uint sAddress, byte[] sMask, uint rdAddress, byte rdByteCount);
            public byte[] CustomReadSingulatedTag(uint rdCmd, byte cmdBits, uint rdBank, byte bankBits, uint passwd, bool secured, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, uint rdAddress, int rdByteCount);
            public byte[] CustomReadTag(uint rdCmd, byte cmdBits, uint rdBank, byte bankBits, uint passwd, bool secured, uint address, int byteCount);
            public byte[] CustomReadTagByEPC(uint rdCmd, byte cmdBits, uint rdBank, byte bankBits, uint passwd, bool secured, byte[] epc, uint rdAddress, int rdByteCount);
            public void CustomWriteSingulatedTag(uint wrCmd, byte cmdBits, uint wrBank, byte bankBits, uint passwd, bool secured, byte sBank, uint sAddress, byte[] sMask, uint wrAddress, byte[] wrBuffer);
            public void CustomWriteSingulatedTag(uint wrCmd, byte cmdBits, uint wrBank, byte bankBits, uint passwd, bool secured, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, uint wrAddress, byte[] wrBuffer);
            public void CustomWriteSingulatedTag(uint wrCmd, byte cmdBits, uint wrBank, byte bankBits, uint passwd, bool secured, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, uint wrAddress, byte[] wrBuffer, int wrBufferPos, int wrBufferLen);
            public void CustomWriteTag(uint wrCmd, byte cmdBits, uint wrBank, byte bankBits, uint passwd, bool secured, uint wrAddress, byte[] wrBuffer);
            public void CustomWriteTag(uint wrCmd, byte cmdBits, uint wrBank, byte bankBits, uint passwd, bool secured, uint wrAddress, byte[] wrBuffer, int wrBufferPos, int wrBufferLen);
            public void CustomWriteTagByEPC(uint wrCmd, byte cmdBits, uint wrBank, byte bankBits, uint passwd, bool secured, byte[] epc, uint wrAddress, byte[] wrBuffer);
            public void CustomWriteTagByEPC(uint wrCmd, byte cmdBits, uint wrBank, byte bankBits, uint passwd, bool secured, byte[] epc, uint wrAddress, byte[] wrBuffer, int wrBufferPos, int wrBufferLen);
            public void Disconnect();
            public void Dispose();
            public void ELog(string txt);
            public void EnterBoot();
            public static List<NurApi.ComPort> EnumerateComPorts();
            public static List<NurApi.UsbDevice> EnumerateUsbDevices();
            public void FactoryReset(uint resetcode);
            public NurApi.TagStorage FetchTags();
            public NurApi.TagStorage FetchTags(bool includeMeta);
            public NurApi.TagStorage FetchTags(bool includeMeta, ref int tagsAdded);
            public uint GetAccessPassword(uint passwd, bool secured, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask);
            public uint GetAccessPasswordByEPC(uint passwd, bool secured, byte[] epc);
            public NurApi.CustomHoptable GetCustomHoptable();
            public static string GetErrorMessage(int error);
            public NurApi.EthConfig GetEthConfig();
            public string GetFileVersion();
            public NurApi.GpioEntry[] GetGPIOConfig();
            public NurApi.GpioStatus GetGPIOStatus(int gpio);
            public IntPtr GetHandle();
            public uint GetKillPassword(uint passwd, bool secured, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask);
            public uint GetKillPasswordByEPC(uint passwd, bool secured, byte[] epc);
            public string GetLogFilePath();
            public int GetLogLevel();
            public bool GetLogToFile();
            public char GetMode();
            public NurApi.ModuleSetup GetModuleSetup();
            public NurApi.ModuleSetup GetModuleSetup(int setupFlags);
            public NurApi.ReaderInfo GetReaderInfo();
            public NurApi.ReflectedPowerInfo GetReflectedPower();
            public NurApi.ReflectedPowerInfo GetReflectedPower(uint freq);
            public NurApi.RegionInfo GetRegionInfo();
            public NurApi.RegionInfo GetRegionInfo(int regionId);
            public NurApi.SensorConfig GetSensorConfig();
            public NurApi.SystemInfo GetSystemInfo();
            public NurApi.TagStorage GetTagStorage();
            public uint GetTimestamp();
            public bool GetUsbAutoConnect();
            public bool GetUseBlockWrite();
            public static byte[] HexStringToBin(string value);
            public static byte[] HexStringToBin(string value, string delim);
            public NurApi.InventoryResponse Inventory();
            public NurApi.InventoryResponse Inventory(int rounds, int Q, int session);
            public NurApi.InventoryResponse InventoryEx(ref NurApi.InventoryExParams param, NurApi.InventoryExFilter[] filters);
            public NurApi.InventoryResponse InventorySelect(int rounds, int Q, int session, bool invertSelect, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask);
            public NurApi.InventoryResponse InventorySelectByEPC(int rounds, int Q, int session, bool invertSelect, byte[] epcBuffer);
            public bool IsConnected();
            public bool IsCreated();
            public bool IsInventoryExRunning();
            public bool IsInventoryStreamRunning();
            public bool IsNXPAlarmStreamRunning();
            public bool IsPeriodicInventoryRunning();
            public static bool IsTagError(int error);
            public bool IsTraceRunning();
            public void KillTag(uint passwd, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask);
            public void KillTagByEPC(uint passwd, byte[] epc);
            public void LoadSetupFile(string filename);
            public void Log(int level, string txt);
            public static bool MemCompare(byte[] b1, byte[] b2);
            public void ModuleRestart();
            public void Monza4QTRead(uint passwd, ref bool reduce, ref bool pubmem, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask);
            public void Monza4QTWrite(uint passwd, bool reduce, bool pubmem, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask);
            public void NurApiSendBroadcast(int cmd, int filterType, int filterOp, byte[] filterData, int filterSize, byte[] data, int dataLength);
            public bool NXPAlarm();
            public void NXPResetEAS(uint passwd, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask);
            public void NXPResetReadProtect(uint passwd, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask);
            public void NXPSetEAS(uint passwd, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask);
            public void NXPSetReadProtect(uint passwd, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask);
            public void NXPStartAlarmStream();
            public void NXPStopAlarmStream();
            public string Ping();
            public byte[] ReadSingulatedTag(uint passwd, bool secured, byte sBank, uint sAddress, byte[] sMask, byte rdBank, uint rdAddress, byte rdByteCount);
            public byte[] ReadSingulatedTag(uint passwd, bool secured, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, byte rdBank, uint rdAddress, int rdByteCount);
            public byte[] ReadTag(uint passwd, bool secured, byte bank, uint address, int byteCount);
            public byte[] ReadTagByEPC(uint passwd, bool secured, byte[] epc, byte rdBank, uint rdAddress, int rdByteCount);
            public void ResetToTarget(int session, bool targetIsA);
            public static byte[] ResizeArray(byte[] oldArray, int newSize);
            public static TInput[] ResizeArrayT<TInput>(TInput[] oldArray, int newSize);
            public void SaveSetupFile(string filename);
            public NurApi.ScanChannelInfo[] ScanChannels();
            public NurApi.ScanChannelInfo[] ScanChannels(int maxChannels);
            public NurApi.TriggerReadData ScanSingle(int timeout);
            public void SetAccessPassword(uint passwd, bool secured, byte sBank, uint sAddress, byte[] sMask, uint newPasswd);
            public void SetAccessPassword(uint passwd, bool secured, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, uint newPasswd);
            public void SetAccessPasswordByEPC(uint passwd, bool secured, byte[] epc, uint newPasswd);
            public void SetEthConfig(ref NurApi.EthConfig ec);
            public void SetGPIOConfig(NurApi.GpioEntry[] config);
            public void SetGPIOStatus(int gpio, bool state);
            public void SetKillPassword(uint passwd, bool secured, byte sBank, uint sAddress, byte[] sMask, uint newPasswd);
            public void SetKillPassword(uint passwd, bool secured, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, uint newPasswd);
            public void SetKillPasswordByEPC(uint passwd, bool secured, byte[] epc, uint newPasswd);
            public void SetLock(uint passwd, byte sBank, uint sAddress, byte[] sMask, uint memoryMask, uint action);
            public void SetLock(uint passwd, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, uint memoryMask, uint action);
            public void SetLockByEPC(uint passwd, byte[] epc, uint memoryMask, uint action);
            public void SetLockRaw(uint passwd, byte sBank, uint sAddress, byte[] sMask, uint lockMask, uint lockAction);
            public void SetLockRaw(uint passwd, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, uint lockMask, uint lockAction);
            public void SetLogFilePath(string path);
            public void SetLogLevel(int mask);
            public void SetLogToFile(bool enable);
            public void SetModuleSetup(int setupFlags, ref NurApi.ModuleSetup setup);
            public void SetNotificationReceiver(Control receiver);
            public void SetSensorConfig(ref NurApi.SensorConfig cfg);
            public void SetUsbAutoConnect(bool useAutoConnect);
            public void SetUseBlockWrite(bool val);
            public NurApi.InventoryResponse SimpleInventory();
            public void StartInventoryEx(ref NurApi.InventoryExParams param, NurApi.InventoryExFilter[] filters);
            public void StartInventorySelectStream(int rounds, int Q, int session, bool invertSelect, byte sBank, uint sAddress, byte[] sMask);
            public void StartInventorySelectStream(int rounds, int Q, int session, bool invertSelect, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask);
            public void StartInventoryStream();
            public void StartInventoryStream(int rounds, int Q, int session);
            public void StartPeriodicInventory(int rounds, int Q, int session);
            public void StartProgramApp(string fname);
            public void StartProgramBootloader(string fname);
            public void StartServer(int port, int maxClients);
            public void StopContCarrier();
            public void StopContinuous();
            public void StopInventoryEx();
            public void StopInventoryStream();
            public void StopPeriodicInventory();
            public void StopPeriodicInventory(bool waitExit);
            public void StopServer();
            public void StoreCurrentSetup();
            public void StoreCurrentSetup(int flags);
            public NurApi.TraceTagData TraceTag(byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, int flags);
            public NurApi.TraceTagData TraceTagByEPC(byte[] epc, int flags);
            public void ULog(string txt);
            public NurApi.TagStorage UpdateTagStorage(bool includeMeta, ref int tagsAdded);
            public void VLog(string txt);
            public void WriteEPC(uint passwd, bool secured, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, byte[] epcBuffer);
            public void WriteEPCByEPC(uint passwd, bool secured, byte[] currentEpcBuffer, byte[] newEpcBuffer);
            public void WriteSingulatedTag(uint passwd, bool secured, byte sBank, uint sAddress, byte[] sMask, byte wrBank, uint wrAddress, byte[] wrBuffer);
            public void WriteSingulatedTag(uint passwd, bool secured, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, byte wrBank, uint wrAddress, byte[] wrBuffer);
            public void WriteSingulatedTag(uint passwd, bool secured, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, byte wrBank, uint wrAddress, byte[] wrBuffer, int wrBufferPos, int wrBufferLen);
            public void WriteTag(uint passwd, bool secured, byte wrBank, uint wrAddress, byte[] wrBuffer);
            public void WriteTag(uint passwd, bool secured, byte wrBank, uint wrAddress, byte[] wrBuffer, int wrBufferPos, int wrBufferLen);
            public void WriteTagByEPC(uint passwd, bool secured, byte[] epc, byte wrBank, uint wrAddress, byte[] wrBuffer);
            public void WriteTagByEPC(uint passwd, bool secured, byte[] epc, byte wrBank, uint wrAddress, byte[] wrBuffer, int wrBufferPos, int wrBufferLen);

            public delegate void NotificationCallbackDelegate(uint timestamp, int type, byte[] data);

            public class BootEventArgs : NurApi.NurEventArgs
            {
                public string message;

                public BootEventArgs(uint timestamp, string message);
            }

            public class ClientInfoEventArgs : NurApi.NurEventArgs
            {
                public NurApi cApi;
                public NurApi.ClientInfoData data;

                public ClientInfoEventArgs(uint timestamp, NurApi.ClientInfoData data);
            }

            public class ComPort
            {
                public string friendlyName;
                public int port;

                public ComPort();
            }

            public class DevInfoEventArgs : NurApi.NurEventArgs
            {
                public NurApi.DevInfoData data;

                public DevInfoEventArgs(uint timestamp, NurApi.DevInfoData data);
            }

            public class InventoryStreamEventArgs : NurApi.NurEventArgs
            {
                public NurApi.InventoryStreamData data;

                public InventoryStreamEventArgs(uint timestamp, NurApi.InventoryStreamData data);
            }

            public class IOChangeEventArgs : NurApi.NurEventArgs
            {
                public NurApi.IOChangeData data;

                public IOChangeEventArgs(uint timestamp, NurApi.IOChangeData data);
            }

            public class LogEventArgs : NurApi.NurEventArgs
            {
                public string message;

                public LogEventArgs(uint timestamp, string message);
            }

            public class NurEventArgs : EventArgs
            {
                public uint timestamp;

                public NurEventArgs(uint timestamp);
            }

            public class NXPAlarmStreamEventArgs : NurApi.NurEventArgs
            {
                public NurApi.NXPAlarmStreamData data;

                public NXPAlarmStreamEventArgs(uint timestamp, NurApi.NXPAlarmStreamData data);
            }

            public class PeriodicInventoryEventArgs : NurApi.NurEventArgs
            {
                public NurApi.PeriodicInventoryData data;

                public PeriodicInventoryEventArgs(uint timestamp, NurApi.PeriodicInventoryData data);
            }

            public class PrgEventArgs : NurApi.NurEventArgs
            {
                public NurApi.PrgProgressData data;

                public PrgEventArgs(uint timestamp, NurApi.PrgProgressData data);
            }

            public class Tag : ICloneable
            {
                public byte antennaId;
                public byte channel;
                public uint frequency;
                public NurApi hApi;
                public ushort pc;
                public sbyte rssi;
                public sbyte scaledRssi;
                public ushort timestamp;
                public object userData;

                public Tag();
                public Tag(NurApi api);
                public Tag(NurApi.Tag src);
                public Tag(NurApi api, ref NurApi.TagData data);

                public static bool operator !=(NurApi.Tag left, NurApi.Tag right);
                public static bool operator ==(NurApi.Tag left, NurApi.Tag right);

                public byte[] epc { get; set; }

                public object Clone();
                public bool Compare(NurApi.Tag t);
                public override bool Equals(object obj);
                public uint GetAccessPassword(uint passwd, bool secured);
                public string GetEpcString();
                public override int GetHashCode();
                public uint GetKillPassword(uint passwd, bool secured);
                public void Inventory();
                public void Inventory(int rounds, int Q, int session);
                public bool IsNull();
                public void KillTag(uint passwd);
                public byte[] ReadTag(uint passwd, bool secured, byte rdBank, uint rdAddress, int rdByteCount);
                public void SetAccessPassword(uint passwd, bool secured, uint newPasswd);
                public void SetKillPassword(uint passwd, bool secured, uint newPasswd);
                public void SetLock(uint passwd, uint memoryMask, uint action);
                public override string ToString();
                public NurApi.TraceTagData TraceTag(int flags);
                public void WriteEPC(uint passwd, bool secured, byte[] newEpcBuffer);
                public void WriteTag(uint passwd, bool secured, byte wrBank, uint wrAddress, byte[] wrBuffer);
                public void WriteTag(uint passwd, bool secured, byte wrBank, uint wrAddress, byte[] wrBuffer, int wrBufferPos, int wrBufferLen);
            }

            public class TagStorage : IEnumerable
            {
                public TagStorage();

                public int Count { get; }

                public NurApi.Tag this[byte[] epc] { get; }
                public NurApi.Tag this[int i] { get; }

                public bool AddTag(NurApi.Tag tag);
                public bool AddTag(NurApi.Tag tag, out NurApi.Tag storedTag);
                public void Clear();
                public int Copy(NurApi.TagStorage srcStorage);
                public IEnumerator GetEnumerator();
                public NurApi.Tag GetTag(byte[] epc);
                public NurApi.Tag GetTag(string epcStr);
                public bool HasTag(byte[] epc);
                public bool HasTag(NurApi.Tag tag);
                public bool HasTag(string epcStr);
                public bool TryGetTag(byte[] epc, out NurApi.Tag tag);
            }

            public class TraceTagEventArgs : NurApi.NurEventArgs
            {
                public NurApi.TraceTagData data;

                public TraceTagEventArgs(uint timestamp, NurApi.TraceTagData data);
            }

            public class TriggerReadEventArgs : NurApi.NurEventArgs
            {
                public NurApi.TriggerReadData data;

                public TriggerReadEventArgs(uint timestamp, NurApi.TriggerReadData data);
            }

            public class UnknownNotifyEventArgs : NurApi.NurEventArgs
            {
                public byte[] data;
                public int type;

                public UnknownNotifyEventArgs(uint timestamp, int type, byte[] data);
            }

            public class UsbDevice
            {
                public string devPath;
                public string friendlyName;

                public UsbDevice();
            }

            public struct ClientInfoData
            {
                public IntPtr hApi;
                public string ip;
                public int port;
            }

            public struct CustomExchangeParams
            {
                public bool appendHandle;
                public bool asWrite;
                public byte[] bitBuffer;
                public bool noRxCRC;
                public bool noTxCRC;
                public ushort rxLen;
                public bool rxLenUnknown;
                public bool rxStripHandle;
                public uint rxTimeout;
                public bool txCRC5;
                public ushort txLen;
                public bool txOnly;
                public bool xorRN16;
            }

            public struct CustomExchangeResponse
            {
                public int error;
                public byte[] tagBytes;
            }

            public struct CustomHoptable
            {
                public uint chTime;
                public uint Count;
                public uint[] freqs;
                public uint maxLF;
                public uint silentTime;
                public uint Tari;
            }

            public struct DevInfoData
            {
                public char[] altSerial;
                public byte boardID;
                public byte boardType;
                public NurApi.EthConfig eth;
                public char[] hwVer;
                public byte IOstate;
                public int lightADC;
                public byte nurAntNum;
                public char[] nurName;
                public byte[] nurVer;
                public char[] serial;
                public byte status;
                public int tapADC;
                public double temp;
                public int upTime;
            }

            public struct EthConfig
            {
                public byte addrType;
                public byte[] gw;
                public byte[] hostip;
                public byte hostmode;
                public int hostPort;
                public byte[] ip;
                public byte[] mac;
                public byte[] mask;
                public byte[] reserved;
                public int serverPort;
                public byte[] staticip;
                public string title;
                public byte transport;
                public int version;
            }

            public struct GpioConfig
            {
                public int count;
                public NurApi.GpioEntry[] entries;
            }

            public struct GpioEntry
            {
                public int action;
                public bool available;
                public int edge;
                public bool enabled;
                public int type;
            }

            public struct GpioStatus
            {
                public bool enabled;
                public bool state;
                public int type;
            }

            public struct InventoryExFilter
            {
                public byte action;
                public uint address;
                public byte bank;
                public int maskBitLength;
                public byte[] maskData;
                public byte target;
                public bool truncate;
            }

            public struct InventoryExParams
            {
                public int inventorySelState;
                public int inventoryTarget;
                public int Q;
                public int rounds;
                public int session;
                public int transitTime;
            }

            public struct InventoryResponse
            {
                public int collisions;
                public int numTagsFound;
                public int numTagsMem;
                public int Q;
                public int roundsDone;
            }

            public struct InventoryStreamData
            {
                public int collisions;
                public int Q;
                public int roundsDone;
                public bool stopped;
                public int tagsAdded;
            }

            public struct IOChangeData
            {
                public int dir;
                public bool sensor;
                public int source;
            }

            public struct ModuleSetup
            {
                public int antennaMask;
                public int inventoryEpcLength;
                public int inventoryQ;
                public int inventoryRounds;
                public NurApi.RssiFilter inventoryRssiFilter;
                public int inventorySession;
                public int inventoryTarget;
                public int inventoryTriggerTimeout;
                public int linkFreq;
                public uint opFlags;
                public NurApi.RssiFilter readRssiFilter;
                public int regionId;
                public int rxDecoding;
                public int scanSingleTriggerTimeout;
                public int selectedAntenna;
                public int txLevel;
                public int txModulation;
                public NurApi.RssiFilter writeRssiFilter;
            }

            public struct NXPAlarmStreamData
            {
                public bool armed;
                public bool stopped;
            }

            public struct PeriodicInventoryData
            {
                public int collisions;
                public int error;
                public int numTagsFound;
                public int numTagsMem;
                public int Q;
                public int roundsDone;
            }

            public struct PrgProgressData
            {
                public int curPage;
                public int error;
                public int totalPages;
            }

            public struct ReaderInfo
            {
                public string altSerial;
                public char devBuild;
                public string fccId;
                public string hwVersion;
                public string name;
                public int numAntennas;
                public int numGpio;
                public int numRegions;
                public int numSensors;
                public string serial;
                public int swVerMajor;
                public int swVerMinor;

                public string GetVersionString();
            }

            public struct ReflectedPowerInfo
            {
                public int div;
                public int iPart;
                public int qPart;
            }

            public struct RegionInfo
            {
                public uint baseFrequency;
                public uint channelCount;
                public uint channelSpacing;
                public uint channelTime;
                public string name;
                public uint regionId;
            }

            public struct RssiFilter
            {
                public sbyte max;
                public sbyte min;
            }

            public struct ScanChannelInfo
            {
                public uint freq;
                public int rawiq;
                public int rssi;
            }

            public struct SensorConfig
            {
                public int lightAction;
                public bool lightEnabled;
                public int tapAction;
                public bool tapEnabled;
            }

            public struct SystemInfo
            {
                public uint appAddr;
                public uint appCRCWord;
                public uint appSzWord;
                public uint blAddr;
                public uint mainStackUsage;
                public uint nvSetAddr;
                public uint stackTop;
                public uint szFlash;
                public uint szNvSettings;
                public uint szSram;
                public uint szTagBuffer;
                public uint szUsedSram;
                public uint vectorBase;
            }

            public struct TagData
            {
                public byte antennaId;
                public byte channel;
                public byte[] epc;
                public byte epcLen;
                public uint freq;
                public ushort pc;
                public sbyte rssi;
                public sbyte scaledRssi;
                public ushort timestamp;
            }

            public struct TraceTagData
            {
                public int antennaID;
                public byte[] epc;
                public int epcLen;
                public int rssi;
                public int scaledRssi;
            }

            public struct TriggerReadData
            {
                public int antennaID;
                public byte[] epc;
                public int epcLen;
                public int rssi;
                public int scaledRssi;
                public bool sensor;
                public int source;
            }
        }
    }





