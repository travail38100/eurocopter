

.. index::
   pair: API ; Lecteur Nordic
   pair: API ; SetUsbAutoConnect
   pair: API; SetAccessPassword

.. _fct_utilisees_api_nordic:

====================================
Fonctions utilisées de l'API Nordic
====================================

.. contents::
   :depth: 3

L'API Nordic NurApiDotNet
=========================


.. toctree::
   :maxdepth: 4

   NurApiDotNet.rst


:download:`Télécharger le document décrivant l'API DotNet <NurApi_DotNET_Documentation.chm>`

Quelques exemples de l'API par le constructeur
----------------------------------------------

:download:`Télécharger le document Word décrivant l'API Nordic <ProgrammingNURWithdotNET2.docx>`.



Connexion au lecteur
====================

.. toctree::
   :maxdepth: 4

   connexion/index


Ecriture de tags
================================

.. toctree::
   :maxdepth: 4

   ecriture/index


Lecture de tags
================================

.. toctree::
   :maxdepth: 4

   lecture/index


Monza (cartes Monza)
====================

.. toctree::
   :maxdepth: 4

   monza/index


.. _gestion_mots_de_passe:

Gestion des mots de passe
=========================

SetAccessPassword
-----------------

::

    public void SetAccessPassword(uint passwd, bool secured, byte sBank, uint sAddress, byte[] sMask, uint newPasswd);
    public void SetAccessPassword(uint passwd, bool secured, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask, uint newPasswd);


SetAccessPasswordByEPC
----------------------

::

    public void SetAccessPasswordByEPC(uint passwd, bool secured, byte[] epc, uint newPasswd);


Structures de la bibliothèque
===============================

.. toctree::
   :maxdepth: 4

   structures/index


Fonctions utilitaires de la bibliothèque
========================================

.. toctree::
   :maxdepth: 4

   util/index








