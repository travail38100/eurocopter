

.. index::
   pair: API ; Tag


.. _Tag:

==================================
NurApi.Tag class
==================================

.. contents::
   :depth: 5


::

    public class Tag : ICloneable
    {
        public byte antennaId;
        public byte channel;
        public uint frequency;
        public NurApi hApi;
        public ushort pc;
        public sbyte rssi;
        public sbyte scaledRssi;
        public ushort timestamp;
        public object userData;

        public Tag();
        public Tag(NurApi api);
        public Tag(NurApi.Tag src);
        public Tag(NurApi api, ref NurApi.TagData data);

        public static bool operator !=(NurApi.Tag left, NurApi.Tag right);
        public static bool operator ==(NurApi.Tag left, NurApi.Tag right);

        public byte[] epc { get; set; }

        public object Clone();
        public bool Compare(NurApi.Tag t);
        public override bool Equals(object obj);
        public uint GetAccessPassword(uint passwd, bool secured);
        public string GetEpcString();
        public override int GetHashCode();
        public uint GetKillPassword(uint passwd, bool secured);
        public void Inventory();
        public void Inventory(int rounds, int Q, int session);
        public bool IsNull();
        public void KillTag(uint passwd);
        public byte[] ReadTag(uint passwd, bool secured, byte rdBank, uint rdAddress, int rdByteCount);
        public void SetAccessPassword(uint passwd, bool secured, uint newPasswd);
        public void SetKillPassword(uint passwd, bool secured, uint newPasswd);
        public void SetLock(uint passwd, uint memoryMask, uint action);
        public override string ToString();
        public NurApi.TraceTagData TraceTag(int flags);
        public void WriteEPC(uint passwd, bool secured, byte[] newEpcBuffer);
        public void WriteTag(uint passwd, bool secured, byte wrBank, uint wrAddress, byte[] wrBuffer);
        public void WriteTag(uint passwd, bool secured, byte wrBank, uint wrAddress, byte[] wrBuffer, int wrBufferPos, int wrBufferLen);
    }
