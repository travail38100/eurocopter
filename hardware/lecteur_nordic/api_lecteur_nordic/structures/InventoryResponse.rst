

.. index::
   pair: API ; InventoryResponse


.. _InventoryResponse:

==================================
NurApi.InventoryResponse structure
==================================

.. contents::
   :depth: 5


::

    public struct InventoryResponse
    {
        public int collisions;
        public int numTagsFound;
        public int numTagsMem;
        public int Q;
        public int roundsDone;
    }

