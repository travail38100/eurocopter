

.. index::
   pair: API ; TagStorage


.. _TagStorage:

==================================
NurApi.TagStorage class
==================================

.. seealso::

   - :ref:`Tag`

.. contents::
   :depth: 5


::


    public class TagStorage : IEnumerable
    {
        public TagStorage();

        public int Count { get; }

        public NurApi.Tag this[byte[] epc] { get; }
        public NurApi.Tag this[int i] { get; }

        public bool AddTag(NurApi.Tag tag);
        public bool AddTag(NurApi.Tag tag, out NurApi.Tag storedTag);
        public void Clear();
        public int Copy(NurApi.TagStorage srcStorage);
        public IEnumerator GetEnumerator();
        public NurApi.Tag GetTag(byte[] epc);
        public NurApi.Tag GetTag(string epcStr);
        public bool HasTag(byte[] epc);
        public bool HasTag(NurApi.Tag tag);
        public bool HasTag(string epcStr);
        public bool TryGetTag(byte[] epc, out NurApi.Tag tag);
    }
