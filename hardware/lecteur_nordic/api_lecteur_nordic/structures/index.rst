

.. index::
   pair: API ; Structures


.. _api_nordic_structures:


================================
API Nordic structures
================================

.. toctree::
   :maxdepth: 5


   InventoryResponse
   Tag
   TagStorage

