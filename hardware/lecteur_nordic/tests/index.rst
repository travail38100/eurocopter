


.. index::
   pair: Nordic; Tests
   pair: Puissance; 8 Watts


.. _tests_lecteur_nordic:

==========================================
Tests du lecteur Nordic
==========================================



.. contents::
   :depth: 3


Introduction
============

Les tests du lecteur Nordic se font au moyen du logiciel de démonstration.

.. figure:: RFID_demo.png
   :align: center


Test de la l'inventaire
========================

.. warning:: Penser à baisser la puissance émise qui est à 500 watts au démarrage !!.
   8 Watts est largement suffisant.

Test de l'inventaire des tags.

.. figure:: RFID_start_inventory.png
   :align: center














