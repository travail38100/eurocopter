


.. index::
   pair: Lecteur; Nordic
   pair: Lecteur; Sampo


.. _lecteur_nordic:

======================================
Lecteur nordic Sampo
======================================


.. seealso::

   - http://www.nordicid.com/
   - http://www.nordicid.com/fra/
   - http://www.nordicid.com/fra/produits/?group=5
   
   

.. figure:: norsa868.jpg
   :align: center
   
   

.. figure:: lecteur_nordic.jpg
   :align: center   

.. toctree::
   :maxdepth: 3

   introduction
   api/index
   api_lecteur_nordic/index
   demo/index
   installation/index
   support/index
   tests/index









