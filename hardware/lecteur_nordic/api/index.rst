


.. index::
   pair: DotNet; NurAPI
   pair: Dll; NurAPI

.. _api_lecteur_nordic:

======================================
API du lecteur nordic (NUR/Sempo)
======================================

.. seealso::

   - :ref:`fct_utilisees_api_nordic`
   - http://www.orcanthus.com/fr/product_121/norsa868.htm
   - http://www.nordicid.com/eng/products/?group=5


.. contents::
   :depth: 3


.. figure:: nordic_api_dll.jpg
   :align: center

   :file:`C:\\Program Files\\Nordic ID\\RFID Configurator`


NURAPI.dll
==========

.. seealso::

   - :ref:`fct_utilisees_api_nordic`

NURAPI.dll includes low-level functionalities.


NurApi .NET
============

.. seealso::

   - :ref:`fct_utilisees_api_nordic`

NurApiDotNet.dll is intended to Windows .NET based RFID applications.

NUR API .NET consist easy-to-use functionalities for creating sophisticated
RFID applications for NUR module.








