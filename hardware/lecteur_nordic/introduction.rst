


.. index::
   pair: PID; 0112
   pair: VID; 04e6

.. _intro_lecteur_nordic:

======================================
Introduction Lecteur nordic
======================================

.. contents::
   :depth: 3



VendorID (VID) 04e6
===================

L'identifiant du vendeur pour le lecteur Nordic est *04e6*.



ProductID (PID) 0112
====================


L'identifiant de produit pour le lecteur Nordic est *0112*.









