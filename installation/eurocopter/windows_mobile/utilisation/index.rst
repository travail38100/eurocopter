

.. index::
   pair: Utilisation ; Windows Mobile Center


.. _utilisation_windows_mobile_center:

=====================================
Utilisation de Windows Mobile Center
=====================================


.. contents::
   :depth: 3


Introduction
============


Quand on connecte le PDA au PC par l'intermédiaire de la base "craddle"
la fenêtre suivante apparait.

.. figure:: windows_mobile_center.png
   :align: center
   
   
Cliquer sur parcourir le contenu du fichier
============================================

Pour accéder aux répertoires du PDA cliquer sur ``Parcourir le contenu``.

.. figure::  parcourir_contenu.png
   :align: center
   
   
Ordinateur Pocket PC
=====================

L'explorateur de fichiers est sur ``Ordinateur Pocket PC``.


.. figure::  ordinateur_pocket_pc.png
   :align: center
   

.. _appdata_pda_db_eurocopter:

Application Data\Eurocopter ``DB_Eurocopter.sqlite``
====================================================

La base de données Eurocopter se trouve sous le répertoire 
``Ordinateur\Pocket_PC\\\Application Data\Eurocopter``.

.. figure:: appdata/appdata_eurocopter.png


Program Files\mobileeurocopter
===============================

Le programme de gestion Eurocopter se trouve sous 
``Ordinateur\Pocket_PC\\\Program Files\mobileeurocopter``.

.. figure:: program_files/mobileeurocopter.png
   :align: center
   
   



    
