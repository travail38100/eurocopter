

.. index::
   pair: Windows ; Mobile Center


.. _install_windows_mobile_center:

=====================================
Installation de Windows Mobile Center
=====================================


.. contents::
   :depth: 3

Introduction
=============

Ce logiciel permet d'accéder aux répertoires du PDA et de faire les
opérations suivantes:

- copie de la base ``DB_Eurocopter.sqlite`` vers le répertoire 
  %APPDATA%\Eurocopter du PC Windows.

- mise à jour du logiciel mobileeurocopter.exe sur le PDA.


Installer le logiciel
======================

- cliquer Windows Mobile Device Center
  drvupdate-amd64.exe
  drvupdate-x86.exe
  

Utilisation
============

.. toctree::
   :maxdepth: 3
   
   utilisation/index
   
   
   
