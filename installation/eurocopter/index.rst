

.. index::
   pair: Installation; Eurocopter
   pair: Base de données; port 8001
   pair: Windows ; Mobile Center


.. _installation_eurocopter:

==========================================
Installation des logiciels pour Eurocopter
==========================================


.. seealso::

   - :ref:`installation_commune`


.. contents::
   :depth: 3



Prérequis
=========

Il faut avoir installer les :ref:`composants communs <installation_commune>` 
avant d'installer ceux pour Eurocopter.


Installation du logiciel de synchronisation ActiveSync
=======================================================

.. toctree::
   :maxdepth: 5
   
   synchronisation/index
   

Installation du logiciel de synchronisation Windows Mobile Center
=================================================================

.. toctree::
   :maxdepth: 5
   
   windows_mobile/index
   
   


Gestion de la base de données Eurocopter
========================================

.. toctree::
   :maxdepth: 5
   
   database_management/index

