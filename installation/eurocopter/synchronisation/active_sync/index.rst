

.. index::
   pair: Active ; Sync
   ! ActiveSync


.. _install_active_sync:

=====================================
Installation de ActiveSync
=====================================


.. seealso::

   - http://fr.wikipedia.org/wiki/ActiveSync
   - :ref:`install_windows_mobile_center`
   - :ref:`installation_synchro_eurocopter`   
   - http://www.softpedia.com/get/Internet/Telephony-SMS-GSM/ActiveSync.shtml
   - http://www.softpedia.com/dyn-postdownload.php?p=18828&t=5&i=1


.. contents::
   :depth: 3

Introduction
=============

.. seealso::

   - :ref:`intro_synchronisation`


Ce logiciel permet d'accéder aux répertoires du PDA et de faire les
opérations suivantes:

- copier la base :file:`DB_Eurocopter.sqlite` vers le répertoire 
  :file:`/My Documents/Eurocopter` du PC Windows.


.. note:: Sur le PDA, le répertoire :file:`\My Documents` est obtenu par la 
   commande C# Environment.GetFolderPath(Environment.SpecialFolder.Personal);


Installer le logiciel
======================

Aller sous le répertoire :file:`/LivraisonTools/install_tools/eurocopter/active_sync``
et taper la commande::


    setup.msi
  

Manuel utilisateur d'ActiveSync
================================

.. toctree::
   :maxdepth: 3
   
   manuel_utilisateur/index
   
      
   
   
