

.. index::
   pair: ActiveSync ; Utilisation
   pair: ActiveSync; Synchronisation


.. _utilisation_active_sync:
.. _manu_util_active_sync:

=================================
Manuel utilisateur d'ActiveSync
=================================

.. seealso::

   - :ref:`intro_synchronisation`

.. contents::
   :depth: 3



Préconditions
=============

- avoir installé ``ActiveSync``
- le PDA doit être placé sur son socle de rechargement, la communication 
  entre le PC et le PDA se faisant au moyen d'un câble USB.
   
   
Fenêtre de synchronisation d'ActiveAsync
=========================================

   
.. figure:: fenetre_synchronisation.png
   :align: center
   
   
Attention : cette fenêtre stipule que le fichier de droite (en l’occurrence 
celui du PDA) sera remplacé. 

Il est donc recommandé d’être vigilant quant à l’ordre des opérations à 
effectuer : des modifications effectuées sur PC peuvent amener à écraser des 
données d’historique présentes sur le PDA. 

.. warning:: Il demeure donc préférable d’utiliser l’interface de gestion 
   lorsque le PDA est en chargement/synchronisation sur son socle. 

   
