

.. index::
   pair: Installation; Synchronisation


.. _installation_synchro_eurocopter:

==============================================
Installation des logiciels de synchronisation
==============================================


.. contents::
   :depth: 3


.. _intro_synchronisation:

Introduction à la synchronisation 
=================================  

La base de données du PDA peut être synchronisée dans les sens suivants:

- PDA vers PC : pour visualiser sur le PC l’historique des mélanges, 
  des ouvertures de pot, des données concernant les vacations,
- PC vers PDA : pour mettre à jour les opérateurs, les composants, les types de 
  mélange possibles, etc.

Trois cas de figure peuvent se présenter lors de la synchronisation de la base:

1. La synchronisation est transparente, le fichier du PDA est copié sur le PC. 
   Toutes les données sont maintenant accessibles via l’interface de gestion.

2. La base a été modifiée sur le PC (exemple : ajout d’un opérateur, purge de 
   l’historique des mélanges, etc.). 
   Il faut alors remplacer la base de données du PDA avec la version locale du 
   fichier. 

3. La base a été modifiée sur le PDA, mais également sur le PC. 
   Dans ce cas, un des deux fichiers sera remplacé, à l’appréciation de 
   l’utilisateur. 


Installation d'ActiveSync sous Windows XP
==========================================

.. toctree::
   :maxdepth: 3
   
   active_sync/index


Installation de Windows Mobile Center sous Windows 7, Windows 8
===============================================================


.. toctree::
   :maxdepth: 3
   
   windows_mobile/index
