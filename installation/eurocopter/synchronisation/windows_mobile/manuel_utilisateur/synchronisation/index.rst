

.. index::
   pair: Synchronisation ; Windows Mobile Center
   pair: Variable; PATH_DB_EUROCOPTER
   pair: PATH_DB_EUROCOPTER; DB_Eurocopter.sqlite
   pair: Synchronisation ; PC
   pair: Synchronisation ; PDA


.. _synchronisation_base_de_donnees:

=====================================================
Synchronisation des bases de données entre PC et PDA
=====================================================

.. contents::
   :depth: 3
   
   
Ecran d'entrée pour le paramétrage de la synchronisation
========================================================

.. figure:: parametres.png
   :align: center



Eléments à synchroniser
========================

.. figure:: parametres.png
   :align: center



.. _synchro_bases_pc_pda:

Synchronisation des bases de données entre PC et PDA
====================================================

.. seealso::

   - :ref:`emplacement_db_eurocopter`

Les fichiers à synchroniser entre PC et PDA.

Sur le PC, on indique l'emplacement de la base de données Eurocopter au moyen
de la variable d'environnement :envvar:`PATH_DB_EUROCOPTER`

.. figure:: synchro_fichiers.png
   :align: center


Mise en évidence de la synchronisation:

- connecter le PDA au PC via sa base (connection par USB)
- lancer windows mobile center et établir une connection
- ajouter un nouvel utilisateur dans la base de données au moyen de l'interface
  de gestion des utilisateurs
- on voit que windows mobile center se synchronise avec le PDA
- on peut vérifier que nous avons bien un nouvel utilisateur dans la combo des
  opérateurs.
   

   



    
