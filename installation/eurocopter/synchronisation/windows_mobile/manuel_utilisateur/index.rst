

.. index::
   pair: Manuel utilisateur ; Windows Mobile Center


.. _utilisation_windows_mobile_center_bis:
.. _windows_mobile_center:
.. _manu_util_windows_mobile_center:


============================================
Manuel utilisateur de Windows Mobile Center
============================================


.. contents::
   :depth: 3


Préconditions
=============

- avoir installé ``Windows Mobile Center``
- le PDA doit être placé sur son socle de rechargement, la communication 
  entre le PC et le PDA se faisant au moyen d'un câble USB.
      

Introduction
============


Quand on connecte le PDA au PC par l'intermédiaire de son socle de base
la fenêtre suivante apparait.

.. figure:: windows_mobile_center.png
   :align: center
   
   
Cliquer sur parcourir le contenu du fichier
============================================

Pour accéder aux répertoires du PDA cliquer sur ``Parcourir le contenu``.

.. figure::  parcourir_contenu.png
   :align: center
   
   
Ordinateur Pocket PC
=====================

L'explorateur de fichiers est sur ``Ordinateur Pocket PC``.


.. figure::  ordinateur_pocket_pc.png
   :align: center
   

.. _appdata_pda_db_eurocopter_bis:
.. _mydocuments_db_eurocopter:


:file:`My Documents/Eurocopter/DB_Eurocopter.sqlite`
=======================================================

La base de données Eurocopter se trouve sous le répertoire 
:file:`Ordinateur/Pocket_PC/My Documents/Eurocopter`.


::

    // Avec Windows Mobile Center, la synchronisation entre le PDA et le PC se fait sur le répertoire \My Documents\
    // qui correspond en C# " à Environment.GetFolderPath(Environment.SpecialFolder.Personal)
    // ATTENTION il existe bien une répertoire Personal sous \My Documents\ mais en C# il est accessible en faisant
    // Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Personal") !!!
    // Du côté PC le fichier se retrouve dans 
    string repertoire_my_documents = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
    try
    {
        string message = string.Format("repertoire_my_documents: {0}", repertoire_my_documents);
        // MessageBox.Show(message);

        // On crée un répertoire Eurocopter sous \My Documents
        //==============================================================
        string dirMyDocumentsEurocopter = Path.Combine(repertoire_my_documents, "Eurocopter");
        if (!Directory.Exists(dirMyDocumentsEurocopter))
        {
            Directory.CreateDirectory(dirMyDocumentsEurocopter);
        }

        nom_base_de_donnees = Path.Combine(dirMyDocumentsEurocopter, "DB_Eurocopter.sqlite");

        message = string.Format("La base a bien été trouvé: {0}", nom_base_de_donnees);
        // MessageBox.Show(message);
    }
    catch (Exception ex)
    {
        string message = string.Format("pb in {0}", ex.Message);
        MessageBox.Show("Problème avec la base de données :" + message, "Erreur");
        Log.Write("Problème avec la base de données :" + message + Environment.NewLine + ex.StackTrace, false);
    }
    
    

.. figure:: mydocuments/db_eurocopter.png
   :align: center


:file:`Program Files/mobileeurocopter`
=======================================

Le programme de gestion Eurocopter se trouve sous 
:file:`Ordinateur/Pocket_PC/Program Files/mobileeurocopter`.

.. figure:: program_files/mobileeurocopter.png
   :align: center
   
   
Synchronisation des bases de données entre PC et PDA
=====================================================

.. toctree::
   :maxdepth: 3
   
   synchronisation/index
   
   

   



    
