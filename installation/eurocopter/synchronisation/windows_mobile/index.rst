

.. index::
   pair: Windows ; Mobile Center


.. _install_synchro_windows_mobile_center:

=====================================
Installation de Windows Mobile Center
=====================================

.. seealso::

   - http://fr.wikipedia.org/wiki/Gestionnaire_de_p%C3%A9riph%C3%A9riques_mobiles
   - :ref:`install_active_sync`
   - :ref:`installation_synchro_eurocopter`
   - http://www.softpedia.com/get/Internet/Telephony-SMS-GSM/Microsoft-Windows-Mobile-Device-Center.shtml
   - http://www.softpedia.com/dyn-postdownload.php?p=181562&t=0&i=2

   

.. contents::
   :depth: 3

Introduction
=============

.. seealso::

   - :ref:`intro_synchronisation`


Ce logiciel permet d'accéder aux répertoires du PDA et de faire les
opérations suivantes:

- copier la base :file:`DB_Eurocopter.sqlite` vers le répertoire 
  :file:`\My Documents\Eurocopter` du PC Windows.

- mettre à jour du logiciel mobileeurocopter.exe sur le PDA.


.. note:: Le répertoire :file:`\My Documents` est obtenu par la commande C#
   Environment.GetFolderPath(Environment.SpecialFolder.Personal);


Installer le logiciel
======================

- cliquer Windows Mobile Device Center
  drvupdate-amd64.exe
  drvupdate-x86.exe
  

Manuel utilisateur de Windows Mobile Center
===========================================

.. toctree::
   :maxdepth: 3
   
   manuel_utilisateur/index
   
   
   
