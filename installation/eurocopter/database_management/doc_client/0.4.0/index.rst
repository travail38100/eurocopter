

.. index::
   pair: Installation; Application Django (0.4.0)


.. _installation_appli_django_0.4.0:

=======================================================================================================================
Documentation ``cliente`` concernant l'installation de l'application Django gérant la base de données Eurocopter 0.4.0
=======================================================================================================================


.. contents::
   :depth: 3

Document au format PDF
=======================
   

:download:`Installation de l'application Web Django 0.4.0 <InstallationApplicationDjangoEurocopter_XSPC3Y385.pdf>`



Document au format doc
=======================
   

:download:`Installation de l'application Web Django 0.4.0 <InstallationApplicationDjangoEurocopter_XSPC3Y385.doc>`

