

.. index::
   pair: Serveur; Web
   pair: Local; Web
   pair: DB_Eurocopter.sqlite; Web
   pair: WORKON_HOME; Python
   

.. _lancer_server_web_local:
.. _installation_db_eurocopter:

=======================================================
Installation de la base de données DB_Eurocopter.sqlite
=======================================================


.. contents::
   :depth: 3

Préconditions
=============

WORKON_HOME
-----------

.. seealso::
 
   - :ref:`workon_home`


Introduction
============

Pour installer la base de données ``DB_Eurocopter``:

- aller sous le répertoire :file:`/Livraison_Eurocopter/django_db_eurocopter/Installer`
- cliquer sur l'installateur ``BD_Eurocopter_0.X.exe``

Après installation, 2 icônes sont rajoutées au bureau

- une icône de lancement de la base locale
- une icône de gestion de la base de données.

.. figure:: icones_lancement.png
   :align: center


Manuel utilisateur de la base DB_Eurocopter.sqlite
==================================================

.. seealso::

   - :ref:`manu_utilisateur_base_eurocopter`
   
   
