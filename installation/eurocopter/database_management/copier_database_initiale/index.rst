

.. _copier_DB_Eurocopter:

==========================================================
Copier la base de données initiale DB_Eurocopter.sqlite
==========================================================

.. seealso::

   - :ref:`appdata_pda_db_eurocopter`



.. contents::
   :depth: 3

Prérequis
=========

.. seealso::

   - :ref:`var_path_db_eurocopter`   
   - :ref:`installation_synchro_eurocopter`

1) Paramétrage du logiciel de synchronisation
-----------------------------------------------

Un :ref:`logiciel de synchronisation <installation_synchro_eurocopter>` doit 
avoir été installé (ActiveSync ou Windows Mobile Center).

Ce n'est qu'*après* l'installation de ce logiciel que l'on peut savoir où sera 
stockée la base de données ``DB_Eurocopter.sqlite``.


2) Initialisation de la variable d'environnment ``PATH_DB_EUROCOPTER``
----------------------------------------------------------------------
  
La variable d'environnement :envvar:`PATH_DB_EUROCOPTER` doit être initialisée 
avec le répertoire au dessus du dossier ``Eurocopter``)  
  
  Exemple: si la base doit être située sous le répertoire :file:`C:/Users/%USERNAME%/Documents/Documents sur PDA_Bartec/Eurocopter`
  alors la la variable d'environnement :envvar:`PATH_DB_EUROCOPTER`  doit être
  initialisée avec la valeur::
  
      C:\Users\%USERNAME%\Documents\Documents sur PDA_Bartec  


.. figure:: path_deb_eurocopter.png
   :align: center



Introduction
=============

Cas de la première mise en service
-----------------------------------

Cliquer sur le fichier de commandes :file:`copy_database_in_path_db_eurocopter.bat`

Un répertoire ``%PATH_DB_EUROCOPTER%\Eurocopter`` est créé qui contient la base
de données ``DB_Eurocopter.sqlite``.

**Grâce au logiciel de synchronisation la base de données sera mise à jour sur le PDA**.


