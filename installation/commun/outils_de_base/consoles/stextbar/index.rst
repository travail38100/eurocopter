
.. index::
   pair: Installation; stextbar
   pair: Console; stextbar
   
   
.. _stextbar:

===========================================
Installation stextbar
===========================================

.. seealso::

   - http://code.google.com/p/stexbar/

    
    
The ultimate extensions for Windows Explorer or: what Microsoft forgot to 
implement in the explorer.     
