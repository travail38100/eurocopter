
.. index::
   pair: Installation; Console-devel Z
   pair: Console; Console-devel Z
   
   

.. _console_devel_z:

===========================================
Installation console-devel Z
===========================================

.. seealso::

   - https://github.com/cbucher/console



This is a modified version of Console 2 for a better experience under 
Windows Vista/7/8 and a better visual rendering. 

Built packages are available here:

https://github.com/cbucher/console/wiki/Downloads


  

    
    
    
