
.. index::
   pair: Installation; Console2
   
   

.. _console_devel_2:

===========================================
Installation console 2
===========================================

.. seealso::

   - http://sourceforge.net/projects/console/
   - http://chocolatey.org/packages/Console2
    

.. contents::
   :depth: 3    
 
Introduction
============ 
    
Console is a Windows console window enhancement. 

Console features include: multiple tabs, text editor-like text selection, 
different background types, alpha and color-key transparency, configurable font,
different window styles.

    
Prérequis
==========

L'installation a marché après l'installation du DotNet Framework 2.0.

Installation
=============

::

    install_console2.bat
    
    
