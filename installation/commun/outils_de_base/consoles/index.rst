
.. index::
   pair: Installation; Consoles
      

.. _install_consoles:

===========================================
Installation d'une des consoles suivantes
===========================================

.. contents::
   :depth: 3

Prérequis
=========


- DotNet 2.0
- DotNet 4.0


L'installation de :ref:`console devel 2 <console_devel_2>` se passe correctement 
après l'installation des :ref:`composants DotNet <installation_dotnet>`.


Différents types de console
============================


.. toctree::
   :maxdepth: 3
   
   
   console_devel_2/index
   console_devel_z/index
   stextbar/index
      
    
    
    
