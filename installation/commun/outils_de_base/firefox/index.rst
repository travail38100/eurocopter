

.. index::
   pair: Installation; Firefox



.. _installation_firefox:

===================================================
Installation de Firefox + extension sqlitemanager
===================================================



.. contents::
   :depth: 3


Firefox 
=======


.. seealso::

   - http://www.mozilla.org/fr/firefox/new/
   - http://www.mozilla.org/fr/download/?product=firefox-24.0&os=win&lang=fr


Utilisé par Django.


Sqlitemanager
=============


Permet de voir le contenu d'une base de données sqlite3.



    
    
    
    
