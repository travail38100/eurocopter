
.. index::
   pair: Installation; Gimp


.. _gimp_instalaltion:

===========================================
Installation de gimp
===========================================


.. contents::
   :depth: 3
   

Intérêt de GIMP
================

Permet de prendre des images pour:

- produire la documentation
- décrire les problèmes


Installation sous Windows
==========================


.. seealso::

   - http://gimp-win.sourceforge.net/stable.html
   
   
GIMP requires Windows XP with Service Pack 3, or a newer version of Windows 
to run.

This version of GIMP also requires a CPU with SSE instruction set 
(Pentium III, Athlon XP or better).    


      
    
    
    
