

.. index::
   pair: Installation; DotNet



.. _installation_dotnet:

===================================================
Installation de DotNet
===================================================


.. seealso::

   - http://en.wikipedia.org/wiki/.NET_Framework_version_history#.NET_Framework_4
   - http://www.commentcamarche.net/download/telecharger-34055209-microsoft-net-framework-2-0-x86

.. contents::
   :depth: 3

Besoin
======

On a besoin du framework DotNet 4.0 car le programme est écrit en C#4:

On a aussi besoin du Framework 2.0 pour les programmes suivants:

- :ref:`path_editor`
- :ref:`console_devel_2`

Présentation
=============

Le .NET Framework est le modèle de programmation complet et cohérent de
Microsoft pour la génération d'applications proposant une expérience utilisateur
visuellement surprenante, des communications sûres et fluides, ainsi que la
capacité à modéliser toute une gamme de processus métiers.

.NET Framework 4 fonctionne parfaitement en parallèle à d'anciennes versions
Framework. Les applications basées sur des versions précédentes du Framework,
quant à elles, continueront de s'exécuter sur la version ciblée par défaut.



Téléchargement de la version autonome
======================================

- http://www.microsoft.com/fr-be/download/details.aspx?id=17718


Le package redistribuable Microsoft .NET Framework 4 installe le runtime .NET
Framework et les fichiers associés requis pour exécuter et développer des
applications pour cibler .NET Framework 4.




    
    
    
    
