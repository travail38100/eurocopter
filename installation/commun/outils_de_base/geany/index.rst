


==================================
Geany
==================================


.. seealso:: 

   - https://secure.wikimedia.org/wikipedia/en/wiki/Geany
   - http://www.geany.org/Main/HomePage
    
  
Geany is a lightweight cross-platform GTK+ text editor based on Scintilla 
and including basic Integrated Development Environment (IDE) features. 

It is designed to have short load times, with limited dependency on separate 
packages or external libraries. It is available for a wide range of operating 
systems, such as BSD, Linux, Mac OS X[3], Solaris and Windows. 

Among the supported programming languages are C, Java, JavaScript, PHP, HTML, 
CSS, Python, Perl, Ruby, Pascal and Haskell and reStructuredText !

It is free software licensed under the terms of the GNU GPL version 2 or later

  
    
