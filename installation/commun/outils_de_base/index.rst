

.. index::
   pair: Installation; Base


.. _installation_de_base:

================================
Installation des outils de base
================================


.. toctree::
   :maxdepth: 3

   dotnet/index
   path_editor/index   
   consoles/index
   firefox/index
   tab_explorer/index
   env_editor/index   
   geany/index
   gimp/index
   7zip/index
   
    
    
   







