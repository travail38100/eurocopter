

.. index::
   pair: Installation ; Env editor
   pair: Environment ; Editor



.. _env_editor:


================================
Installation de Env editor
================================

.. seealso::

   - http://www.rapidee.com/en/about
   - http://www.rapidee.com/en/download


.. contents::
   :depth: 3


Besoin
======

On a besoin de Env Editor pour pouvoir ajouter facilement des variables
d'environnement telles que:

- la :ref:`variable d'environnement WORKON_HOME <workon_home>`
 
 
Sous Windows XP le répertoire %USERPROFILES% est de la forme 
``c:\\Documents And Settings\\Admin`` par exemple ce qui pose des problèmes
lorsque l'on veut appeler un exécutable situé sous ce répertoire ce qui est
le cas lorsqu'on utilise un environnement virtuel python.
Dans ce cas il faut redéfinir la variable d'environnement ``WORKON_HOME`` 
pour qu'elle point sur un chemin ne contenant pas de caractères ' ', par exemple
``c:\projects\envs``.



Présentation
=============

Rapid Environment Editor (RapidEE) is an environment variables editor. 

It includes an easy to use GUI and replaces the small and inconvenient 
Windows edit box. 

RapidEE 8.x supports Windows XP, 2003, Vista, 2008, Windows 7 & Windows 8 
(including 64-bit versions). 

If you still use Windows NT or 2000, then use version 6.1. 

For Windows 9x or ME use version 2.1.


    
    
    
