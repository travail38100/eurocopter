
.. index::
   pair: Installation ; Python;
   pair: 2.7 ; Python;
   pair: Innosetup; Python tools
   pair: Variable d'environnement ; WORKON_HOME
   pair: Variable d'environnement ; PATH


.. _installation_python:

====================================================
Installation de Python et des outils de base Python
====================================================


.. contents::
   :depth: 3
   

1) installation de l'interpréteur Python 2.7 32 bits
=======================================================

.. seealso::

   - http://www.python.org/getit/

::

    python-2.7.5_32bits.msi



Modification du PATH pour pointer sur python.
Exemple: c:\Python27

Installation de l'interpréteur Python sous ``c:\python2.7``.

On se sert de SetupPathEditor pour mettre à jour le PATH (on rajoute ``c:\python27``)
 

Extrait du fichier Innosetup
-----------------------------

::

    [Registry]
    ; set PATH
    ; http://stackoverflow.com/questions/10687188/inno-setup-setting-java-environment-variable?rq=1
    ; http://www.jrsoftware.org/ishelp/index.php?topic=registrysection
    ; Root: HKCU; Subkey: "Environment"; ValueType:string; ValueName:"PATH"; ValueData:"{olddata};{pf}\Java\bin" ;Flags: preservestringtype
    ; set JAVA_HOME
    [Registry]
    Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Control\Session Manager\Environment"; ValueType:string; ValueName:"PATH"; ValueData:"{olddata};C:\Python27" ; Flags: preservestringtype; Check: NeedsAddPath('C:\Python27')
    Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Control\Session Manager\Environment"; ValueType:string; ValueName:"PATH"; ValueData:"{olddata};C:\Python27\Scripts" ; Flags: preservestringtype; Check: NeedsAddPath('C:\Python27\Scripts')
 
 

::

    [Code]
    function NeedsAddPath(Param: string): boolean;
    var
      OrigPath: string;
    begin
      if not RegQueryStringValue(HKEY_LOCAL_MACHINE,
        'SYSTEM\CurrentControlSet\Control\Session Manager\Environment',
        'Path', OrigPath)
      then begin
        Result := True;
        exit;
      end;
      // look for the path with leading and trailing semicolon
      // Pos() returns 0 if not found
      Result := Pos(';' + Param + ';', ';' + OrigPath + ';') = 0;
    end; 
    
    

2) installation des outils python 
======================================

Les outils de gestion de l'environnement virtuel sont au nombre de 4:

- setuptools
- pip
- virtualenv
- virtualenvwrapper-win

On utilise l'outil Innosetup_ pour installer ces 4 logiciels.


.. _workon_home:

La variable d'environnement ``WORKON_HOME``
---------------------------------------------


La variable d'environnement ``WORKON_HOME`` est mise à jour par le script 
d'installation et est initialisé à la valeur ``c:\project\python_env``.
Le répertoire est créé par le script ``mkdir_python_envs.bat`` appelé
par l'installateur.


.. _Innosetup:  https://www.kymoto.org/products/inno-script-studio/release-history

Extrait du fichier Innosetup d'installation
--------------------------------------------

::

    [Setup]
    ; Pour prendre en compte la valeur WORKON_HOME
    ChangesEnvironment=yes


    [Registry]
    ; set PATH
    ; http://stackoverflow.com/questions/10687188/inno-setup-setting-java-environment-variable?rq=1
    ; http://www.jrsoftware.org/ishelp/index.php?topic=registrysection
    ; Root: HKCU; Subkey: "Environment"; ValueType:string; ValueName:"PATH"; ValueData:"{olddata};{pf}\Java\bin" ;Flags: preservestringtype
    ; set JAVA_HOME
    [Registry]
    Root: HKCU; Subkey: "Environment"; ValueType:string; ValueName:"WORKON_HOME"; ValueData:"c:\project\python_envs" ;Flags: preservestringtype


    [Run]
    ; Filename: "{app}\add_path.exe"; Parameters: "{app}"; StatusMsg: "Adding installation path to search path..."  
    Filename: mkdir_python_envs.bat; Description: "Make python envs directory";
    Filename: {app}\setuptools-0.8\install_setuptools.bat; Description: "Installation de setuptools";  
    Filename: {app}\pip-1.3.1\install_pip.bat; Description: "Installation de pip"; 
    Filename: {app}\virtualenv-1.9.1\install_virtualenv.bat; Description: "Installation de virtualenv";  
    Filename: {app}\virtualenvwrapper-win-1.1.5\install_virtualenvwrapper.bat; Description: "Installation de virtualenvwrapper"; 



3) créer un environnement virtuel "eurocopter"
====================================================


On exécute le script ``build_virtual_eurocopter.bat``

::

    mkvirtualenv eurocopter
    
    
Remarques:

- si python2.7 n'est pas dans le PATH, utiliser la commande d'accès direct
  à mkvirtualenv::
  
      c:\python27\mkvirtualenv eurocopter
      

.. figure:: python_tools.png
   :align: center
   

        
La suite: Installation des composants Django
============================================

Une fois que l'on a créé l'environnement virtuel 'eurocopter' on peut installer
le framework Django.

.. seealso::

   - :ref:`installation_de_django`
   
   
        
            
    
