

.. index::
   pair: Python; Distributions


.. _sauver_distribs_python:

==========================================================================
Sauver les distributions locales à l'environnement virtuel ``Eurocopter``
==========================================================================

.. seealso::

   - :ref:`django_installation`
   
 
.. contents::
   :depth: 3 
 
But
====

**Dans les grosses boites, l'accès internet est souvent inexistant ou interdit**.

Il faut donc installer les distributions Python à partir des sources.


workon eurocopter + pip freeze > requirements.txt
==================================================

Dans un premier temps, on récupère la liste des distributions installées
dans l'environnement virtuel pour lequel on veut récupérer les archives.


::

    (eurocopter) >pip freeze > requirements.txt
    
::

    Django==1.5.1
    Jinja2==2.7
    MarkupSafe==0.18
    Pygments==1.6
    South==0.7.6
    Unipath==1.0
    diff-match-patch==20120106
    django-debug-toolbar==0.9.4
    django-import-export==0.1.3
    django-model-utils==1.3.1
    docutils==0.10
    pytz==2013b
    tablib==0.9.11


    
Sauvegarder les distributions dans un répertoire 'distributions'
================================================================

::

    mkdir distributions
    pip install --download=distributions -r requirements.txt
    
    
On obtient
==========

::

    (eurocopter) c:\Tmp>mkdir distributions
    

::
    

    (eurocopter) c:\Tmp>pip install --download=distributions -r requirements.txt
    
    
::

    
    Downloading/unpacking Django==1.5.1 (from -r requirements.txt (line 1))
      Downloading Django-1.5.1.tar.gz (8.0MB): 8.0MB downloaded
    Saved c:\tmp\distributions\django-1.5.1.tar.gz
    Running setup.py egg_info for package Django

      warning: no previously-included files matching '__pycache__' found under directory '*'
      warning: no previously-included files matching '*.py[co]' found under directory '*'
    Downloading/unpacking Jinja2==2.7 (from -r requirements.txt (line 2))
    Downloading Jinja2-2.7.tar.gz (377kB): 377kB downloaded
    Saved c:\tmp\distributions\jinja2-2.7.tar.gz
    Running setup.py egg_info for package Jinja2

    warning: no files found matching '*' under directory 'custom_fixers'
    warning: no previously-included files matching '*' found under directory 'docs\_build'
    warning: no previously-included files matching '*.pyc' found under directory 'jinja2'
    warning: no previously-included files matching '*.pyc' found under directory 'docs'
    warning: no previously-included files matching '*.pyo' found under directory 'jinja2'
    warning: no previously-included files matching '*.pyo' found under directory 'docs'
    Downloading/unpacking MarkupSafe==0.18 (from -r requirements.txt (line 3))
    Downloading MarkupSafe-0.18.tar.gz
    Saved c:\tmp\distributions\markupsafe-0.18.tar.gz
    Running setup.py egg_info for package MarkupSafe

    Downloading/unpacking Pygments==1.6 (from -r requirements.txt (line 4))
    Downloading Pygments-1.6.tar.gz (1.4MB): 1.4MB downloaded
    Saved c:\tmp\distributions\pygments-1.6.tar.gz
    Running setup.py egg_info for package Pygments

    Downloading/unpacking South==0.7.6 (from -r requirements.txt (line 5))
    Downloading South-0.7.6.tar.gz (91kB): 91kB downloaded
    Saved c:\tmp\distributions\south-0.7.6.tar.gz
    Running setup.py egg_info for package South

    Downloading/unpacking Unipath==1.0 (from -r requirements.txt (line 6))
    Downloading Unipath-1.0.tar.gz
    Saved c:\tmp\distributions\unipath-1.0.tar.gz
    Running setup.py egg_info for package Unipath

    Downloading/unpacking diff-match-patch==20120106 (from -r requirements.txt (line 7))
    Downloading diff-match-patch-20120106.tar.gz (54kB): 54kB downloaded
    Saved c:\tmp\distributions\diff-match-patch-20120106.tar.gz
    Running setup.py egg_info for package diff-match-patch

    Downloading/unpacking django-debug-toolbar==0.9.4 (from -r requirements.txt (line 8))
    Downloading django-debug-toolbar-0.9.4.tar.gz (150kB): 150kB downloaded
    Saved c:\tmp\distributions\django-debug-toolbar-0.9.4.tar.gz
    Running setup.py egg_info for package django-debug-toolbar

    no previously-included directories found matching 'example'
    Downloading/unpacking django-import-export==0.1.3 (from -r requirements.txt (line 9))
    Downloading django-import-export-0.1.3.tar.gz
    Saved c:\tmp\distributions\django-import-export-0.1.3.tar.gz
    Running setup.py egg_info for package django-import-export

    Downloading/unpacking django-model-utils==1.3.1 (from -r requirements.txt (line 10))
    Downloading django-model-utils-1.3.1.tar.gz
    Saved c:\tmp\distributions\django-model-utils-1.3.1.tar.gz
    Running setup.py egg_info for package django-model-utils

    Downloading/unpacking docutils==0.10 (from -r requirements.txt (line 11))
    Downloading docutils-0.10.tar.gz (1.6MB): 1.6MB downloaded
    Saved c:\tmp\distributions\docutils-0.10.tar.gz
    Running setup.py egg_info for package docutils

    warning: no files found matching 'MANIFEST'
    warning: no files found matching '*' under directory 'extras'
    warning: no previously-included files matching '.cvsignore' found under directory '*'
    warning: no previously-included files matching '*.pyc' found under directory '*'
    warning: no previously-included files matching '*~' found under directory '*'
    warning: no previously-included files matching '.DS_Store' found under directory '*'
    Downloading/unpacking pytz==2013b (from -r requirements.txt (line 12))
    Downloading pytz-2013b.tar.bz2 (199kB): 199kB downloaded
    Saved c:\tmp\distributions\pytz-2013b.tar.bz2
    Running setup.py egg_info for package pytz

    warning: no files found matching '*.pot' under directory 'pytz'
    warning: no previously-included files found matching 'test_zdump.py'
    Downloading/unpacking tablib==0.9.11 (from -r requirements.txt (line 13))
    Downloading tablib-0.9.11.tar.gz (571kB): 571kB downloaded
    Saved c:\tmp\distributions\tablib-0.9.11.tar.gz
    Running setup.py egg_info for package tablib

    Successfully downloaded Django Jinja2 MarkupSafe Pygments South Unipath diff-match-patch django-debug-toolbar django-import-export django-model-utils docutils pytz tablib
    Cleaning up...

    (eurocopter) c:\Tmp>dir distributions\
     Le volume dans le lecteur C s'appelle OS
     Le numéro de série du volume est 46AD-0A9C

     Répertoire de c:\Tmp\distributions

    30/10/2013  14:08    <DIR>          .
    30/10/2013  14:08    <DIR>          ..
    30/10/2013  14:07            54 099 diff-match-patch-20120106.tar.gz
    30/10/2013  14:07         8 028 963 Django-1.5.1.tar.gz
    30/10/2013  14:07           150 062 django-debug-toolbar-0.9.4.tar.gz
    30/10/2013  14:08            17 898 django-import-export-0.1.3.tar.gz
    30/10/2013  14:08            29 005 django-model-utils-1.3.1.tar.gz
    30/10/2013  14:08         1 602 552 docutils-0.10.tar.gz
    30/10/2013  14:07           377 603 Jinja2-2.7.tar.gz
    30/10/2013  14:07            11 748 MarkupSafe-0.18.tar.gz
    30/10/2013  14:07         1 423 161 Pygments-1.6.tar.gz
    30/10/2013  14:08           199 509 pytz-2013b.tar.bz2
    30/10/2013  14:07            91 528 South-0.7.6.tar.gz
    30/10/2013  14:08           571 410 tablib-0.9.11.tar.gz
    30/10/2013  14:07            29 306 Unipath-1.0.tar.gz
                  13 fichier(s)       12 586 844 octets
                   2 Rép(s)  232 048 009 216 octets libres

    (eurocopter) c:\Tmp>


Le script ``install_django.bat``
================================

.. literalinclude:: install_django.bat
      




        

