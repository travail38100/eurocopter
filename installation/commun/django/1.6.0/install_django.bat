
REM L'installation/Mise à jour se fait dans un environnement virtuel 'eurocopter'

%WORKON_HOME%\eurocopter\Scripts\pip install distributions\Django-1.6.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install distributions\diff-match-patch-20120106.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install distributions\sqlparse-0.1.10.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install distributions\django-debug-toolbar-0.11.0.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install distributions\tablib-0.9.11.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install distributions\django-import-export-0.1.4.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install distributions\django-model-utils-1.5.0.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install distributions\docutils-0.11.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install distributions\Jinja2-2.7.1.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install distributions\MarkupSafe-0.18.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install distributions\Pygments-1.6.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install distributions\pytz-2013.8.zip
%WORKON_HOME%\eurocopter\Scripts\pip install distributions\South-0.8.4.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install distributions\Unipath-1.0.tar.gz


pause

