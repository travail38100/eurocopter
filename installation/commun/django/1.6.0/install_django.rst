
.. index::
   pair: Django; install_django.bat
   


.. _install_django_2013:

====================================================================
Résultats de l'installation de Django et de packages Django (2013)
====================================================================



.. contents::
   :depth: 3
   


Le script ``install_django.bat``
================================

.. literalinclude:: install_django.bat
      


Resultats
==========

::

    ..\install_tools\commun\django>workon eurocopter
    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>install_django.bat
    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>rem  http://stackoverflow.com/questions/7225900/how-to-pip-install-packages-according-to-requirements-txt-from-a-local-directory?rq=1
    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>pip install distributions\Django-1.5.1.tar.gz
    Unpacking e:\id3\livraisontools\install_tools\commun\django\distributions\django-1.5.1.tar.gz
    Running setup.py egg_info for package from file:///e%7C%5Cid3%5Clivraisontools%5Cinstall_tools%5Ccommun%5Cdjango%5Cdistributions%5Cdjango-1.5.1.tar.gz
    warning: no previously-included files matching '__pycache__' found under directory '*'
    warning: no previously-included files matching '*.py[co]' found under directory '*'
    Installing collected packages: Django
    Running setup.py install for Django
    warning: no previously-included files matching '__pycache__' found under directory '*'
    warning: no previously-included files matching '*.py[co]' found under directory '*'
    Successfully installed Django
    Cleaning up...

    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>pip install distributions\diff-match-patch-20120106.tar.gz
    Unpacking e:\id3\livraisontools\install_tools\commun\django\distributions\diff-match-patch-20120106.tar.gz
    Running setup.py egg_info for package from file:///e%7C%5Cid3%5Clivraisontools%5Cinstall_tools%5Ccommun%5Cdjango%5Cdistributions%5Cdiff-match-patch-20120106.tar.gz
    Installing collected packages: diff-match-patch
    Running setup.py install for diff-match-patch
    Successfully installed diff-match-patch
    Cleaning up...

    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>pip install distributions\django-debug-toolbar-0.9.4.tar.gz
    Unpacking e:\id3\livraisontools\install_tools\commun\django\distributions\django-debug-toolbar-0.9.4.tar.gz
    Running setup.py egg_info for package from file:///e%7C%5Cid3%5Clivraisontools%5Cinstall_tools%5Ccommun%5Cdjango%5Cdistributions%5Cdjango-debug-toolbar-0.9.4.tar.gz
    no previously-included directories found matching 'example'
    Installing collected packages: django-debug-toolbar
    Running setup.py install for django-debug-toolbar
    no previously-included directories found matching 'example'
    Successfully installed django-debug-toolbar
    Cleaning up...

    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>pip install distributions\tablib-0.9.11.tar.gz
    Unpacking e:\id3\livraisontools\install_tools\commun\django\distributions\tablib-0.9.11.tar.gz
    Running setup.py egg_info for package from file:///e%7C%5Cid3%5Clivraisontools%5Cinstall_tools%5Ccommun%5Cdjango%5Cdistributions%5Ctablib-0.9.11.tar.gz
    Installing collected packages: tablib
    Running setup.py install for tablib
    Successfully installed tablib
    Cleaning up...

    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>pip install distributions\django-import-export-0.1.3.tar.gz
    Unpacking e:\id3\livraisontools\install_tools\commun\django\distributions\django-import-export-0.1.3.tar.gz
    Running setup.py egg_info for package from file:///e%7C%5Cid3%5Clivraisontools%5Cinstall_tools%5Ccommun%5Cdjango%5Cdistributions%5Cdjango-import-export-0.1.3.tar.gz
    Requirement already satisfied (use --upgrade to upgrade): tablib in c:\documents and settings\administrateur\envs\eurocopter\lib\site-packages (from django-import-export==0.1.3)
    Requirement already satisfied (use --upgrade to upgrade): Django>=1.4.2 in c:\documents and settings\administrateur\envs\eurocopter\lib\site-packages (from django-import-export==0.1.3)
    Requirement already satisfied (use --upgrade to upgrade): diff-match-patch in c:\documents and settings\administrateur\envs\eurocopter\lib\site-packages (from django-import-export==0.1.3)
    Installing collected packages: django-import-export
    Running setup.py install for django-import-export
    Successfully installed django-import-export
    Cleaning up...

    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>pip install distributions\django-model-utils-1.3.1.tar.gz
    Unpacking e:\id3\livraisontools\install_tools\commun\django\distributions\django-model-utils-1.3.1.tar.gz
    Running setup.py egg_info for package from file:///e%7C%5Cid3%5Clivraisontools%5Cinstall_tools%5Ccommun%5Cdjango%5Cdistributions%5Cdjango-model-utils-1.3.1.tar.gz
    Installing collected packages: django-model-utils
    Running setup.py install for django-model-utils

    Successfully installed django-model-utils
    Cleaning up...

    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>pip install distributions\docutils-0.10.tar.gz
    Unpacking e:\id3\livraisontools\install_tools\commun\django\distributions\docutils-0.10.tar.gz
    Running setup.py egg_info for package from file:///e%7C%5Cid3%5Clivraisontools%5Cinstall_tools%5Ccommun%5Cdjango%5Cdistributions%5Cdocutils-0.10.tar.gz
    warning: no files found matching 'MANIFEST'
    warning: no files found matching '*' under directory 'extras'
    warning: no previously-included files matching '.cvsignore' found under directory '*'
    warning: no previously-included files matching '*.pyc' found under directory '*'
    warning: no previously-included files matching '*~' found under directory '*'
    warning: no previously-included files matching '.DS_Store' found under directory '*'
    Installing collected packages: docutils
    Running setup.py install for docutils

    warning: no files found matching 'MANIFEST'
    warning: no files found matching '*' under directory 'extras'
    warning: no previously-included files matching '.cvsignore' found under directory '*'
    warning: no previously-included files matching '*.pyc' found under directory '*'
    warning: no previously-included files matching '*~' found under directory '*'
    warning: no previously-included files matching '.DS_Store' found under directory '*'
    Successfully installed docutils
    Cleaning up...

    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>pip install distributions\Jinja2-2.7.tar.gz
    Unpacking e:\id3\livraisontools\install_tools\commun\django\distributions\jinja2-2.7.tar.gz
    Running setup.py egg_info for package from file:///e%7C%5Cid3%5Clivraisontools%5Cinstall_tools%5Ccommun%5Cdjango%5Cdistributions%5Cjinja2-2.7.tar.gz
    warning: no files found matching '*' under directory 'custom_fixers'
    warning: no previously-included files matching '*' found under directory 'docs\_build'
    warning: no previously-included files matching '*.pyc' found under directory 'jinja2'
    warning: no previously-included files matching '*.pyc' found under directory 'docs'
    warning: no previously-included files matching '*.pyo' found under directory 'jinja2'
    warning: no previously-included files matching '*.pyo' found under directory 'docs'
    Downloading/unpacking markupsafe (from Jinja2==2.7)
    Cannot fetch index base URL https://pypi.python.org/simple/
    Could not find any downloads that satisfy the requirement markupsafe (from Jinja2==2.7)
    No distributions at all found for markupsafe (from Jinja2==2.7)
    Storing complete log in C:\Documents and Settings\Administrateur\pip\pip.log

    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>pip install distributions\MarkupSafe-0.18.tar.gz
    Unpacking e:\id3\livraisontools\install_tools\commun\django\distributions\markupsafe-0.18.tar.gz
    Running setup.py egg_info for package from file:///e%7C%5Cid3%5Clivraisontools%5Cinstall_tools%5Ccommun%5Cdjango%5Cdistributions%5Cmarkupsafe-0.18.tar.gz
    Installing collected packages: MarkupSafe
    Running setup.py install for MarkupSafe
    building 'markupsafe._speedups' extension
    ==========================================================================
    WARNING: The C extension could not be compiled, speedups are not enabled.
    Failure information, if any, is above.
    Retrying the build without the C extension now.
    ==========================================================================
    WARNING: The C extension could not be compiled, speedups are not enabled.
    Plain-Python installation succeeded.
    ==========================================================================
    Successfully installed MarkupSafe
    Cleaning up...

    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>pip install distributions\Pygments-1.6.tar.gz
    Unpacking e:\id3\livraisontools\install_tools\commun\django\distributions\pygments-1.6.tar.gz
    Running setup.py egg_info for package from file:///e%7C%5Cid3%5Clivraisontools%5Cinstall_tools%5Ccommun%5Cdjango%5Cdistributions%5Cpygments-1.6.tar.gz
    Installing collected packages: Pygments
    Running setup.py install for Pygments
    Installing pygmentize-script.py script to C:\Documents and Settings\Administrateur\Envs\eurocopter\Scripts
    Installing pygmentize.exe script to C:\Documents and Settings\Administrateur\Envs\eurocopter\Scripts
    Installing pygmentize.exe.manifest script to C:\Documents and Settings\Administrateur\Envs\eurocopter\Scripts
    Successfully installed Pygments
    Cleaning up...

    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>pip install distributions\pytz-2013b.zip
    Unpacking e:\id3\livraisontools\install_tools\commun\django\distributions\pytz-2013b.zip
    Running setup.py egg_info for package from file:///e%7C%5Cid3%5Clivraisontools%5Cinstall_tools%5Ccommun%5Cdjango%5Cdistributions%5Cpytz-2013b.zip
    warning: no files found matching '*.pot' under directory 'pytz'
    warning: no previously-included files found matching 'test_zdump.py'
    Installing collected packages: pytz
    Running setup.py install for pytz

    warning: no files found matching '*.pot' under directory 'pytz'
    warning: no previously-included files found matching 'test_zdump.py'
    Successfully installed pytz
    Cleaning up...
    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>pip install distributions\South-0.7.6.tar.gz
    Unpacking e:\id3\livraisontools\install_tools\commun\django\distributions\south-0.7.6.tar.gz
    Running setup.py egg_info for package from file:///e%7C%5Cid3%5Clivraisontools%5Cinstall_tools%5Ccommun%5Cdjango%5Cdistributions%5Csouth-0.7.6.tar.gz

    Installing collected packages: South
    Running setup.py install for South
    Successfully installed South
    Cleaning up...

    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>pip install distributions\Unipath-1.0.tar.gz
    Unpacking e:\id3\livraisontools\install_tools\commun\django\distributions\unipath-1.0.tar.gz
    Running setup.py egg_info for package from file:///e%7C%5Cid3%5Clivraisontools%5Cinstall_tools%5Ccommun%5Cdjango%5Cdistributions%5Cunipath-1.0.tar.gz
    Installing collected packages: Unipath
    Running setup.py install for Unipath
    Successfully installed Unipath
    Cleaning up...

    (eurocopter) E:\id3\LivraisonTools\install_tools\commun\django>
