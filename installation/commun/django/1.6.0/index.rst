
.. index::
   pair: Django; Installation (1.6.0)



.. _installation_de_django_1.6.0:

==============================================================
Installation de Django 1.6.0 et des packages associés en 2013
==============================================================

.. seealso::

   - :ref:`django_installation`
   
   

.. contents::
   :depth: 3


Sauvegarde des distributions locales à l'environnement virtuel de développement
===============================================================================


.. toctree::
   :maxdepth: 3
   
   sauver_distributions_locales



Installation de Django
=======================

.. seealso::

   -  http://stackoverflow.com/questions/7225900/how-to-pip-install-packages-according-to-requirements-txt-from-a-local-directory?rq=1


Ouvrir une console sur le répertoire install_tools\commun\django et taper
la commande suivante::

    install_django.bat

qui contient les commandes suivantes::

	pip install distributions\Django-1.5.1.tar.gz
	pip install distributions\diff-match-patch-20120106.tar.gz
	pip install distributions\django-debug-toolbar-0.9.4.tar.gz
	pip install distributions\tablib-0.9.11.tar.gz
	pip install distributions\django-import-export-0.1.3.tar.gz
	pip install distributions\django-model-utils-1.3.1.tar.gz
	pip install distributions\docutils-0.10.tar.gz
	pip install distributions\Jinja2-2.7.tar.gz
	pip install distributions\MarkupSafe-0.18.tar.gz
	pip install distributions\Pygments-1.6.tar.gz
	pip install distributions\pytz-2013b.zip
	pip install distributions\South-0.7.6.tar.gz
	pip install distributions\Unipath-1.0.tar.gz



Résultats de l'installation
===========================

.. toctree::
   :maxdepth: 3
   
   
   install_django
   
   

   

