
.. index::
   pair: Django; Installation (1.6.2)



.. _installation_de_django_1.6.2:

==============================================================
Installation de Django 1.6.2 et des packages associés en 2014
==============================================================

.. seealso::

   - :ref:`django_installation`
   
   

.. contents::
   :depth: 3



Le fichier requirements.txt
===========================

Ce fichier est obtenu au moyen de la commande::

    pip freeze > requirements.txt
    


.. note:: Il faut être dans l'environnement virtuel ``eurocopter``
   i.e activation au moyen de la commande ``workon eurocopter``


contenu du fichier requirements.txt
-----------------------------------

.. literalinclude:: requirements.txt
    


Sauvegarde des distributions locales à l'environnement virtuel de développement
===============================================================================




.. literalinclude:: sauvegarder_distributions.bat



Installation de Django
=======================


Ouvrir une console sur le répertoire install_tools\commun\django\1.6.2 et taper
la commande suivante::

    update_django_packages.bat

qui contient les commandes suivantes::


.. literalinclude:: update_django_packages.bat



   
   

   

