Django==1.6.2
Jinja2==2.7.2
MarkupSafe==0.18
Pygments==1.6
South==0.8.4
Unipath==1.0
diff-match-patch==20120106
django-debug-toolbar==1.0.1
django-import-export==0.2.0
django-model-utils==2.0.1
django-tables2==0.15.0
docutils==0.11
pytz==2013.9
six==1.5.2
sqlparse==0.1.11
tablib==0.9.11

