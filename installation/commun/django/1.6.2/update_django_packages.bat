
REM L'installation/Mise à jour se fait dans un environnement virtuel 'eurocopter'


%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\diff-match-patch-20120106.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\Django-1.6.2.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\django-debug-toolbar-1.0.1.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\django-import-export-0.2.0.zip
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\django-model-utils-2.0.1.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\django-tables2-0.15.0.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\docutils-0.11.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\Jinja2-2.7.2.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\MarkupSafe-0.18.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\Pygments-1.6.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\pytz-2013.9.tar.bz2
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\six-1.5.2.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\South-0.8.4.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\sqlparse-0.1.11.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\tablib-0.9.11.tar.gz
%WORKON_HOME%\eurocopter\Scripts\pip install --upgrade distributions\Unipath-1.0.tar.gz
    
    
pause

