

.. index::
   pair: Django; Installation


.. _installation_de_django_1:
.. _installation_de_django:

================================================
Installation de Django et des packages associés
================================================

.. seealso::

   - :ref:`django_installation`
   
   

.. contents::
   :depth: 3

Préambule
==========

Il faut installer au préalable:

- SetupPathEditor (ou édition manuelle du PATH)
- une console virtuelle (c'est plus pratique)
- python2.7 et les outils python (setuptools + pip + virtualenv + virtualenvwrapper )
- python2.7 et firefox doivent être dans le PATH 
- l'environnement virtuel "eurocopter" a été créé



Versions
=============

.. toctree::
   :maxdepth: 3
   

   1.6.2/index
   1.6.2_wheels/index
   1.6.0/index
   
   
