
.. index::
   pair: Django; Installation (2014)
   pair: Django; Installation (Wheels)
   ! Wheel



.. _installation_de_django_wheels:

======================================================================
Installation de Django et des packages associés avec wheels (en 2014)
======================================================================

.. seealso::

   - :ref:`django_installation`
   - http://pip.readthedocs.org/en/latest/index.html
   
   

.. contents::
   :depth: 3



pip wheel
=========

.. warning:: On ne peut pas encore l'utiliser.
   Il faut attendre la version 1.6 de pip.



Présentation de wheel
=====================



.. seealso::

   - http://pip.readthedocs.org/en/latest/user_guide.html#installing-from-wheels
   
  
``Wheel`` is a built, archive format that can **greatly speed** installation 
compared to building and installing from **source archives**. 

For more information, see the Wheel docs , PEP427, and PEP425

Pip prefers Wheels where they are available. 

To disable this, use the –no-use-wheel flag for pip install.

If no satisfactory wheels are found, pip will default to finding source archives.   


To build wheels for your requirements and all their dependencies to a local directory
=====================================================================================

::

    pip install wheel
    pip wheel --wheel-dir=./wheelhouse -r requirements.txt



::

    Building wheels for collected packages: Django, Jinja2, MarkupSafe, Pygments, South, Unipath, diff-match-patch, django-debug-toolbar, django-import-export, django-model-utils, django-tables2, docutils, pytz, six, sqlparse, tablib
    Skipping building wheel: https://pypi.python.org/packages/any/D/Django/Django-1.6.2-py2.py3-none-any.whl#md5=3bd014923e85df771b34d12c0ab3c9e1
    Running setup.py bdist_wheel for Jinja2
    Destination directory: c:\tmp\wheelhouse
    Running setup.py bdist_wheel for MarkupSafe
    Destination directory: c:\tmp\wheelhouse
    Running setup.py bdist_wheel for Pygments
    Destination directory: c:\tmp\wheelhouse
    Skipping building wheel: https://pypi.python.org/packages/2.7/S/South/South-0.8.4-py2.py3-none-any.whl#md5=6b2338a7590b43e37eae98203b50ef21
    Running setup.py bdist_wheel for Unipath
    Destination directory: c:\tmp\wheelhouse
    Running setup.py bdist_wheel for diff-match-patch
    Destination directory: c:\tmp\wheelhouse
    Skipping building wheel: https://pypi.python.org/packages/2.7/d/django-debug-toolbar/django_debug_toolbar-0.11-py2.py3-none-any.whl#md5=e30f6dfa174405c759a5d73663cd1c0d
    Running setup.py bdist_wheel for django-import-export
    Destination directory: c:\tmp\wheelhouse
    Running setup.py bdist_wheel for django-model-utils
    Destination directory: c:\tmp\wheelhouse
    Running setup.py bdist_wheel for django-tables2
    Destination directory: c:\tmp\wheelhouse
    Running setup.py bdist_wheel for docutils
    Destination directory: c:\tmp\wheelhouse
    Running setup.py bdist_wheel for pytz
    Destination directory: c:\tmp\wheelhouse
    Skipping building wheel: https://pypi.python.org/packages/3.3/s/six/six-1.5.2-py2.py3-none-any.whl#md5=ba32222ad0c5c7057a7c42e66e81289d
    Running setup.py bdist_wheel for sqlparse
    Destination directory: c:\tmp\wheelhouse
    Running setup.py bdist_wheel for tablib
    Destination directory: c:\tmp\wheelhouse
    Successfully built Jinja2 MarkupSafe Pygments Unipath diff-match-patch django-import-export django-model-utils django-tables2 docutils pytz sqlparse tablib


::

    (eurocopter) c:\Tmp>dir wheelhouse\
    Le volume dans le lecteur C s'appelle OS
    Le numéro de série du volume est 46AD-0A9C

    Répertoire de c:\Tmp\wheelhouse

    17/02/2014  09:57    <DIR>          .
    17/02/2014  09:57    <DIR>          ..
    17/02/2014  09:57            27 802 diff_match_patch-20120106-py27-none-any.whl
    17/02/2014  09:57            19 479 django_import_export-0.1.4-py27-none-any.whl
    17/02/2014  09:57            25 359 django_model_utils-1.5.0-py27-none-any.whl
    17/02/2014  09:57            60 619 django_tables2-0.15.0-py27-none-any.whl
    17/02/2014  09:57           509 217 docutils-0.11-py27-none-any.whl
    17/02/2014  09:56           296 683 Jinja2-2.7.1-py27-none-any.whl
    17/02/2014  09:57            17 473 MarkupSafe-0.18-cp27-none-win32.whl
    17/02/2014  09:57           664 246 Pygments-1.6-py27-none-any.whl
    17/02/2014  09:57           491 803 pytz-2013.8-py27-none-any.whl
    17/02/2014  09:57            33 357 sqlparse-0.1.10-py27-none-any.whl
    17/02/2014  09:57           384 506 tablib-0.9.11-py27-none-any.whl
    17/02/2014  09:57            10 094 Unipath-1.0-py27-none-any.whl
    12 fichier(s)        2 540 638 octets
    2 Rép(s)  212 401 106 944 octets libres


