
=========================================
Arborescence des fichiers d'installation
=========================================

::

    |   install.txt
    |   README.txt
    |   
    +---commun
    |   |   README.txt
    |   |   
    |   +---django
    |   |   |   install_django.bat
    |   |   |   README.txt
    |   |   |   
    |   |   \---distributions
    |   |           diff-match-patch-20120106.tar.gz
    |   |           Django-1.5.1.tar.gz
    |   |           django-debug-toolbar-0.9.4.tar.gz
    |   |           django-import-export-0.1.3.tar.gz
    |   |           django-model-utils-1.3.1.tar.gz
    |   |           docutils-0.10.tar.gz
    |   |           Jinja2-2.7.tar.gz
    |   |           MarkupSafe-0.18.tar.gz
    |   |           Pygments-1.6.tar.gz
    |   |           pytz-2013b.zip
    |   |           South-0.7.6.tar.gz
    |   |           tablib-0.9.11.tar.gz
    |   |           Unipath-1.0.tar.gz
    |   |           
    |   +---outils_de_base
    |   |   |   README.txt
    |   |   |   
    |   |   +---7zip
    |   |   |       7z920-x64.msi
    |   |   |       7z920.exe
    |   |   |       README.txt
    |   |   |       
    |   |   +---consoles
    |   |   |   |   README.txt
    |   |   |   |   
    |   |   |   +---console_devel_2
    |   |   |   |       Console2_32.exe
    |   |   |   |       Console2_64.exe
    |   |   |   |       install.bat
    |   |   |   |       README.txt
    |   |   |   |       
    |   |   |   +---console_devel_z
    |   |   |   |   |   README.txt
    |   |   |   |   |   
    |   |   |   |   +---ConsoleZ.x64.1.07.1
    |   |   |   |   |       console.chm
    |   |   |   |   |       Console.exe
    |   |   |   |   |       ConsoleHook.dll
    |   |   |   |   |       ConsoleHook32.dll
    |   |   |   |   |       ConsoleWow.exe
    |   |   |   |   |       FreeImage.dll
    |   |   |   |   |       FreeImagePlus.dll
    |   |   |   |   |       
    |   |   |   |   \---ConsoleZ.x86.1.07.1
    |   |   |   |           console.chm
    |   |   |   |           Console.exe
    |   |   |   |           ConsoleHook.dll
    |   |   |   |           FreeImage.dll
    |   |   |   |           FreeImagePlus.dll
    |   |   |   |           
    |   |   |   \---stextbar
    |   |   |           README.txt
    |   |   |           StExBar-1.8.4.msi
    |   |   |           StExBar64-1.8.4.msi
    |   |   |           
    |   |   +---diff_tools
    |   |   |       meld-1.7.2.0.zip
    |   |   |       
    |   |   +---firefox
    |   |   |       Firefox Setup 24.0.exe
    |   |   |       README.txt
    |   |   |       sqlitemanager-0.7.7-all.xpi
    |   |   |       
    |   |   +---gimp
    |   |   |       gimp-2.8.6-setup.exe
    |   |   |       
    |   |   +---path_editor
    |   |   |       README.txt
    |   |   |       SetupPathEditor.msi
    |   |   |       
    |   |   \---tab_explorer
    |   |           TabExplorer_setup.exe
    |   |           
    |   \---python
    |       |   python-2.7.5_32bits.msi
    |       |   README.txt
    |       |   
    |       +---pip
    |       |   |   README.txt
    |       |   |   
    |       |   +---pip-1.3.1
    |       |   |   |   AUTHORS.txt
    |       |   |   |   CHANGES.txt
    |       |   |   |   LICENSE.txt
    |       |   |   |   MANIFEST.in
    |       |   |   |   PKG-INFO
    |       |   |   |   PROJECT.txt
    |       |   |   |   README.rst
    |       |   |   |   setup.cfg
    |       |   |   |   setup.py
    |       |   |   |   
    |       |   |   +---docs
    |       |   |   |       configuration.txt
    |       |   |   |       cookbook.txt
    |       |   |   |       development.txt
    |       |   |   |       index.txt
    |       |   |   |       installing.txt
    |       |   |   |       logic.txt
    |       |   |   |       news.txt
    |       |   |   |       other-tools.txt
    |       |   |   |       quickstart.txt
    |       |   |   |       usage.txt
    |       |   |   |       
    |       |   |   +---pip
    |       |   |   |   |   basecommand.py
    |       |   |   |   |   baseparser.py
    |       |   |   |   |   cacert.pem
    |       |   |   |   |   cmdoptions.py
    |       |   |   |   |   download.py
    |       |   |   |   |   exceptions.py
    |       |   |   |   |   index.py
    |       |   |   |   |   locations.py
    |       |   |   |   |   log.py
    |       |   |   |   |   req.py
    |       |   |   |   |   runner.py
    |       |   |   |   |   status_codes.py
    |       |   |   |   |   util.py
    |       |   |   |   |   __init__.py
    |       |   |   |   |   __main__.py
    |       |   |   |   |   
    |       |   |   |   +---backwardcompat
    |       |   |   |   |       socket_create_connection.py
    |       |   |   |   |       ssl_match_hostname.py
    |       |   |   |   |       __init__.py
    |       |   |   |   |       
    |       |   |   |   +---commands
    |       |   |   |   |       bundle.py
    |       |   |   |   |       completion.py
    |       |   |   |   |       freeze.py
    |       |   |   |   |       help.py
    |       |   |   |   |       install.py
    |       |   |   |   |       list.py
    |       |   |   |   |       search.py
    |       |   |   |   |       show.py
    |       |   |   |   |       uninstall.py
    |       |   |   |   |       unzip.py
    |       |   |   |   |       zip.py
    |       |   |   |   |       __init__.py
    |       |   |   |   |       
    |       |   |   |   \---vcs
    |       |   |   |           bazaar.py
    |       |   |   |           git.py
    |       |   |   |           mercurial.py
    |       |   |   |           subversion.py
    |       |   |   |           __init__.py
    |       |   |   |           
    |       |   |   \---pip.egg-info
    |       |   |           dependency_links.txt
    |       |   |           entry_points.txt
    |       |   |           not-zip-safe
    |       |   |           PKG-INFO
    |       |   |           requires.txt
    |       |   |           SOURCES.txt
    |       |   |           top_level.txt
    |       |   |           
    |       |   \---setuptools-0.8
    |       |       |   CHANGES (links).txt
    |       |       |   CHANGES.txt
    |       |       |   CONTRIBUTORS.txt
    |       |       |   DEVGUIDE.txt
    |       |       |   easy_install.py
    |       |       |   ez_setup.py
    |       |       |   launcher.c
    |       |       |   MANIFEST.in
    |       |       |   PKG-INFO
    |       |       |   pkg_resources.py
    |       |       |   README.txt
    |       |       |   release.py
    |       |       |   setup.cfg
    |       |       |   setup.py
    |       |       |   
    |       |       +---docs
    |       |       |   |   conf.py
    |       |       |   |   easy_install.txt
    |       |       |   |   formats.txt
    |       |       |   |   index.txt
    |       |       |   |   Makefile
    |       |       |   |   merge-faq.txt
    |       |       |   |   merge.txt
    |       |       |   |   pkg_resources.txt
    |       |       |   |   python3.txt
    |       |       |   |   roadmap.txt
    |       |       |   |   setuptools.txt
    |       |       |   |   using.txt
    |       |       |   |   
    |       |       |   +---build
    |       |       |   |   \---html
    |       |       |   |       +---_sources
    |       |       |   |       |       easy_install.txt
    |       |       |   |       |       formats.txt
    |       |       |   |       |       index.txt
    |       |       |   |       |       merge-faq.txt
    |       |       |   |       |       merge.txt
    |       |       |   |       |       pkg_resources.txt
    |       |       |   |       |       python3.txt
    |       |       |   |       |       roadmap.txt
    |       |       |   |       |       setuptools.txt
    |       |       |   |       |       using.txt
    |       |       |   |       |       
    |       |       |   |       \---_static
    |       |       |   |               basic.css
    |       |       |   |               nature.css
    |       |       |   |               pygments.css
    |       |       |   |               
    |       |       |   +---_templates
    |       |       |   |       indexsidebar.html
    |       |       |   |       
    |       |       |   \---_theme
    |       |       |       \---nature
    |       |       |           |   theme.conf
    |       |       |           |   
    |       |       |           \---static
    |       |       |                   nature.css_t
    |       |       |                   pygments.css
    |       |       |                   
    |       |       +---setuptools
    |       |       |   |   archive_util.py
    |       |       |   |   cli-32.exe
    |       |       |   |   cli-64.exe
    |       |       |   |   cli-arm-32.exe
    |       |       |   |   cli.exe
    |       |       |   |   compat.py
    |       |       |   |   depends.py
    |       |       |   |   dist.py
    |       |       |   |   extension.py
    |       |       |   |   gui-32.exe
    |       |       |   |   gui-64.exe
    |       |       |   |   gui-arm-32.exe
    |       |       |   |   gui.exe
    |       |       |   |   package_index.py
    |       |       |   |   py24compat.py
    |       |       |   |   py27compat.py
    |       |       |   |   sandbox.py
    |       |       |   |   script template (dev).py
    |       |       |   |   script template.py
    |       |       |   |   site-patch.py
    |       |       |   |   ssl_support.py
    |       |       |   |   __init__.py
    |       |       |   |   
    |       |       |   +---command
    |       |       |   |       alias.py
    |       |       |   |       bdist_egg.py
    |       |       |   |       bdist_rpm.py
    |       |       |   |       bdist_wininst.py
    |       |       |   |       build_ext.py
    |       |       |   |       build_py.py
    |       |       |   |       develop.py
    |       |       |   |       easy_install.py
    |       |       |   |       egg_info.py
    |       |       |   |       install.py
    |       |       |   |       install_egg_info.py
    |       |       |   |       install_lib.py
    |       |       |   |       install_scripts.py
    |       |       |   |       launcher manifest.xml
    |       |       |   |       register.py
    |       |       |   |       rotate.py
    |       |       |   |       saveopts.py
    |       |       |   |       sdist.py
    |       |       |   |       setopt.py
    |       |       |   |       test.py
    |       |       |   |       upload.py
    |       |       |   |       upload_docs.py
    |       |       |   |       __init__.py
    |       |       |   |       
    |       |       |   \---tests
    |       |       |       |   doctest.py
    |       |       |       |   entries-v10
    |       |       |       |   py26compat.py
    |       |       |       |   server.py
    |       |       |       |   test_bdist_egg.py
    |       |       |       |   test_build_ext.py
    |       |       |       |   test_develop.py
    |       |       |       |   test_dist_info.py
    |       |       |       |   test_easy_install.py
    |       |       |       |   test_egg_info.py
    |       |       |       |   test_markerlib.py
    |       |       |       |   test_packageindex.py
    |       |       |       |   test_resources.py
    |       |       |       |   test_sandbox.py
    |       |       |       |   test_sdist.py
    |       |       |       |   test_test.py
    |       |       |       |   test_upload_docs.py
    |       |       |       |   win_script_wrapper.txt
    |       |       |       |   __init__.py
    |       |       |       |   
    |       |       |       \---indexes
    |       |       |           \---test_links_priority
    |       |       |               |   external.html
    |       |       |               |   
    |       |       |               \---simple
    |       |       |                   \---foobar
    |       |       |                           index.html
    |       |       |                           
    |       |       +---setuptools.egg-info
    |       |       |   |   dependency_links.txt
    |       |       |   |   entry_points.txt
    |       |       |   |   entry_points.txt.orig
    |       |       |   |   PKG-INFO
    |       |       |   |   requires.txt
    |       |       |   |   requires.txt.orig
    |       |       |   |   SOURCES.txt
    |       |       |   |   top_level.txt
    |       |       |   |   zip-safe
    |       |       |   |   
    |       |       |   \---EGG-INFO
    |       |       |           PKG-INFO
    |       |       |           
    |       |       +---tests
    |       |       |   |   api_tests.txt
    |       |       |   |   manual_test.py
    |       |       |   |   test_ez_setup.py
    |       |       |   |   test_pkg_resources.py
    |       |       |   |   
    |       |       |   \---shlib_test
    |       |       |           hello.c
    |       |       |           hello.pyx
    |       |       |           hellolib.c
    |       |       |           setup.py
    |       |       |           test_hello.py
    |       |       |           
    |       |       \---_markerlib
    |       |               markers.py
    |       |               __init__.py
    |       |               
    |       \---virtualenv
    |           |   README.txt
    |           |   
    |           +---virtualenv-1.9.1
    |           |   |   AUTHORS.txt
    |           |   |   LICENSE.txt
    |           |   |   MANIFEST.in
    |           |   |   PKG-INFO
    |           |   |   README.rst
    |           |   |   setup.cfg
    |           |   |   setup.py
    |           |   |   virtualenv.py
    |           |   |   
    |           |   +---bin
    |           |   |       rebuild-script.py
    |           |   |       refresh-support-files.py
    |           |   |       
    |           |   +---docs
    |           |   |   |   conf.py
    |           |   |   |   index.txt
    |           |   |   |   make.bat
    |           |   |   |   Makefile
    |           |   |   |   news.txt
    |           |   |   |   
    |           |   |   \---_theme
    |           |   |       \---nature
    |           |   |           |   theme.conf
    |           |   |           |   
    |           |   |           \---static
    |           |   |                   nature.css_t
    |           |   |                   pygments.css
    |           |   |                   
    |           |   +---scripts
    |           |   |       virtualenv
    |           |   |       
    |           |   +---virtualenv.egg-info
    |           |   |       dependency_links.txt
    |           |   |       entry_points.txt
    |           |   |       not-zip-safe
    |           |   |       PKG-INFO
    |           |   |       SOURCES.txt
    |           |   |       top_level.txt
    |           |   |       
    |           |   +---virtualenv_embedded
    |           |   |       activate.bat
    |           |   |       activate.csh
    |           |   |       activate.fish
    |           |   |       activate.ps1
    |           |   |       activate.sh
    |           |   |       activate_this.py
    |           |   |       deactivate.bat
    |           |   |       distribute_from_egg.py
    |           |   |       distribute_setup.py
    |           |   |       distutils-init.py
    |           |   |       distutils.cfg
    |           |   |       ez_setup.py
    |           |   |       site.py
    |           |   |       
    |           |   \---virtualenv_support
    |           |           distribute-0.6.34.tar.gz
    |           |           pip-1.3.1.tar.gz
    |           |           setuptools-0.6c11-py2.5.egg
    |           |           setuptools-0.6c11-py2.6.egg
    |           |           setuptools-0.6c11-py2.7.egg
    |           |           __init__.py
    |           |           
    |           \---virtualenvwrapper-win-1.1.5
    |               |   distribute_setup.py
    |               |   LICENSE
    |               |   MANIFEST.in
    |               |   PKG-INFO
    |               |   README.rst
    |               |   setup.cfg
    |               |   setup.py
    |               |   
    |               +---scripts
    |               |       add2virtualenv.bat
    |               |       cd-.bat
    |               |       cdproject.bat
    |               |       cdsitepackages.bat
    |               |       cdvirtualenv.bat
    |               |       folder_delete.bat
    |               |       lssitepackages.bat
    |               |       lsvirtualenv.bat
    |               |       mkvirtualenv.bat
    |               |       rmvirtualenv.bat
    |               |       setprojectdir.bat
    |               |       toggleglobalsitepackages.bat
    |               |       whereis.bat
    |               |       workon.bat
    |               |       
    |               \---virtualenvwrapper_win.egg-info
    |                       dependency_links.txt
    |                       not-zip-safe
    |                       PKG-INFO
    |                       requires.txt
    |                       SOURCES.txt
    |                       top_level.txt
    |                       
    +---eurocopter
    |   |   drvupdate-amd64.exe
    |   |   drvupdate-x86.exe
    |   |   README.txt
    |   |   
    |   \---sounds
    |           ding-s-dong.wav
    |           laser_shot_silenced.wav
    |           
    \---paban
        |   ExcelViewer.exe
        |   
        +---Nordic_ID_RFID_Configurator
        |       RFID_Configurator_Setup_v116.exe
        |       
        \---Nordic_ID_RFID_Demo
                RFID_Demo_Setup_v116.exe
            



