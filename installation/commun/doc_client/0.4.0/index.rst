

.. index::
   pair: Installateurs; Django (0.4.0)
   pair: Installateurs; Python (0.4.0)


.. _installation_python_django_0.4.0:

===============================================================================
Documentation ``cliente`` concernant l'installation de Django et Python 0.4.0)
===============================================================================


.. seealso::

   - :ref:`installation_eurocopter`
   - :ref:`installation_paban`



.. contents::
   :depth: 3

Document au format PDF
=======================
   

:download:`Installation de l'Environnement Python et Django <InstallationEnvironnementPythonDjango_XSPC3Y381.pdf>`



Document au format doc
=======================
   

:download:`Installation de l'Environnement Python et Django <InstallationEnvironnementPythonDjango_XSPC3Y381.doc>`
