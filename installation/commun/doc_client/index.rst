

.. index::
   pair: Installateurs; Django
   pair: Installateurs; Python


.. _installation_commune_client:

=======================================================================
Documentation ``cliente`` concernant l'installation de Django et Python
=======================================================================


.. seealso::

   - :ref:`installation_eurocopter`
   - :ref:`installation_paban`



.. toctree::
   :maxdepth: 3
   
   0.4.0/index
   
   



