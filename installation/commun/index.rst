

.. index::
   pair: Installation; Commune
   pair: workon; Eurocopter
   pair: Reindexation; Table
   pair: Index; Table



.. _installation_commune:

==========================================
Logiciels communs aux 2 installations
==========================================


.. seealso::

   - :ref:`installation_eurocopter`
   - :ref:`installation_paban`


.. contents::
   :depth: 3



Introduction
=============

La gestion des bases de données nécessite l'installation des outils suivants:

- le framework de gestion de base de données Django
- l'interpréteur Python
- SetupPathEditor.msi pour pouvoir éditer facilement les paths
- le logiciel Firefox avec son extension sqlitemanager



Arborescence des fichiers d'installation
=========================================

.. figure:: livraison_tools.png
   :align: center


.. toctree::
   :maxdepth: 3
   
   arbo_program_install
   

Installation des outils de base
===============================


.. toctree::
   :maxdepth: 3
   
   outils_de_base/index
   
   
Installation de l'interpréteur Python 2.7
===========================================

.. toctree::
   :maxdepth: 3
   
   python/index


Installation de Django
=============================

.. toctree::
   :maxdepth: 3
   
   django/index


Doc cliente sur Django et Python
================================

.. toctree::
   :maxdepth: 3
   
   doc_client/index






