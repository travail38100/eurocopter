


.. _installation_sur_carpobrutus_1_octobre_2013:

============================================================================================
Essai d'installation sur la machine Carpobrutus (Windows XP SP3) le mardi 1er octobre 2013
============================================================================================

.. contents::
   :depth: 3

Installation réussie des composants de base suivants
=====================================================

- DotNet 2
- DotNet 4
- Console 2
- TabExplorer
- PathEditor  
   
   
Installation réussie des composants suivants
============================================

- python 2.7
- setuptools
- pip
- virtualenv
- virtualenvwrapper

Installation réussie des composants Django suivants
===================================================

- distributions\Django-1.5.1.tar.gz
- distributions\diff-match-patch-20120106.tar.gz
- distributions\django-debug-toolbar-0.9.4.tar.gz
- distributions\tablib-0.9.11.tar.gz
- distributions\django-import-export-0.1.3.tar.gz
- distributions\django-model-utils-1.3.1.tar.gz
- distributions\docutils-0.10.tar.gz
- distributions\Jinja2-2.7.tar.gz
- distributions\MarkupSafe-0.18.tar.gz
- distributions\Pygments-1.6.tar.gz
- distributions\pytz-2013b.zip
- distributions\South-0.7.6.tar.gz
- distributions\Unipath-1.0.tar.gz





