
.. index::
   pair: Installation; 7 octobre 2013


.. _installation_sur_carpobrutus_7_octobre_2013:

============================================================================================
Installation sur la machine Carpobrutus le lundi 7 octobre 2013
============================================================================================

.. contents::
   :depth: 3


Installation de python
======================


Installation des outils python
==============================


Logiciel de synchronisation ActiveSync
=======================================

.. toctree::
   :maxdepth: 3
   
   activesync/index
      
   
Installation du gestionnaire de la base de données Eurocopter
==============================================================


.. warning:: La variable d'environnement PATH_DB_EUROCOPTER doit pointer
   sur un emplacement ayant la forme suivante 
   


.. figure:: path_db_eurocopter.png
   :align: center
   
   


   
     
