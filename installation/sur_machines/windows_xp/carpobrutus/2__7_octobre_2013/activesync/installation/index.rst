
.. index::
   pair: Installation; ActiveSync


.. _activesync_install:

============================================================================================
Installation d'ActiveSync 
============================================================================================

.. contents::
   :depth: 3



Etape 1
========

.. figure:: install_3.png
   :align: center
   
   
Etape 2
========

   
.. figure:: install_2.png
   :align: center
   
     
Etape 3
========     
     
.. figure:: install_1.png
   :align: center
   
       
