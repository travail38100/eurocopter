
.. index::
   pair: Carpobrutus; Synchronisation


.. _repertoire_activesync_7_octobre_2013:

============================================================================================
Répertoire de synchronisation d'ActiveSync 
============================================================================================

.. contents::
   :depth: 3



Répertoire de synchronisation sur le PC
========================================

.. seealso::

   - :ref:`appli_gestion_base_eurocopter`

Pour que l'application de gestion de la base de données puisse accéder à la
base, il faut que la variable d'environnement :envvar:`PATH_DB_EUROCOPTER`
soit correctement initialisé.



.. figure:: mydocuments.png
   :align: center
   

