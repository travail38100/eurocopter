

.. index::
   pair: Installation; Logiciels



.. _installation_des_logiciels:

==========================================
Installation de l'environnement logiciel
==========================================


.. contents::
   :depth: 3


Introduction
=============

Ce chapitre décrit la procédure d'installation des logiciels pour
les entreprises Paban et Eurocopter.

De façon générale, on va éviter de se connecter sur internet pour installer
les logiciels.



Logiciels
==========

.. toctree::
   :maxdepth: 5


   commun/index
   paban/index
   eurocopter/index



Test de l'installation sur les machines
========================================

.. toctree::
   :maxdepth: 5
   
   sur_machines/index
   
   
   

