
.. index::
   pair: Installation; Base de données (Paban)


.. _installation_db_paban:

=========================================
Installation de la base de données Paban
=========================================



.. contents::
   :depth: 3
   
   

Introduction
=============
   

La :ref:`base de données Paban <appli_db_paban>` ne comporte 
qu'une seule table la table de :ref:`livraison des pots <table_livraison_pots>`.

Pour installer la base de données Paban:

- aller sous le répertoire :file:`/Livraison_Paban/django_bd_paban/Installer`
- cliquer sur l'installateur 

Après installation, 2 icônes sont rajoutées au bureau

- une icône de lancement de la base locale
- une icône de gestion de la base de données.

.. figure:: icones_lancement.png
   :align: center


Utilisation de la base
======================

Pour gérer la base:


Lancer le serveur de gestion de la base de données
---------------------------------------------------

- cliquer sur l'icône permettant le lancement du serveur local de la base
  de données
  

.. figure:: icone_lancer_serveur_web_local.png
   :align: center
  
.. figure:: lancement_serveur_base_paban_locale.png
   :align: center
     
  
Lancer l'interface de gestion de la base de données
---------------------------------------------------
  
  
- cliquer sur l'icône permettant de gérer la base de données  

.. figure:: lancement_appli_base_paban.png
   :align: center
   
   
.. figure:: admin_base_paban.png
   :align: center
   



