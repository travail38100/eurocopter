
.. index::
   pair: Installation; Paban


.. _installation_paban:

==========================================
Installation des logiciels pour Paban
==========================================


.. seealso::

   - :ref:`installation_commune`
   - :ref:`appli_db_paban`


.. toctree::
   :maxdepth: 3
   
   base_de_donnees/index
   encodage_paban/index
   
   
