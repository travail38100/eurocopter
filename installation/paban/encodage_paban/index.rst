
.. index::
   pair: Installation; Base de données (Paban)
   pair: Icone; PC_EncodagePaban


.. _installation_encodage_paban:

=============================================
Installation du logiciel d'encodage des tags 
=============================================


.. contents::
   :depth: 3
      

Introduction
=============
   

Pour installer le logiciel d'encodage ``Paban``:

- aller sous le répertoire :file:`/Livraison_Paban/PC_Encodage_Paban/Installer`
- cliquer sur l'installateur :file:`Install_PC_EncodagePaban.exe` 
  
  
Le programme de lecture/encodage est lancé:

.. figure:: pc_encodage_paban.png
   :align: center
     

Après installation, 1 icône apparait sur le bureau:

.. figure:: icone_pc_encodage_paban.png
   :align: center


