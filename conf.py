# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
#
# Modeles de fichiers conf.py
# ============================
#
# - https://github.com/bashtage/sphinx-material/blob/master/docs/conf.py
# - https://github.com/bashtage/arch/blob/main/doc/source/conf.py
# - https://github.com/statsmodels/statsmodels/blob/main/docs/source/conf.py
#
# theme.conf
# ===========
# - https://github.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
#
import os
import platform
import sys
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

project = "Eurocopter"
html_title = project
html_short_title = project
author = "DevOps people"
html_logo = "logo_Eurocopter.jpg"
html_favicon = "logo_Eurocopter.jpg"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
master_doc = "index"
version = f"0.1.0"
release = version
today = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
source_suffix = ".rst"
language = None
exclude_patterns = ["_build", ".venv", ".git"]

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.viewcode",
    "myst_parser",
    "sphinx_markdown_tables",
    "sphinx_copybutton",
]
autosummary_generate = True
autoclass_content = "class"

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    "cli": ("https://gdevops.gitlab.io/tuto_cli/", None),
    "docu": ("https://gdevops.gitlab.io/tuto_documentation/", None),
}
extensions.append("sphinx.ext.todo")
todo_include_todos = True

# material theme options (see theme.conf for more information)
# https://github.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
html_theme_options = {
    "base_url": "https://travail38100.gitlab.io/eurocopter/",
    "repo_url": "https://gitlab.com/travail38100/eurocopter",
    "repo_name": project,
    "repo_type": "gitlab",
    "html_minify": False,
    "html_prettify": False,
    "css_minify": False,
    "globaltoc_depth": -1,
    # https://www.colorhexa.com/2173f3
    "theme_color": "#2173f3",
    "color_primary": "green",
    "color_accent": "cyan",
    "master_doc": False,
    "nav_title": f"{project} {release} ({today})",
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
    ],
    "heroes": {
        "index": "Eurocopter",
    },
    "table_classes": ["plain"],
}

language = "fr"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True

# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
# extensions.append("sphinx_design")
# html_css_files = ["https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/fontawesome.min.css"]

copyright = f"2012-2015, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"
