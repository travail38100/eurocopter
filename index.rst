

.. index::
   pair: Projets; PDEV2X238
   pair: Projets; Eurocopter

.. _pdev2x238_eurocopter:

======================================================================================
Projet RFID |version| traçabilité des pots de peinture Airbus Helicopters
======================================================================================


- :ref:`applis_eurocopter`
- :ref:`applis_paban`
- :ref:`genindex`
- :ref:`glossaires`


.. figure:: Airbus_helicopters_logo_2014.jpg
   :align: center

   Airbus Helicopters


=====


.. figure:: logo_Paban.png
   :align: center

   Le fournisseur Paban SAS



.. contents::
   :depth: 3


.. only:: paban or id3

    Applications Paban
    ==================

    .. toctree::
       :maxdepth: 3

       development/applis/paban/index
       specifications/applications/paban/index



.. only:: eurocopter or id3

    Applications Airbus Helicopters (Eurocopter)
    =============================================

    .. toctree::
       :maxdepth: 3

       development/applis/eurocopter/index
       specifications/applications/eurocopter/index


Exigences Eurocopter
====================

.. toctree::
   :maxdepth: 3

   exigences/index


Manuels utilisateur
====================

.. toctree::
   :maxdepth: 3

   manuels_utilisateur/manuels_utilisateur



.. only:: id3


    Spécifications
    =================

    .. toctree::
       :maxdepth: 6

       specifications/specifications


    Developpement du logiciel
    =========================

    .. toctree::
       :maxdepth: 3

       development/index


    Intervenants
    ============

    .. toctree::
       :maxdepth: 3

       intervenants


    Entreprises
    ===========

    .. toctree::
       :maxdepth: 3

       entreprises/index


    Hardware
    =================

    .. toctree::
       :maxdepth: 3

       hardware/index


    Réunions
    ============

    .. toctree::
       :maxdepth: 3

       reunions/index

    Techniques
    =================

    .. toctree::
       :maxdepth: 3

       techniques/index



    Installation
    =================

    .. toctree::
       :maxdepth: 3

       installation/index

    Tests
    =================

    .. toctree::
       :maxdepth: 3

       tests/index


    Livraisons
    =================

    .. toctree::
       :maxdepth: 3

       livraisons/index

Glossaires
============

.. toctree::
   :maxdepth: 3

   glossaires/index





