

.. index::
   pair: Spécifications; Base de données


.. _specs_base_de_donnees:

================================================================
Spécifications générales concernant base de données 
================================================================


.. contents::
   :depth: 3


Contexte
========

L'utilisation de bases de données relationnelles **embarquées** est une première
à id3 Technologies.


Spécifications
===============
 
 
.. toctree::
   :maxdepth: 3
   
   bd_eurocopter/index
   
   
    
