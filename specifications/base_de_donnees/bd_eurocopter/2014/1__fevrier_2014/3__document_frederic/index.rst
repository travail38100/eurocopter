

.. index::
   pair: Base de données ; Specs (Frédéric 26 février 2014)

.. _specs_26_fevrier_2014:

==========================================================================
Nouvelles spécifications concernant la base de données du 26 février 2014
==========================================================================


.. seealso::

   - :ref:`champ_compteur_bac_melanges`
   - :ref:`courriel_24_fevrier_2014_2`


.. contents::
   :depth: 3
 
 
Courriel
========
 
::

    Date: 	Tue, 25 Feb 2014 13:44:34 +0100
    From: 	Frederic GAUTIER <frederic.gautier@id3.eu>
    To: 	VERGAIN Patrick <patrick.vergain@id3.eu>, DUCHET Rémi <remi.duchet@id3.eu>
    CC: 	Marc LAVOREL <marc.lavorel@orcanthus.com>, BORNIER Christophe <christophe.bornier@id3.eu>
    Subject: 	airbus-helicopter



Contenu
========

Je vais diffuser ce document chez AH pour validation des modifs avant livraison 
définitive.

Pourriez-vous lire corriger ou étoffer si nécessaire.

Notamment, Patrick et Rémi si vous pouvez enrichir les 1.2) 1.3) et 1.4) au 
sujet de la base.

Et Patrick sur le 1.1) vérifie qu'on est bien en phase.

Point souhaité en fin d'AM pour envoie AH dans la foulée.

FG


Document word
==============


:download:`Télécharger le document <XSP4P112_dem-de-modif-04_02_2014__V1-1.doc>`


