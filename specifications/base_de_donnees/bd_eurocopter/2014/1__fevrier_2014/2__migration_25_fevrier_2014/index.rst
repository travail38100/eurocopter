

.. index::
   pair: Base de données ; Migration (25 février 2014)
   pair: Migration ; schemamigration
   pair: Migration ; convert_to_south
   pair: Migration ; migrate


.. _migration_bdd_25_fevrier_2014:

==============================================================================
Migration de la base de données le 25 février 2014)
==============================================================================


.. contents::
   :depth: 3
 
 

Problème: rajout d'un champ ``compteur_bac_melanges`` dans la table "Melange"
==============================================================================

::

    compteur_bac_melanges = models.IntegerField(help_text=u"Le compteur de bacs mélange créé avec ce type de mélange")



::

    class Cmelange(models.Model):
        u''' Un mélange de composants.
        '''
        nom_melange = models.CharField(max_length=7, unique=True)
        temps_de_murissement = models.IntegerField(help_text=u"Le temps nécessaire (en minutes) au mûrisssement du mélange <= 999 minutes")
        duree_de_vie = models.IntegerField(help_text=u"La durée de vie (en minutes) du mélange <= 999 minutes")
        composants = models.ManyToManyField(Ccomposant, help_text=u"Un mélange est constitué de n composants")
        compteur_bac_melanges = models.IntegerField(help_text=u"Le compteur de bacs mélange créé avec ce type de mélange")




Etapes de la migration
=======================

Avant l'ajout du champ ``compteur_bac_melanges`` (convert_to_south)
--------------------------------------------------------------------

Sauvegarde de la structure de la base de données actuelle.



::

     applications\eurocopter\bd_eurocopter>%WORKON_HOME%\eurocopter\Scripts\python django_admin.py convert_to_south --settings=conf.settings.local apps.melanges
     

::
     
    Creating migrations directory at 'C:\projects_id3\P2X238\XLOG2Y057_IHMs_Paban_Eurocopter\0.6.0\applications\eurocopter\bd_eurocopter\apps\melanges\migrations'...
    Creating __init__.py in 'C:\projects_id3\P2X238\XLOG2Y057_IHMs_Paban_Eurocopter\0.6.0\applications\eurocopter\bd_eurocopter\apps\melanges\migrations'...
    + Added model melanges.Ctypecomposant
    + Added model melanges.Ccomposant
    + Added model melanges.Cmelange
    + Added M2M table for composants on melanges.Cmelange
    + Added model melanges.CBacMelange
    + Added model melanges.COuvertureBac
    Created 0001_initial.py. You can now apply this migration with: ./manage.py migrate melanges
    - Soft matched migration 0001 to 0001_initial.
    Running migrations for melanges:
    - Migrating forwards to 0001_initial.
    > melanges:0001_initial  (faked)


App 'melanges' converted. Note that South assumed the application's models matched the database
(i.e. you haven't changed it since last syncdb); if you have, you should delete the melanges/migrations
directory, revert models.py so it matches the database, and try again.


::

    |   admin.py
    |   admin.pyc
    |   forms.py
    |   models.py
    |   models.pyc
    |   tables.py
    |   tables.pyc
    |   tests.py
    |   tree.txt
    |   urls.py
    |   urls.pyc
    |   views.py
    |   __init__.py
    |   __init__.pyc
    |   
    \---migrations
            0001_initial.py
            0001_initial.pyc
            __init__.py
            __init__.pyc
            
            


Ajout du champ ``compteur_bac_melanges`` (convert_to_south)
------------------------------------------------------------

Modification du fichier :file:`melanges/models.py` et ajout de la ligne 
dans la classe ``CMelange``.


::

    compteur_bac_melanges = models.IntegerField(default = 0, help_text=u"Le compteur de bacs mélange créé avec ce type de mélange")



Shema Migration de la base de données
---------------------------------------

::

    0.6.0\applications\eurocopter\bd_eurocopter>%WORKON_HOME%\eurocopter\Scripts\python django_admin.py schemamigration --settings=conf.settings.local apps.melanges --auto
    

::
    
    + Added field compteur_bac_melanges on melanges.Cmelange
    Created 0002_auto__add_field_cmelange_compteur_bac_melanges.py. You can now apply this migration with: ./manage.py migrate melanges
    
    
Migration de la base de données
---------------------------------------

::
    
    0.6.0\applications\eurocopter\bd_eurocopter>%WORKON_HOME%\eurocopter\Scripts\python django_admin.py migrate --settings=conf.settings.local apps.melanges
    
::
    
    Running migrations for melanges:
     - Migrating forwards to 0002_auto__add_field_cmelange_compteur_bac_melanges.
     > melanges:0002_auto__add_field_cmelange_compteur_bac_melanges
     - Loading initial data for melanges.
    Installed 0 object(s) from 0 fixture(s)    
    



Migration des bases de données Eurocopter
==========================================


Fake 
-----


::

    0.6.0\applications\eurocopter\bd_eurocopter>%WORKON_HOME%\eurocopter\Scripts\python django_admin.py migrate --settings=conf.settings.langes 0001 --fake
    
::

    
    - Soft matched migration 0001 to 0001_initial.
    Running migrations for melanges:
    - Migrating forwards to 0001_initial.
    > melanges:0001_initial
    (faked)



Migrate 
--------

::

    0.6.0\applications\eurocopter\bd_eurocopter>%WORKON_HOME%\eurocopter\Scripts\python django_admin.py migrate --settings=conf.settings.local apps.melanges
    

::
    
    Running migrations for melanges:
    - Migrating forwards to 0002_auto__add_field_cmelange_compteur_bac_melanges.
    > melanges:0002_auto__add_field_cmelange_compteur_bac_melanges
    - Loading initial data for melanges.
    Installed 0 object(s) from 0 fixture(s)




 
