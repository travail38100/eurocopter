

.. index::
   pair: Base de données ; 6 février 2014


.. _specs_base_de_donnees_6_fevrier_2014:

==============================================================================
Définitions de droits spécifiques pour certains utilisateurs (6 février 2014)
==============================================================================


.. contents::
   :depth: 3
 
 

Contexte
========

La gestion des bases de données a été faite grâce au :ref:`framework web python Django <django>`
   
   

Introduction
============


Airbus Helicopers voudrait pouvoir distinguer des groupes d'utilisateurs.

A un groupe d'utilisateurs correspond des droits de consultation.


Etat actuel de l'administration
===============================


Actuellement l'utilisateur ``admin`` a la fenêtre d'administration suivante:


.. figure:: fenetre_admin.png
   :align: center
   
   


 
