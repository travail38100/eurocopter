

.. index::
   pair: Spécifications; Traçabilité


.. _specifications_tracabilite:

======================================================================
Spécifications traçabilité des pots ``peinture`` et bacs ``mélange``
======================================================================

.. toctree::
   :maxdepth: 5

   applications/index
   base_de_donnees/index
   puces_RFID/index
   versions/versions









