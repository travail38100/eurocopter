

.. index::
   pair: Spécifications; 13 décembre 2012
   pair: Plan Mémoire; 13 décembre 2012


.. _specs_plan_memoire_12_decembre_2012:

=================================================
Spécifications du plan mémoire (13 décembre 2012)
=================================================



Courriel
===================

::

    Sujet:  eurocopter
    Date :  Thu, 13 Dec 2012 14:27:42 +0100
    De :    Frederic GAUTIER <frederic.gautier@id3.eu>
    Répondre à :    frederic.gautier@id3.eu
    Organisation :  id3 Semiconductors
    Pour :  VERGAIN Patrick <patrick.vergain@id3.eu>
    Copie à :   Marc LAVOREL <marc.lavorel@orcanthus.com>


J'ai complété et modifié le document avec les notions de bac à mélange.

Est concerné:

- la mémoire "user" précédemment définie (adresse 507 à 511)
- la définition d'un plan mémoire pour les tags "bac à mélange".

Les tags "mélange" ne sont pas gérés par l'appli Paban (tablette) mais par le
PDA.

Lire quand même l'impact sur les tags peinture (adr 507-511)

Egalement le pdf de l'organigramme de l'appli pda

Le seul changement est la spécification du champ RFU.


On a deux 2 nouveaux champs:

- un bit indiquant si le tag est un tag *pot de peinture* ou un tag *bac*.
- un bit LMP indiquant si la date de péremption a été reporté.


Plan mémoire Document 2X348 du 13 décembre 2012
================================================

:download:`Télécharger le plan mémoire du 13 décembre 2012 <plan_memoire.doc>`







