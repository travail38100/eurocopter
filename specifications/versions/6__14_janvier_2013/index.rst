

.. index::
   pair: Spécifications; 14 janvier 2013
   pair: Plan Mémoire; 14 janvier 2012


.. _specs_plan_memoire_14_janvier_2013:

=================================================
Spécifications du plan mémoire (14 janvier 2013)
=================================================


Courriel
===================

::


    Sujet:  eurocopter
    Date :  Tue, 15 Jan 2013 13:47:49 +0100
    De :    Frederic GAUTIER <frederic.gautier@id3.eu>
    Répondre à :    frederic.gautier@id3.eu
    Organisation :  id3 Semiconductors
    Pour :  VERGAIN Patrick <patrick.vergain@id3.eu>


J'ai documenté dans le même document la partie "Tag mélange". Pour info.


J'ai complété et modifié le document avec les notions de bac à mélange.


Plan mémoire Document 2X348 du 14 janvier 2013
================================================

:download:`Télécharger le plan mémoire du 14 janvier 2013 <plan_memoire.doc>`







