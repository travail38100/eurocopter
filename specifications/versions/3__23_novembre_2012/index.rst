

.. index::
   pair: Plan Mémoire; 23 novembre 2012
   pair: Spécifications; 23 novembre 2012
   pair: CRC; Spécifications



.. _specifications_plan_memoire_23_novembre_2012:

=================================================
Maj Spécifications du plan mémoire 23/11/2012
=================================================

.. seealso::

   - http://ghsi.de/CRC/index.php?Polynom=1011&Message=E120CAF0



.. contents::
   :depth: 3


Demande FG
==========

.. seealso::

   - :ref:`CRC_3_bits`


::

    Sujet:  Eurocopter
    Date :  Fri, 23 Nov 2012 13:43:23 +0100
    De :    Frederic GAUTIER <frederic.gautier@id3.eu>
    Répondre à :    frederic.gautier@id3.eu
    Organisation :  id3 Semiconductors
    Pour :  VERGAIN Patrick <patrick.vergain@id3.eu>


On en avait parlé : Si tu as le temps ce serait bien de remplacer les 3
bits RFU restants en mémoire par un CRC 3 bits calculé sur les 128+512
bits de données.

Ce n'est pas obligatoire en ce sens que le protocole d'écriture passe
par une re-lecture/contrôle, mais ce serait plus propre.

Pour info, bien que tu dois connaître déjà tout cela :

- http://ghsi.de/CRC/index.php?Polynom=1011&Message=E120CAF0





