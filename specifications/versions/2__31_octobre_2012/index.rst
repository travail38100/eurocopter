
.. index::
   pair: Plan Mémoire; 31 octobre 2012


.. _courriel_31_octobre_2012:

============================================================================================
Modifications du plan mémoire et description des 2 applications à réaliser (31 octobre 2012)
============================================================================================


.. seealso::

   - voir http://fr.wikipedia.org/wiki/SQLite


.. contents::
   :depth: 3

Message de Frédéric GAUTIER
===========================

::

    Sujet:  eurocopter
    Date :  Wed, 31 Oct 2012 15:35:08 +0100
    De :    Frederic GAUTIER <frederic.gautier@id3.eu>
    Répondre à :    frederic.gautier@id3.eu
    Organisation :  id3 Semiconductors
    Pour :  VERGAIN Patrick <patrick.vergain@id3.eu>
    Copie à :   Marc LAVOREL <marc.lavorel@orcanthus.com>


Patrick,

J'ai complété le document 2x348 ci-joint:

- le plan mémoire du tag est légèrement modifié
- j'ai ajouté une description des deux applications à réaliser.

Dés que possible tu commences à regarder de 4.1.2 à 4.1.4

Pour 4.1.1 (récupération de la base de données) il faut attendre la visite du
fabricant de peinture.

4.1.5 est à laisser de côté dans un premier temps.

merci,

Frédéric


Document 2X348 du 31 octobre 2012
=================================

:download:`Télécharger le plan mémoire du 31 octobre 2012 <plan_memoire.doc>`.








