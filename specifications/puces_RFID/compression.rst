
.. index::
   pair: Compression; Tag
   pair: Caractère; Bourrage
   pair: Caractère; Minuscule
   pair: Caractère; Majuscule


.. _compression_donnees:

===========================
Compression des données
===========================


.. contents::
   :depth: 3

Introduction
=============

Il existe 2 modes de compression des lettres et chiffres

- :ref:`Compression des lettres sur (1 + 5 bits) et des chiffres sur (1 + 4 bits) <pot_compression_lettres_et_chiffres>`
  Cette méthode est employée pour le :ref:`tag Pot <tag_pot>`
- :ref:`Compression des lettres sur (1 + 5 bits) et des chiffres sur (1 + 5 bits) <melange_compression_lettres_et_chiffres>`
  Cette méthode est employée pour le :ref:`tag Mélange <tag_melange>`


Traitement de la longueur fixe des champs
=========================================

Les champs alphanumériques doivent avoir une longueur fixe.

Cas où le champ est inférieur à la longueur fixe
-------------------------------------------------

Dans ce cas on ajoute 1 ou plusieurs **caractères de bourrage**.
On les enlève à la lecture du tag.


Cas où le champ est supérieur à la longueur fixe
-------------------------------------------------

Dans ce cas on supprime les caractères en trop.
On **perd donc de l'information**.


Traitement de la casse des caractères
=====================================

Comme il n'est pas possible de coder les majuscules et minuscules, on code
les lettres en majuscules.








