

.. index::
   pair: Plan mémoire; Mélange
   pair: Mémoire; Mélange
   pair: Tag; Mélange


.. _plan_memoire_bac:
.. _plan_memoire_melange:
.. _plan_memoire_bac_melange:
.. _tag_melange:

=============================================
Plan mémoire de la puce RFID ``bac mélange``
=============================================

.. seealso::

   - :ref:`tag_pot`
   - :ref:`table_historique_tag_melange`

.. toctree::
   :maxdepth: 3

   epc_private
   memoire_utilisateur
   epc_public

.. toctree::
   :maxdepth: 2

   historique/index

   compression/index

