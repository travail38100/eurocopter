
.. index::
   pair: Plan mémoire ; 2014 (Avant le 26 février)
   pair: 2014 (26 février) ; Plan mémoire
   pair: Mémoire ; 2014 (Avant le 26 février)
   pair: Mélange ; 2014 (Avant le 26 février)
   

.. _memoire_privee_bac_melange_before_26_fevrier_2014:

=================================================================================================
Plan de la mémoire ``privée`` ``Bac mélange`` (16 octets => 128 bits) (avant le 26 février 2014)
=================================================================================================


.. seealso::

   - :ref:`plan_memoire_pot`
   - :ref:`tag_melange`


.. contents::
   :depth: 3


=========   ================================  ================= ==================================
Adresse     Data                              Taille            Commentaires
=========   ================================  ================= ==================================
0-15        Date de fabrication du mélange    16 bits           J : 5 bits/ M : 4bits / A-2000 : 7 bits
16-26       Minutes date de fabrication       11 bits           minutes sur 11 bits mn d’offset sur date fab (+ 2048 mn 1440 nécessaire: 24 heures)
27-36       Temps de mûrissement              10 bits           Le temps nécessaire (en minutes) au mûrisssement du mélange <= 999 minutes
37-46       Durée de vie du mélange           10 bits           La durée de vie (en minutes) du mélange <= 999 minutes
47-48       Code vacation                     2 bits            0 tag vierge, 1=matin,2=après-midi, 3=nuit
49-56       Numéro du mélange vacation        8 bits            Numéro du mélange créé pendant la vacation courante
57-86       Nom du mélange                    30 bits           5 caractères codés sur 6 bits 'P' + XXXXX
87-93       RFU                               7 bits            0
94-101      Viscosité                         8 bits            Viscosité 0 à 99 secondes (on se réserve de la place en codant sur 8 bits)
102-111     Température                       10 bits           Température en 10ème de degré (0 à 99,9) 999 < 1024
112-127     CRC                               16 bits           CRC sur 16 bits
=========   ================================  ================= ==================================


:Total: 128 bits



Présentation du contexte
========================

Composantes autorisées
----------------------

Dans un mélange, les :ref:`types de composants <type_composant>` autorisés sont:

- base
- durcisseur
- diluant

Numéro de mélange
-----------------

Le ``numéro de mélange`` est constitué des 3 champs suivants:

- :ref:`code vacation <code_vacation>`
- :ref:`date de fabrication du mélange <date_fabrication_melange>`
- :ref:`numéro du mélange créé durant la vacation <numero_melange>`

