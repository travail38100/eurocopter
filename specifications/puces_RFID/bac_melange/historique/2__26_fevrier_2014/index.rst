

.. index::
   pair: Plan Mémoire; 2014 (26 février)


.. _specs_melange_plan_memoire_26_fevrier_2014:

=================================================
Spécifications du plan mémoire (26 février 2014)
=================================================

.. seealso::

   - :ref:`plan_memoire_melange`



Nouveautés
===========

On prend 4 bits de plus pour le numéro de mélange. Passage de 8 à 12 bits.



Ancien plan mémoire
====================

.. toctree::
   :maxdepth: 3
   
   epc_private

