
.. index::
   pair: Compression; Mélange


.. _melange_compression_donnees:

================================
Mélange compression des données
================================

.. seealso::

   - http://rosettacode.org/wiki/Bitwise_IO


.. toctree::
   :maxdepth: 3

   compression_lettres_et_chiffres
   memoires_concernees




