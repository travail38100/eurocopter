
.. index::
   pair: Compression; Lettres et chiffres
   pair: Chiffres; 1 + 5 bits

.. _melange_compression_lettres_et_chiffres:

=====================================================================
Mélange compression des lettres (1 + 5) bits et chiffres (1 + 5) bits
=====================================================================


.. contents::
   :depth: 3


Compression des lettres sur 1 + 5 bits
======================================

- lettre  : 6 bits = 1 bit d'entête de valeur **0** + 5 bits (32 caractères)


Sur 5 bits on peut coder les 26 lettres de l'alphabet (0 à 25) + les lettres suivantes:

- ' ' (l'espace)
- '.' (le point)
- "-" (le tiret)
- "_" (le souligné)
- "/" (le slash)


Compression des chiffres sur 1 + 5 bits
=======================================


- chiffre : 6 bits = 1 bit d'entête de valeur **1** + 5 bits permettant de coder
  les 10 chiffres de 0 à 9.







