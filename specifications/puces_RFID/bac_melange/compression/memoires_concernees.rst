
.. index::
   pair: Mélange; Compression


.. _mem_melange_compression:

==============================================
Mémoires mélange concernées par la compression
==============================================

.. contents::
   :depth: 3


Tag mélange
===========


Mémoire EPC privée
------------------

- :ref:`nom_melange`





