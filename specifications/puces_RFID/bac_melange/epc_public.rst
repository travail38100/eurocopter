
.. index::
   pair: Bac mélange; Mémoire publique


.. _memoire_publique_bac_melange:

======================================================================
Plan de la mémoire ``publique`` ``Bac mélange`` (12 octets => 96 bits)
======================================================================


=========   ====================    ================= ==================================
Adresse     Data                    Taille            Commentaires
=========   ====================    ================= ==================================
0-7         "0"                     8 bits            le chiffre zéro
8-39        "Eurocopter"            32 bits           identifiant eurocopter
40-92       RFU                     53 bits           non défini/data non protégées
93-95       Organisation Memoire    3 bits            Descripteur de plan mémoire
=========   ====================    ================= ==================================

:Total: 96 bits


``Org mem`` est un descripteur de plan mémoire.

Ce paramètre pourra permettre l’évolution vers de futurs tags possédant plus de
mémoire (compatibilité et évolution).

Par défaut Org.mem = « 0 »

