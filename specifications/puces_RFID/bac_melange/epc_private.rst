
.. index::
   pair: Alarme; Temps de mûrissement non atteint
   pair: Alarme; Temps de péremption dépassé
   pair: Bac à mélange; EPC public
   pair: Bac à mélange; Vacations
   pair: Nom; Mélange
   pair: Temps ; Mûrissement
   pair: Temps ; Péremption
   pair: Date ; Mûrissement
   pair: Date ; Péremption mélange
   pair: Bac à mélange; Temps de mûrissement
   pair: Bac à mélange; Temps de péremption
   pair: Bac à mélange; Température
   pair: Bac à mélange; Viscosité
   pair: Table ; Mélanges autorises
   pair: CRC; EPC
   pair: Bac mélange; Mémoire privée
   pair: Mémoire ; 2014 (Après le 26 février 2014)
   pair: Mélange ; 2014 (Après le 26 février 2014)
   
   

.. _memoire_privee_bac_melange:

==========================================================================================
Plan de la mémoire ``privée`` ``Bac mélange`` (16 octets => 128 bits) au 26 février 2014
==========================================================================================


.. seealso::

   - :ref:`plan_memoire_pot`
   - :ref:`tag_melange`


.. contents::
   :depth: 3



Nouveauté
==========

Le mercredi 26 février 2014, le champ ``numero mélange`` passe de 8 à 12 bits.

Ce numéro mélange faisait réference au numéro de vacation courante.

Désormais, il concerne le numéro de bac mélange créé au cours d'une année.



Plan mémoire 
=============


=========   ================================  ================= ==================================
Adresse     Data                              Taille            Commentaires
=========   ================================  ================= ==================================
0-15        Date de fabrication du mélange    16 bits           J : 5 bits/ M : 4bits / A-2000 : 7 bits
16-26       Minutes date de fabrication       11 bits           minutes sur 11 bits mn d’offset sur date fab (+ 2048 mn 1440 nécessaire: 24 heures)
27-36       Temps de mûrissement              10 bits           Le temps nécessaire (en minutes) au mûrisssement du mélange <= 999 minutes
37-46       Durée de vie du mélange           10 bits           La durée de vie (en minutes) du mélange <= 999 minutes
47-48       Code vacation                     2 bits            0 tag vierge, 1=matin,2=après-midi, 3=nuit
49-60       Numéro du mélange                 12 bits           Numéro du mélange créé au cours de l'année
61-90       Nom du mélange                    30 bits           5 caractères codés sur 6 bits 'P' + XXXXX
91-93       RFU                               3 bits            0
94-101      Viscosité                         8 bits            Viscosité 0 à 99 secondes (on se réserve de la place en codant sur 8 bits)
102-111     Température                       10 bits           Température en 10ème de degré (0 à 99,9) 999 < 1024
112-127     CRC                               16 bits           CRC sur 16 bits
=========   ================================  ================= ==================================


:Total: 128 bits

Présentation du contexte
========================

Composantes autorisées
----------------------

Dans un mélange, les :ref:`types de composants <type_composant>` autorisés sont:

- base
- durcisseur
- diluant

Numéro de mélange
-----------------

Le ``numéro de mélange`` est le numéro du bac mélange créé au cours de l'année.

Etant sur 12 bits il est possible de faire 4096 mélanges.


.. _date_fabrication_melange:

Date de fabrication du mélange
==============================

.. seealso::

   - :ref:`date_fabrication_pot`
   - :ref:`table_bac_melange`


La date de fabrication du mélange ne contient pas les heures et les minutes.


.. _date_fabrication_mn:

Minutes date de fabrication du mélange
======================================

.. seealso::

   - :ref:`table_bac_melange`

Comme la date de fabrication du mélange ne contient pas les heures et les minutes
on rajoute ce champ qui contient les minutes.

.. note:: unjour = 24 x 60 =  1440 minutes => d'ou le besoin de 11 bits.


.. _code_vacation:
.. _code_vacations:

Code vacation
=============

.. seealso::

   - :ref:`table_type_vacation`
   - :ref:`table_vacation_courante`

La vacation est la période de travail d'une équipe.

Pour les équipes en 3*8 il y a 3 vacations:

- le matin de 5h à 13h,      => Code vacation=1 (lettre 'A')
- l'après-midi de 13h à 21h  => Code vacation=2 (lettre 'B')
- et la nuit de 21h à 5h.    => Code vacation=3 (lettre 'C')


.. _numero_melange:

Numéro de mélange 
==========================

.. seealso::

   - :ref:`cvacation_courante_compteur_melange`
   - :ref:`table_bac_melange`


Avant le 26 février 2014
-------------------------

Le numéro de mélange était le numéro créé durant une vacation:

- le 1er mélange  a pour valeur «1»
- le 2ème mélange a pour valeur «2»
- etc...

Le numéro du mélange était :ref:`reseté automatiquement au changement de vacation <cvacation_courante_compteur_melange>`,
c'est-à-dire dés qu’une vacation saisie est différente de la dernière vacation
saisie.


Après le mercredi 26 février 2014
----------------------------------

Le ``numéro de mélange`` est désormais le numéro du bac mélange créé au cours de l'année.

Etant sur 12 bits il est possible de faire 4096 mélanges.


.. _nom_melange:

Nom Mélange (``PXXXXX``)
=========================

.. seealso::

   - :ref:`melange_compression_lettres_et_chiffres`
   - :ref:`nom_de_melange_24_fevrier_2014`

Le nom de mélange est au format ``PXXXXX`` (P + 5 caractères).
Le nom de mélange est compressé suivant la :ref:`méthode de compression suivante <melange_compression_lettres_et_chiffres>`.


Le nom de mélange sera trouvé dans une :ref:`table de la base de donnée interne du PDA <specs_bdd_pda>`.


Voir courriel du :ref:`lundi 24 février 2014 <nom_de_melange_24_fevrier_2014>`. 


Exemples de nom de mélange
--------------------------

- P81
- P05
- P10


.. _table_melanges:

Table des mélanges autorisés
----------------------------

.. seealso::

   - :ref:`champ_ECS`

La table est un tableau comportant x lignes de n+1 chaînes de
caractères::

    ECS1-1, ECS1-2, ECS1-3….,ECS1-n => PAAAAA
    ECS2-1, ECS2-2, ECS2-3….,ECS2-n => PBBBBB
    ...
    ECSx-1, ECSx-2, ECSx-3….,ECSx-n => PZZZZZ

On trouve le ``PXXXXX`` par recherche de la ligne x qui correspond aux n°ECS des
composantes de mélanges tagués lors de la création du mélange.


.. _temps_murissement:

Temps de mûrissement du mélange (Tmur) (0 à 999)
================================================

.. seealso::

   - :ref:`table_bac_melange`

Le temps de mûrissement est la durée en mn (de 0 à 999 mn) qui doit s'écouler
avant l’ouverture du mélange.


Alarme temps de mûrissement non atteint
---------------------------------------


.. warning:: Si l’ouverture du **mélange** se fait alors que ce temps n’est pas atteint,
   **une alarme est émise**.


.. _temps_peremption:

Durée de vie du mélange (Tperemption)  (0 à 999)
===================================================

.. seealso::

   - :ref:`table_bac_melange`

Le temps de péremption est la durée en mn (de 0 à 999 mn) au delà de laquelle
le mélange est périmé.


Alarme temps de péremption dépassé
-----------------------------------

.. warning:: Si l’ouverture du **mélange** se fait alors que ce temps est écoulé, **une alarme est émise**.

Le temps de mûrissement (Tmur) et le temps de péremption (Tper) sera trouvé dans
une :ref:`table base de donnée interne du PDA <table_melange>`.

::

    P81 => temps de mûrissement 3 caractères en minutes
        => durée de vie 3 caractères en minutes



.. _mem_table_melange_autorise:

Table des mélanges avec les temps de mûrissement et péremption
------------------------------------------------------------------------

.. seealso::

   - :ref:`table_melange`

La table est la suite du tableau qui comporte x lignes::

    PAAAAA => Tmur1,Tper1
    PBBBBB => Tmur2,Tper2
    ...
    PZZZZZ => Tmurx,Tperx


.. _viscosite_melange:

Viscosité (0 à 99 secondes), sur 8 bits
=======================================

.. seealso::

   - :ref:`table_bac_melange`

La viscosité se mesure en secondes.

La viscosité est saisie sur 2 caractères par l’opérateur à la création du mélange.

Cette viscosité est stockée en binaire sur 8 bits (on se réserve de la place).


.. _temperature_melange:

Température (0 à 99,9°C), sur 10 bits
=====================================

.. seealso::

   - :ref:`table_bac_melange`

La température se mesure en 10ème de degré (0 à 99,9).

La température est saisie sur 4 caractères avec virgule par l’opérateur à la
création du mélange : TT,T

Cette chaîne est convertie en 10ème de° puis en binaire sur 10 bits (1024 valeurs).


CRC
===

Calcul du :term:`CRC` sur 16 bits.


