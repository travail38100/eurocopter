
.. index::
   pair: Bac mélange; Mémoire utilisateur
   pair: OF; Ordre de fabrication


.. _memoire_utilisateur_bac_melange:

===========================================================================
Plan de la mémoire ``utilisateur`` ``Bac mélange`` (64 octets => 512 bits)
===========================================================================

.. seealso::

   - :ref:`tag_melange`

.. contents::
   :depth: 3

Mapping de la mémoire utilisateur
=================================


Les données sont:

=========   =========================   =============== ===============================================================
Adresse     Data                        Taille          Commentaires
=========   =========================   =============== ===============================================================
0-59        Matricule opérateur         60 bits         10 caractères du matricule de l'opérateur ayant créé le mélange
60-69       delta date user1            10 bits         minutes d’offset/fabrication 1ere ouverture
70-79       delta date user2            10 bits         minutes d’offset/user1 2eme ouverture
80-89       delta date user3            10 bits         minutes d’offset/user2 3ème ouverture
90-99       delta date user4            10 bits         minutes d’offset/user3 4ème ouverture
100-109     delta date user5            10 bits         minutes d’offset/user4 5ème ouverture
110-119     delta date user6            10 bits         minutes d’offset/user5 6ème ouverture
120-129     delta date user7            10 bits         minutes d’offset/user6 7ème ouverture
130-139     delta date user8            10 bits         minutes d’offset/user7 8ème ouverture
140-149     delta date user9            10 bits         minutes d’offset/user8 9ème ouverture
150-159     delta date user10           10 bits         minutes d’offset/user9 10ème ouverture
507         Type tag: Pot ou Mélange    1 bit           1 => tag pot , 0 => tag mélange ou vierge
508         LMP                         1 bit           1 => une demande de prolongation de validité de l'ouverture du mélange a été faite
509-511     CRC                         3 bit           CRC 3 bits sur epc-privé et memoire-user
=========   =========================   =============== ===============================================================


.. _matricule_operateur:


Présentation du contexte
========================


Par ``Eurocopter``
--------------------


::

    Un mélange peut être utilisé pour plusieurs OF (Ordre de Fabrication), pour
    toute la vacation (il y a des restrictions, suivant les peintures, nous devons
    tenir compte des pot life des peintures).
    Au maximum, nous utiliserons ce mélange sur 10 OF.


.. _ordres_de_fabrication:

Les ordres de fabrication
-------------------------

Un ``Ordre de fabrication`` désigne le fait d'ouvrir:

- 1 simple pot =>  ouverture d'un pot (1 pot n'étant ouvert qu'une seule fois)
- 1 mélange => ouverture d'au plus 10 mélanges 


.. note:: L'ordre de fabrication n'est pas écrit dans la radio-étiquette
   mais dans la :ref:`table Ouverture Bac <table_ouverture_bac>` de la :ref:`base de données Eurocopter <specs_bdd_pda>`.


Matricule opérateur
===================

.. seealso::

   - :ref:`melange_compression_lettres_et_chiffres`


Le matricule de l'opérateur ayant créé le mélange codé suivant cette :ref:`méthode <melange_compression_lettres_et_chiffres>`.


.. _delta_date_user1:

Delta date ouverture user1
==========================

- Nombre de minutes par rapport à la date de fabrication.
- première ouverture

.. _delta_date_user2:

Delta date ouverture user2
==========================

- Nombre de minutes par rapport à la première ouverture.
- deuxième ouverture

.. _delta_date_user3:

Delta date ouverture user3
==========================

- Nombre de minutes par rapport à la deuxième ouverture.
- troisième ouverture

.. _delta_date_user4:

Delta date ouverture user4
==========================

- Nombre de minutes par rapport à la troisième ouverture.
- quatrième ouverture

.. _delta_date_user5:

Delta date ouverture user5
==========================

- Nombre de minutes par rapport à la quatrième ouverture.
- cinquième ouverture

.. _delta_date_user6:

Delta date ouverture user6
==========================

- Nombre de minutes par rapport à la cinquième ouverture.
- sixième ouverture

.. _delta_date_user7:

Delta date ouverture user7
==========================

- Nombre de minutes par rapport à la sixième ouverture.
- septième ouverture

.. _delta_date_user8:

Delta date ouverture user8
==========================

- Nombre de minutes par rapport à la septième ouverture.
- huitième ouverture

.. _delta_date_user9:

Delta date ouverture user9
==========================

- Nombre de minutes par rapport à la huitième ouverture.
- neuvième ouverture

.. _delta_date_user10:

Delta date ouverture user10
===========================

- Nombre de minutes par rapport à la neuvième ouverture.
- dixième ouverture

.. _bit_type_tag_melange:

(Tag mélange) Type du tag
==========================

.. seealso::

   - :ref:`bit_type_tag_pot`

- 1 => c'est un :ref:`pot <tag_pot>`
- 0 => :ref:`mélange ou vierge <tag_melange>`


LMP
===

- 1=>  une demande de prolongation pour l'ouverture du mélange a été faite


CRC
===

CRC 3 bits sur epc-privé et memoire-user.




