

.. index::
   pair: Puces RFID; Plans mémoire
   pair: Plans; Mémoire
   ! Plans mémoire

.. _specifications_plan_memoire:
.. _specifications_plans_memoire:
.. _plans_memoire:
.. _plan_memoires:

=================================================
Spécifications des plans mémoire des puces RFID
=================================================


.. contents::
   :depth: 3


Puces RFID Pot et bac mélange
=============================

.. toctree::
   :maxdepth: 5

   bac_melange/index
   pot/index


Compression des données en mémoire
==================================

.. toctree::
   :maxdepth: 5

   compression







