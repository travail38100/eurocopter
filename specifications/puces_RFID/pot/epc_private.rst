
.. index::
   pair: Pot; Mémoire privée
   pair: ECS; Mélange
   pair: Date; Péremption
   pair: Date; Livraison
   pair: Date; Fabrication
   ! ECS


.. _memoire_privee_pot:

===============================================================
Plan de la mémoire ``privée`` ``Pot`` (16 octets => 128 bits)
===============================================================


.. seealso::

   - :ref:`tag_pot`
   - :ref:`table_pot`



.. contents::
   :depth: 3



Liste des champs de la mémoire EPC ``privée``.




======= =========   =========================     ========  ===============================================================================================
Numéro  Adresse     Data                          Taille    Commentaires
======= =========   =========================     ========  ===============================================================================================
 1      0- 15       Date fabrication              16 bits   jour du mois (sur 5 bits) + le mois (sur 4 bits) / An-2000 (sur 7 bits)
 2      16- 27      Delta date peremption         12 bits   différence en nb jours entre la date de fabrication et la date de péremption (max: 2**12 = 4096  j)
 3      28- 38      Delta date livraison          11 bits   différence en nb jours entre la date de fabrication et la date de livraison  (max: 2**11 = 2048 j)
 4      39- 50      Delta date ouverture pot      12 bits   différence en nb jours entre la date de fabrication et la date d'ouverture du pot  (2**12 = max: 4096 j)
 5      51- 59      Nb jours péremption           9  bits   (max: 2**9 = 512 j) Nombre de jours de péremption après ouverture du pot
 6      60- 66      Température stockage min      7  bits   (1 bit de signe et 6 bits de data (-63..+63)
 7      67- 73      Température stockage max      7  bits   (max: 2**7 = 128, 0..128)
 8      74- 91      Quantité de peinture          18 bits   0-16384 (2**14 sur 14 bits) + unité (4 bits)
 9      92-127      Code ECS                      36 bits   Code ECS (code Eurocopter) 6 caractères
======= =========   =========================     ========  ===============================================================================================


:Total: 128 bits




.. _date_fabrication:
.. _date_fabrication_pot:

Date de fabrication du pot
==========================

.. seealso::

   - :ref:`table_pot`


La date de fabrication est codée sur 16 bits comme étant un offset de l’an 2000.

- jour  (sur 5 bits)
- mois  (sur 4 bits)
- année (sur 7 bits => offset max de 128 ans)


L’encodage doit tenir compte des années bissextiles.


.. _pot_delta_date_peremption:

Delta date péremption/date de fabrication
=============================================

.. seealso::

   - :ref:`table_pot`

Delta date de péremption calculée à partir du delta en jour par rapport à la
date de fabrication.


.. _pot_delta_date_livraison:

Delta date livraison/date de fabrication
========================================

.. seealso::

   - :ref:`date_livraison_excel`


Delta date de livraison calculée à partir du delta en jour par rapport à la
date de fabrication.

Cette date de livraison peut être mise à jour à partir du lecteur PDA.


.. _delta_ouverture_pot_date_fab:

Delta date ouverture pot/date de fabrication
============================================

.. seealso::

   - :ref:`ouverture_pot`

Delta date d'ouverture du pot calculée à partir du delta en jour par rapport
à la :ref:`date de fabrication <date_fabrication_pot>`.


.. _pot_nb_jours_peremption_pot_ouvert:

Nombre de jours péremption après pot ouvert
============================================

- Cette donnée permet de calculer la date de péremption «pot ouvert».
- Durée de péremption en jours : quantité à ajouter à date d’ouverture pour
  calculer la nouvelle date de péremption «pot ouvert»


Température stockage minimale
=============================

Température min sur 1 bit de signe et 6 bits de data (+/- 63°)


Température stockage maximale
==============================


Température max sur 7 bits de data (0/127°)


Quantité de peinture
=====================

La quantité de produit est codée sur 18 bits.

Il faudra réussir à interpréter les données dans la base du fabricant de peinture
pour déterminer la partie numérique et les unités de l’information.

Partie numérique du champ «quantité» sur 14 bits et reconnaissance de
l’unité (L, mL, g, kg, ...) et codage de l’unité sur 4 bits.

Si la tâche est trop compliquée on pourra réduire les champs «delta dates»
à 11 bits afin de récupérer 24 bits pour le champ « quantité »

Les six caractères ascii de «quantité» seront alors interprétés et codés
compressés sur 6 caractères hexadécimaux réduits:

- «0»  = 00h
- «1»  = 01h
- .
- «8»  = 08h
- «9»  = 09h
- «.»  = 0Ah
- «g»  = 0Bh
- «L»  = 0Ch
- «kg» = 0Dh
- «ml» = 0Eh
- «cl» = 0Fh


.. _champ_ECS_27_novembre:
.. _champ_ECS:

Champ ECS (Code produit Eurocopter) 6 caractères
================================================

.. seealso::

   - :ref:`table_composant`

Le champs ECS sur 6 caractères représente le codage de la peinture.

Il est utilisé dans le cas de la création des :ref:`mélanges <table_melange>`.

Le champ ECS est compressé suivant la :ref:`méthode de compression suivante <pot_compression_donnees>`.


