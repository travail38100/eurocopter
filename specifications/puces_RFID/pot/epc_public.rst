
.. index::
   pair: Pot; Mémoire publique

.. _memoire_epc_public_27_novembre_2012:
.. _memoire_pot_public:
.. _memoire_publique_pot:

==================================================================
Plan de la mémoire ``publique`` ``Pot`` (12 octets => 96 bits)
==================================================================


.. contents::
   :depth: 3


Introduction
============


Bien que non obligatoire on peut organiser le code epc public au standard
normalisé.

Liste des champs
=====================

Liste des champs de la mémoire EPC public.


=========   ====================    ================= ==================================
Adresse     Data                    Taille            Commentaires
=========   ====================    ================= ==================================
0-7         "0"                     8 bits            le chiffre zéro
8-39        "Eurocopter"            32 bits           identifiant eurocopter
40-92       RFU                     53 bits           non défini/data non protégées
93-95       Organisation Memoire    3 bits            Descripteur de plan mémoire
=========   ====================    ================= ==================================

:Total: 96 bits


Description des champs
======================

Organisation Memoire
---------------------

**Descripteur de plan mémoire**

Ce paramètre pourra permettre l’évolution vers de futurs tags possédant plus de
mémoire (compatibilité et évolution).


