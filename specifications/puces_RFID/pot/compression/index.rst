
.. index::
   pair: Compression; Pot

.. _pot_compression_donnees:

===========================
Pot compression des données
===========================

.. seealso::

   - http://rosettacode.org/wiki/Bitwise_IO


.. toctree::
   :maxdepth: 3

   compression_lettres_et_chiffres
   memoires_concernees




