
.. index::
   pair: Compression; Lettres et chiffres
   pair: Chiffres; 1 + 4 bits

.. _pot_compression_lettres_et_chiffres:

=================================================================
Pot compression des lettres (1 + 5) bits et chiffres (1 + 4) bits
=================================================================


.. contents::
   :depth: 3


Compression des lettres sur 1 + 5 bits
======================================

- une lettre est codée sur 6 bits = 1 bit d'entête de valeur **0** + 5 bits (32 caractères)


Sur 5 bits on peut coder les 26 lettres de l'alphabet (0 à 25) + les lettres suivantes:

- ' ' (l'espace)
- '.' (le point)
- "-" (le tiret)
- "_" (le souligné)
- "/" (le slash)


Compression des chiffres sur 1 + 4 bits
=======================================


- un chiffre est codé sur 5 bits = 1 bit d'entête de valeur **1** + 4 bits permettant de coder
  les 10 chiffres de 0 à 9.







