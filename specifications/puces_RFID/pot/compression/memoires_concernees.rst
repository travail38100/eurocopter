
.. index::
   pair: Compression; Tag



.. _pot_compression:

===========================================
Pot mémoires concernées par la compression
===========================================

.. contents::
   :depth: 3


Mémoire utilisateur 512 bits
============================

- :ref:`memoire_utilisateur_pot`


Mémoire EPC privée
===================

- :ref:`champ_ECS`





