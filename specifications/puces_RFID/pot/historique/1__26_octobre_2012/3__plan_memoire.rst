

.. index::
   pair: Plan mémoire; 26 octobre 2012



.. _plan_memoire_26_octobre_2012:

===============================
Plan mémoire du 26 octobre 2012
===============================

.. contents::
   :depth: 3


mémoire epc public
==================

Bien que non obligatoire on peut organiser le code epc public au standard
normalisé :

EPC public (96 bits)

=========   ============    =================  ========================
Adresse     Data            taille             commentaires
=========   ============    =================  ========================
0-7         "0"             8 bits
8-39        "Eurocopter"    32 bits
40-75       code ECS        36 bits            6 caractères
76          LMP             1 bit
77-92       RFU             16 bits            usage non défini
93-95       Org. Mem        3 bits             type d’organisation de la mémoire
=========   ============    =================  ========================

:Total: 96 bits


«Org mem» est un descripteur de plan mémoire. Ce paramètre pourra permettre
l’évolution vers de futurs tags possédant plus de mémoire (compatibilité et évolution).


EPC private (128 bits)
======================

=========   ==================  ======== ========================================
Adresse     Data                Taille   Commentaires
=========   ==================  ======== ========================================
0-15        date fab            16 bits  1-366j sur 9 bits / An-2000 sur 7 bits
16-25       delta date per      10 bits  jours d’offset sur date fab (+ 1024 j)
26-35       delta date livr     10 bits
36-45       delta date ouvert   10 bits
46-55       delta date per +    10 bits
56-64       température          9 bits  -100 à   +100°C (512 val possible)
65-88       quantité            24 bits  10^6 valeurs + unité (4bits)
88-124      ECS                 36 bits  répétition
125-127     Org Mem             3 bits   répétition
=========   ==================  ======== ========================================


:Total: 128 bits



.. _memoire_utilisateur_512_bits_26_octobre:

Mémoire utilisateur (512 bits) du 26 octobre 2012
==================================================

Les données (95 caractères max) sont compressées.

=========   =========================   ========================================
Adresse     Data                        Taille
=========   =========================   ========================================
0-3         long. champ «Nom»           4 bits «nom  de 0 à 15 caractères
4-7         long. Champ «réf»           4 bits «réf.» de 4 à 20 caractères
8-11        long. Ch.   «Fournisseur»   4 bits «Fournisseur» de 0 à 15 caractères
12-16       long. Champ «lot»           5 bits «lot» de 0 à 30 caractères
17-20       long. Champ «teinte»        4 bits «coul.» de 0 à 15 caractères
21-508      Nom+réf+Four+lot+teinte     488 bits compress 81 lettres/95 chiffres
509-511     RFU                         3 bits
=========   =========================   ========================================

:Total: 512

Compression
============

Le champ «data» de 488 bits est compressé comme suit:

- lettre  : 6 bits = 1 bit entête (L/C) + 5 bits (32 caractères QSCII)
- chiffre : 5 bits = 1 bit entête (L/C) + 4 bits (16 symboles)

La perte max de 14 caractères sur 95 (si tout les caractères sont des lettres)
sera imputée obligatoirement aux 2 champs «Nom» et «Fournisseur».


