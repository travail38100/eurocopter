
.. index::
   pair: Mémoire; 512 bits
   pair: Mémoire; Utilisateur
   pair: Mémoire Utilisateur; 512 bits
   pair: Pot; Teinte
   pair: Pot; Lot
   pair: Pot; Nom
   pair: Pot; Référence



.. _memoire_utilisateur_pot:

===================================================================
Plan de la mémoire ``utilisateur`` ``Pot`` (64 octets => 512 bits)
===================================================================

.. seealso::

   - :ref:`tag_pot`


.. contents::
   :depth: 3


Liste des champs de la mémoire utilisateur 512 bits.

On a deux 2 nouveaux champs:.

- un bit (bit 507) indiquant si le tag est un tag *pot de peinture* ou un tag *bac*.
- un bit LMP indiquant si la date de péremption a été reporté.


=========   =========================   =============== =========================
Adresse     Data                        Taille          Commentaires
=========   =========================   =============== =========================
0-3         long. champ "Nom"           4 bits          "nom" de 0 à 15 caractères
4-7         long. Champ "réf"           4 bits          "réf"  de 4 à 19 caractères => longueur stockée = longueur - 4
8-11        long. Ch.   "Fournisseur"   4 bits          "Fournisseur" de 0 à 15 caractères
12-16       long. Champ "lot"           5 bits          "lot" de 0 à 30 caractères
17-20       long. Champ "teinte"        4 bits          "coul" de 0 à 15 caractères
21-506      Nom+réf+Four+lot+teinte     486 bits        compress 81 lettres/95 chiffres
507         Type tag: Pot ou Mélange    1 bit           1 => tag pot , 0 => tag mélange ou vierge
508         LMP                         1 bit           1 => une demande de prolongation de validité du pot ouvert a été faite
509-511     CRC                         3 bit           CRC 3 bits sur epc-privé et memoire-user
=========   =========================   =============== =========================


- les bits 504 à 506 (3 bits) sont occupés (0x07)
- le bit 507 (Pot) est le 4e bit (0x08)
- le bit 508 (LMP) est le 5e bit (0x10)
- les bits 509 à 511 sont sur les 3 derniers bits (0xe0)


La perte max de 14 caractères sur 95 (si tous les caractères sont des lettres)
sera imputée obligatoirement au 2 champs «Nom» et «Fournisseur».


Nom
===

Référence
=========


Fournisseur
===========

Lot
===


Teinte
======



.. _bit_type_tag_pot:

(Tag pot) Type du tag
======================

.. seealso::

   - :ref:`bit_type_tag_melange`


- 1 => c'est un :ref:`pot <tag_pot>`
- 0 => :ref:`mélange ou vierge <tag_melange>`


LMP
===

- 1=>  une demande de prolongation pour l'ouverture du pot a été faite.



.. _CRC_3_bits:

CRC 3bits
==========

.. seealso::

   - :ref:`specifications_plan_memoire_23_novembre_2012`


Le :term:`CRC` est calculé sur toutes les données epc-privé et memoire-user.


Le CRC occupe les 3 derniers bits du dernier octet.


.. note::  22/11/2012 - RFU à remplacer par CRC 3bits.



.. _compression_data_nom_teinte:

Compression des données Nom+réf+Four+lot+teinte
===============================================

.. seealso::

   - :ref:`compression_donnees`


Les données (95 caractères max) sont compressées comme :ref:`suit <compression_donnees>`
de façon à n'occuper que 61 octets au maximum soit (61x8=488 bits).

La perte max de 14 caractères sur 95 (si tous les caractères sont des lettres)
sera imputée obligatoirement aux 2 champs "Nom" et "Fournisseur".


Accès
=====

L’accès à la mémoire utilisateur est protégé par mot de passe.






