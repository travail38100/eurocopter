
.. index::
   pair: Base de données; PDA
   pair: Base de données; DB_Eurocopter.sqlite


.. _base_de_donnees_pda:
.. _specs_bdd_pda:

==============================================
Base de données du PDA (DB_Eurocopter.sqlite)
==============================================

.. contents::
   :depth: 3


Introduction
=============

.. seealso::

   - :ref:`db_eurocopter`

Le PDA comporte une base de données caractéristique de mélange, cette base de
données sera en partie renseignée dés l’origine du logiciel mais pourra être
enrichie à tout moment par Eurocopter via un mécanisme d’édition-saisie.

Mélange-(Nom/Tmur,Tper)
=======================

.. seealso::

   - :ref:`nom_melange`
   - la :ref:`table des mélanges <table_melange>`
   - la :ref:`table des composants <table_composant>`

C’est la base de x lignes comportant n-1 entrées :

ECSx-1, ECSx-2, ECSx-3….,ECSx-n,P*****,Tmurx,Tperx

ECS* = 36bits des 6 caractères ECS de chaque composante du mélange.

:ref:`PXXXXX <nom_melange>` : Nom :*****  = 5 caractères de 6 bits chacun.

Tmur : en mn sur 3 caractères chiffres à convertir en binaire 10bits
Tper : en mn sur 3 caractères chiffres à convertir en binaire 10bits

Mélange-valide
===============

.. seealso::

   - la :ref:`table des associations entre un mélange et ses composants <table_melange_composants>`


A la création d’un mélange la base de données permet de vérifier que le mélange
en cours est autorisé.

Les composantes du mélange en création sont tagués, le n°ECS des composantes est
récupéré et la composition d’ECS est comparée avec chaque ligne de la base de
donnée.

Si cette composition d’ECS est présente dans la base de donnée le programme
autorise la création sinon une alarme est émise.

L’ensemble ECS1+ECS2+….ECSm lu doit exister dans une des lignes ECSx-1, ECSx-2,
ECSx-3….,ECSx-n  de la base.

**Il suffit que toutes les composantes du mélange apparaissent dans une ligne
unique de la base**.

L’ordre d’apparition dans la ligne ne doit pas avoir d’importance
(ECS1,ECS2,..ECSm est accepté au même titre que ECSm,ECS1,..ECS2 par exemple).


