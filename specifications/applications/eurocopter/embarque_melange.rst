

.. index::
   pair: Applications; Mélange
   pair: Extension; Spécifications


.. _extension_specifications:

=====================================================================
Extension des spécifications => Fabrication d’un mélange dans un bac
=====================================================================


.. contents::
   :depth: 3

Introduction
=============

.. seealso::

   - la :ref:`table des associations entre un mélange et ses composants <table_melange_composants>`


L’application sur PDA doit pouvoir gérer la **fabrication d’un mélange** de peinture.

Cf Organigramme.

La création d’un mélange passe par 3 étapes:

- lecture des tags des peintures à mélanger (appelés tags ``pot``)
- vérifier dans une table interne au PDA la compatibilité du mélange
- création du code mélange à écrire dans le tag du bac à mélange (tag ``bac``)
  (écrire également les infos de date du mélange, :ref:`cf plan tag **BAC mélange** <tag_melange>`.


