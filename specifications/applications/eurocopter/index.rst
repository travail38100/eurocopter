

.. index::
   pair: Spécifications; PDA Eurocopter


.. _specs_pda_eurocopter:

================================================================
Spécifications de l'application PDA Eurocopter 
================================================================



L'application PDA Eurocopter est embarqué dans le pda Atex Tectus TMC13.56/868-EX.

.. toctree::
   :maxdepth: 3

   embarque_melange
   embarque_ouverture_pot
   plan_memoire
   base_de_donnees
