

.. index::
   pair: Applications; Ouverture pot
   ! Ouverture pot


.. _ouverture_pot:

========================================================
Ouverture du pot
========================================================

.. seealso::
 
   - :ref:`onglet_pot`
   - :ref:`delta_ouverture_pot_date_fab`



- décodage des données (extraction et reformatage des champs)
- réaliser l’IHM d’affichage des données
- réaliser l’IHM de modification des données
- stocker les données sous un format excel ou sqlite.


.. note:: Dans un premier temps le 4.2.3) se réduit à afficher un bouton «ouverture pot»
   qui déclenche la mise à jour du champ de la date «delta date ouverture» dans
   le tag.

Ce champ est formaté à partir de la «date» du pda.
