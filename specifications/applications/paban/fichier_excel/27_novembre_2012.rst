

.. index::
   pair: Spécifications; Excel
   pair: Table; Excel
   pair: Date; Livraison
   pair: Excel; Date de fabrication
   pair: Excel; Date de péremption
   pair: Fichiers; Excel
   ! Excel


.. _specs_excel_27_11_2012:

==============================================
Spécifications fichier Excel 27 novembre 2012
==============================================

.. figure:: 90px-Excel.png
   :align: center

.. seealso::

   - http://fr.wikipedia.org/wiki/Excel


.. contents::
   :depth: 4


Annonce réception spécification du fichier Excel Paban
======================================================

.. seealso::

   - :ref:`excel_to_sqlite3`


::

    Sujet:  Eurocopter
    Date :  Tue, 27 Nov 2012 14:00:56 +0100
    De :    Frederic GAUTIER <frederic.gautier@id3.eu>
    Répondre à :    frederic.gautier@id3.eu
    Organisation :  id3 Semiconductors
    Pour :  VERGAIN Patrick <patrick.vergain@id3.eu>


Voici la feuille :download:`.xls <CERTIFICAT_CONFORMITE_test_RFID.xls>` de Paban
à saisir pour validation du poste d'encodage.

FG

Fichier Excel (A1:J43)
======================


:download:`Télécharger le fichier excel <CERTIFICAT_CONFORMITE_test_RFID.xls>`



Position des champs dans la feuille
===================================


Les champs nécessaires au traitement interne
---------------------------------------------

Le champ "NOMBRE_DE_FEUILLES" nous permet de connaitre le nombre de feuilles
du fichier excel à traiter.


.. figure:: champ_nombre_de_feuilles.png
   :align: center



=========================  =======  ======= ========  ===================
CHAMP                      CELLULE  LIGNE   COLONNE   Commentaire
=========================  =======  ======= ========  ===================
NOMBRE_DE_FEUILLES         E5       5       5         1  Analyse syntaxique: "Nombre de feuilles : 1"
=========================  =======  ======= ========  ===================


Analyse syntaxique du nombre de feuilles (E5)
++++++++++++++++++++++++++++++++++++++++++++++

Le nombre de feuilles est de la forme : "Nombre de feuilles : 1"



.. _date_livraison_excel:

Les champs communs à une livraison
----------------------------------

.. seealso::

   - :ref:`pot_delta_date_livraison`



.. figure:: champs_communs.png
   :align: center

Les champs communs à une livraison.

=========================  =======  ======= ========  ===================
CHAMP                      CELLULE  LIGNE   COLONNE   Commentaire
=========================  =======  ======= ========  ===================
FOURNISSEUR                B3       3       2         PABAN S.A.S.
DATE_LIVRAISON             H15      15      8         20/11/2012 Analyse syntaxique: "123456 DU 20/11/2012"
UNITE_QUANTITE             G17      17      7         L  Analyse syntaxique: "Cond. en litres"
=========================  =======  ======= ========  ===================



Analyse syntaxique du nombre de feuilles (E5)
++++++++++++++++++++++++++++++++++++++++++++++

Le nombre de feuilles est de la forme : "Nombre de feuilles : 1"


Analyse syntaxique de la date de livraison (H15)
++++++++++++++++++++++++++++++++++++++++++++++++

La date de livraison est de la forme: "123456 DU 20/11/2012"


.. note:: La date de livraison contient le numéro du jour contrairement aux dates
   de fabrication et de péremption.



Analyse de l'unité de quantité (G17)
++++++++++++++++++++++++++++++++++++

A extraire à partir de la forme: "Cond. en litres"


Les champs multiples situés à partir de la ligne A20:J20
--------------------------------------------------------


.. figure:: champs_multi_lignes.png
   :align: center


Les champs multilignes sont situés à partir de la ligne 20 (A20:J20).

=========================  =======  ======= ========  ===================
CHAMP                      CELLULE  LIGNE   COLONNE   Commentaire
=========================  =======  ======= ========  ===================
REFERENCE_PRODUIT          A20      20      1         610795
NOM_PRODUIT                B20      20      2         STANDOFLEET  HS
NUMERO_ECS                 C20      20      3         2405.20 (sera tronqué à 6 caractères: "2405.2")
TEINTE                     D20      20      4         AFNOR 1625
TEMPERATURE_STOCKAGE_MIN   E20      20      5         Analyse syntaxique: 5-30
TEMPERATURE_STOCKAGE_MAX   E20      20      5         Analyse syntaxique: 5-30
QUANTITE_PRODUIT           F20      20      6         5
NB_TOTAL_POTS              F20      20      7         4
NUMERO_LOT                 H20      20      8         12112012092
DATE_FABRICATION           I20      20      9         09/2012
DATE_PEREMPTION            J20      20      10        09/2014
=========================  =======  ======= ========  ===================


Numéro ECS
+++++++++++

.. warning:: Le numéro ECS est tronqué à 6 caractères.
   Le caractère '.' est supprimé.


Date de fabrication de la peinture
+++++++++++++++++++++++++++++++++++

.. warning:: On peut remarquer que la date de fabrication de la peinture ne comporte pas le jour.


Date de péremption de la peinture
+++++++++++++++++++++++++++++++++++

.. warning:: On peut remarquer que la date de péremption de la peinture ne comporte pas le jour.


Analyse syntaxique des températures min et max de stockage (E20)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Les valeurs sont à extraire à partir de la forme: "5-30"



Les champs absents de la feuille excel
---------------------------------------

Les champs absents de la feuille excel.

=========================  =======  ======= ========  ===================
CHAMP                      CELLULE  LIGNE   COLONNE   Commentaire
=========================  =======  ======= ========  ===================
DELTA_POT_OUVERT                                      absent
LMP                   0       0                       absent
=========================  =======  ======= ========  ===================

















