
.. index::
   pair: Cas utilisation; Paban


.. _use_cases_data_flow:
.. _cas_util_paban:
.. _usecase_appli_paban:

===============================================
Les cas d'utilisation pour l'application Paban
===============================================

.. seealso::

   - :ref:`appli_encodage_paban`


.. figure:: usecases_paban.svg
   :align: center

.. toctree::
   :maxdepth: 7

   deposer_fichiers_excel/index
   tagger_pots/index
   purger_pots/index
   parametrer_eurocopter_ini/index




