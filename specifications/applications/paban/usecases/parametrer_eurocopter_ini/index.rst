
.. index::
   pair: Paramétres; Eurocopter.ini
   

.. _parametrer_eurocopter_ini:

========================================================
Modifier des paramétres avec le fichier Eurocopter.ini
========================================================



Le fichier :file:`Eurocopter.ini` est situé sous le répertoire 
:file:`%APPDATA%/Eurocopter`


Le fichier :file:`Eurocopter.ini` permet d'initialiser la valeur de 3 paramètres:

- le répertoire des logs (**RepertoireLogs**)
- le répertoire des fiochiers Excel (**RepertoireFichiersExcel**)
- la puissance du lecteur en milliwatts (**Puissance_milli_watts**)



.. literalinclude:: Eurocopter.ini
   :encoding: latin-1

 
   

