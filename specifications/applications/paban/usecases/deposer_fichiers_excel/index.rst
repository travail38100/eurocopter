
.. index::
   pair: Transfert; Fichiers Excel
   
   

.. _excel_to_sqlite3:
.. _deposer_fichiers_excel:

==============================================================================================================
Déposer les fichiers Excel dans le répertoire ``%APPDATA%\Eurocopter\ExcelFiles`` ou un répertoire quelconque
==============================================================================================================

.. seealso::

   - :ref:`specs_excel_27_11_2012`


.. contents::
   :depth: 3

Diagramme 
==========

Chaque fichier ``Excel`` est déposé:

- soit dans le répertoire :file:`%APPDATA%\\Eurocopter\ExcelFiles`
- soit dans un répertoire quelconque.



Dépot dans le répertoire :file:`%APPDATA%\\Eurocopter\ExcelFiles`
------------------------------------------------------------------

Si le fichier est déposé dans le répertoire :file:`%APPDATA%\\Eurocopter\ExcelFiles` 
alors les fichiers excel seront traités au démarrage du programme d'encodage.
**Ils sont détruits après avoir été transféré dans la base de données locale**.

Dépot dans un répertoire quelconque
-----------------------------------

Si le fichier est déposé dans un répertoire quelconque, ce fichier peut être
lu grâce au programme d'encodage. 

**Le fichier traité n'est pas détruit après traitement**.


.. figure:: bouton_lire_fichier_excel.png
   :align: center
   
   Lecture du fichier excel


Traitement des fichiers excel
------------------------------

Le programme extrait les informations de chaque fichier excel et met à jour
la base de données locale :file:`%APPDATA%/Eurocopter/DB_Paban.sqlite`.



.. figure:: excel_to_sqlite3.svg
   :align: center


.. figure:: repertoire_db_paban.png
   :align: center
   
   

Exemples de livraison
======================

.. toctree::
   :maxdepth: 3
   
   exemples/index
   
   
   


