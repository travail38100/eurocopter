
.. index::
   pair: Transfert; Fichiers Excel
   
   

==================================================================================
Exemple de livraison: Livraison du fichier ``CERTIFICAT_CONFORMITE_test_RFID.xls``
==================================================================================

.. contents::
   :depth: 3
   

Fichier excel
==============

.. figure:: fichier_excel.png 
   :align: center
   

Transfert du fichier dans le répertoire de traitement
======================================================

Le transfert d'un fichier ``Excel`` dans le répertoire ``%APPDATA%\\Eurocopter\\ExcelFiles``
provoque la mise à jour de la base de données ``%APPDATA%\\Eurocopter\\DB_Paban.sqlite``.


.. figure:: appdata_eurocopter_excelfiles.png
   :align: center


La base de données ``DB_Paban.sqlite`` est alimentée par le fichier ``Excel``
==============================================================================

.. figure:: django_admin.png
   :align: center
   

Combobox mise à jour à partir de la base de données
====================================================

La combobox de l'interface graphique est mise à jour au moyen de la base de
données.


.. figure:: encodage_paban.png
   :align: center


   


