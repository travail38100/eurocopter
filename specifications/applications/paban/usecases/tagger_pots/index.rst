
.. index::
   pair: Cas; Tagger un pot
   pair: Tagger; Pot 
   

.. _tagger_un_pot:

======================================
Tagger les pots à livrer à Eurocopter
======================================


.. contents::
   :depth: 5


Description
============

Prérequis
----------

Un fichier ``Excel`` a été déposé dans le :ref:`répertoire adéquat <deposer_fichiers_excel>`.


A ce moment là les produits à livrer apparaissent au niveau de l'interface 
graphique du poste d'encodage.
 

Diagramme 
==========

.. figure:: taggage_pots.svg
   :align: center


Exemples de taggage des pots
=============================

.. toctree::
   :maxdepth: 3
   
   exemples/index
   
   

