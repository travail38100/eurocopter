
==================
7) En cas d'erreur
==================


En cas d'erreur, il est possible d'éditer l'enregistrement en base de données.


On peut modifier les champs suivants:

- ``Livraison traitée`` si on pense qu'il faut retagger des pots
- ``Nb pots traités`` 

.. figure:: django_nb_pots_traites_modifies.png   
   :align: center
   
   
   
   

    




