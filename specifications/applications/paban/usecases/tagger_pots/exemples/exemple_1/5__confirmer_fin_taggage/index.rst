
======================================= 
5) Confirmer la fin taggage des 4 pots
=======================================


Si vous avez taggé tous les pots appuyer sur OK pour confirmer que tous les 
pots ont bien été taggé.

.. figure:: livraison_terminee.png
   :align: center   
  

Vérification dans la base de données
====================================   
  
On peut vérifier que l'enregistrement a bien été acquitté.

- ``LivraisonTraitée`` == 1   



.. figure:: django_livraison_traitee.png    
   :align: center
   
   




