
.. index::
   pair: STANDOFLEET; Usecase


.. _exemple_tagger_4_pots:

==========================================
Taggage des 4 pots "STANDOFLEET" à livrer
==========================================


.. toctree::
   :maxdepth: 3
   
   
   1__choisir_produit/index
   2__detecter_tag/index
   3__ecrire_tag/index
   4__repetition_taggage/index
   5__confirmer_fin_taggage/index
   6__produit_suivant/index
   7__cas_erreur/index
   
