

====================================================
3) Ecrire des informations "livraison" dans le tag 
====================================================


.. contents::
   :depth: 3


On écrit les informations concernant le produit en appuyant sur le bouton 
"Ecrire les information de livraison dans le tag"


Premier pot
===========


.. figure:: ecrire_pot1.png
   :align: center 
   
  
Confirmation de réécriture du tag
=======================================

Comme le tag a déjà été écrit, on demande une confirmation d'écriture:

   
.. figure:: tagger_produit_1_confirm.png   
   :align: center
   

L'écriture du tag s'est bien passé
=============================================

.. figure:: tagger_produit_1_confirm_ok.png   
   :align: center
   
 
 
