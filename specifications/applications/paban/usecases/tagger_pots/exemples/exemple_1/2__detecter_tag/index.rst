

=========================================================
2) Détecter la présence d'un pot dans le champ du lecteur
=========================================================


On présente un premier pot "STANDOFLEET" équipé d'un tag et on le détecte
en appuyant sur le bouton **LectureTag**



Quand le tag est détecté, le bouton "Ecrire les informations de livraison dans le tag"
apparait en vert.


.. figure:: lancer_inventaire_2.png
   :align: center  



