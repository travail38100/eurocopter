
.. index::
   pair: Cas; Suppression des pots de la base de données locale
   pair: Pots ; Base de données locale 
   

.. _purger_base_locale:

==================================================
Suppression des pots de la base de données locale
==================================================



On supprime les pots de la base de données locale en sélectionnant cliquant 
sur l'item suivant:

.. figure:: bouton_purger_pots.png
   :align: center


   

