

.. index::
   pair: Application; Paban


.. _specs_application_paban:

=========================================
Spécifications des 2 applications Paban 
=========================================

.. seealso::

   - :ref:`entreprise_paban`
   - :ref:`applis_paban`
   - :ref:`gui_appli_paban`



.. contents::
   :depth: 3

Description
===========

.. seealso::

   - :ref:`applis_paban`


Il existe 2 applications ``Paban``.

L'application d'encodage des données
------------------------------------

.. seealso::

   - :ref:`appli_encodage_paban`
   - :ref:`specs_fichiers_excel`


Cette application :ref:`graphique <appli_encodage_paban>` permet:

- d'écrire dans une :ref:`base de données locale <db_tables_paban>` les données 
  issues des :ref:`fichiers excel <specs_fichiers_excel>` 
- de permettre à un opérateur de sélectionner ces données et de les écrire  
  dans le :ref:`plan mémoire <plan_memoire_pot>` d'une :term:`Puce RFID` fixée 
  à un pot de peinture.


L'application de gestion de la base de données
-----------------------------------------------

.. seealso::

   - :ref:`appli_db_paban`

Cette application permet de consulter/vérifier la :ref:`table de données <table_livraison_pots>`  
mise à jour par l'application d'encodage.


Portabilité des applications ``Paban``
=======================================


**Les applications Paban doivent fonctionner sur Windows XP et Windows Seven**.



Cas d'utilisation
==================

.. toctree::
   :maxdepth: 3
   
   usecases/index
   
  
Spécifications du format des fichiers Excel
===========================================

.. toctree::
   :maxdepth: 3
   
   
   fichier_excel/index
   



