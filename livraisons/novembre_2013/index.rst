

.. index::
   pair: Livraisons; Novembre 2013
   pair: Livraisons; 0.4.0



.. _livraisons_eurocopter_2013:

==========================================
Livraisons Eurocopter novembre 2013
==========================================


.. contents::
   :depth: 3   



Livraison Paban
================


Livraison 0.4.0
----------------

.. figure:: livraison_0_4_0.png
   :align: center
   
   
Désinstallation PC Encodage Paban
++++++++++++++++++++++++++++++++++

   
.. figure:: desinstaller_pc_encodage_paban.png
   :align: center

   

Champ puissance milliwatts
----------------------------

.. figure:: champ_puissance_milliwatts.png
   :align: center
  
  

Commun
======
   
Maj Django 1.5 -> 1.6
----------------------

Maj Django 1.5 -> 1.6.

.. figure:: maj_django_1.5__1.6.png
   :align: center
   
 

Eurocopter
==========

   
Info Path db Eurocopter
------------------------

Info Path db Eurocopter.


.. figure:: path_db_eurocopter.png
   :align: center
   
    
Désinstaller BD Eurocopter
---------------------------
    
.. figure:: desinstaller_bd_eurocopter.png    
   :align: center
   
   
Install application db_eurocopter   
----------------------------------

Install application db_eurocopter. 

.. figure:: install_application_db_eurocopter.png
   :align: center
   
   
   
