@ECHO OFF

REM Command file for id3 Sphinx documentation

if "%SPHINXBUILD%" == "" (
	set SPHINXBUILD=sphinx-build
)

REM Id3 Technologies doc 
REM ====================

set DOC_CLIENT=id3

make.bat %1
