

.. index::
   pair: Glossaire; Eurocopter


.. _glossaire_eurocopter:

=====================================
Glossaire Eurocopter
=====================================

.. glossary::

   

   Laboratoire Matériaux et Procédés
   LMP
       Laboratoire Matériaux et Procédés

       .. seealso:: :ref:`lmp`

   OF
   Ordre de Fabrication
       Ordre de Fabrication
       Un ordre de fabrication est codé sur 10 caractères.


   PDA
       Ordinateur de poche équipé d'un lecteur UHF permettant la lecture/écriture
       d'un :term:`Tag` (alias :term:`Radio-étiquette`).
       
       .. seealso:: :ref:`lecteur_bartec_9190`       

       
   Pot
       Conteneur du produit à tracer à toutes les étapes de sa vie.
       Chaque pot est équipé d'un :term:`Tag` (alias :term:`Radio-étiquette`).
    
    
   Tag
   Tags
   tags
   Radio-étiquettes
   Radio-étiquette
   Puces RFID
   Puce RFID
   puce RFID   
   
       Support physique des données de traçabilité.
       
       Les radio-étiquettes sont de petits objets, tels que des étiquettes 
       autoadhésives, qui peuvent être collés ou incorporés dans des objets ou 
       produits et même implantés dans des organismes vivants (animaux, corps humain). 
       
       Les radio-étiquettes comprennent une antenne associée à une puce 
       électronique qui leur permet de recevoir et de répondre aux requêtes 
       radio émises depuis l’émetteur-récepteur.

       Ces puces électroniques contiennent un identifiant et éventuellement des 
       données complémentaires.

       Cette technologie d’identification peut être utilisée pour identifier:

          - les objets, comme avec un code-barres (on parle alors d’étiquette électronique) 
       
       Contrairement aux étiquettes à code barre, les informations contenues 
       peuvent être mises à jour à tout moment.     
      
       Définitions Légifrance du 9 août 2013

       .. seealso::      
                      
          - https://fr.wikipedia.org/wiki/Radio-%C3%A9tiquette
          - http://www.legifrance.gouv.fr/affichTexte.do;jsessionid=?cidTexte=JORFTEXT000027823415&dateTexte=&oldAction=dernierJO&categorieLien=id
       

   Traçabilité
       La traçabilité désigne la situation où l'on dispose de l'information 
       nécessaire et suffisante pour connaitre (éventuellement de façon rétrospective) 
       la composition d'un matériau ou d'un produit tout au long de sa chaîne de 
       production et de distribution.
       Et ce, en quelque endroit que ce soit, et depuis l'origine première du produit 
       jusqu'à sa fin de vie, soit comme le dit l'adage :« du berceau jusqu'à la tombe » 
       pour les produits industriels, ou « de la fourche à la fourchette » pour un 
       produit alimentaire. 
       Les règles et bonnes pratiques en matière de traçabilité sont déterminées par 
       des normes et/ou des organismes de Contrôle nationaux ou internationaux. 
       Ainsi en Europe, la traçabilité des aliments est sous le contrôle de l'EFSA,
       (Autorité européenne de sécurité des aliments).

       Le terme vient de l'anglais traceability qui signifie littéralement 
       « aptitude au traçage » ; il n'est pas reconnu mais simplement toléré par 
       l'Académie française.
   
       .. seealso:: https://fr.wikipedia.org/wiki/Tra%C3%A7abilit%C3%A9
       
       
       La traçabilité a profité de progrès majeurs, liés à l'informatique et à 
       sa miniaturisation, avec notamment trois outils :

       - les codes-barres, permettant une identification rapide et automatique 
         par lecteur optique. 
         La puce électronique qui peut contenir plus d’informations tend à les 
         remplacer pour les usages **sophistiqués**.
       - les radio-étiquettes (ou "puces" RFID) qui permettent un suivi complet 
         de la vie d'un produit. 
         En effet, elles sont à lecture/écriture et ce à distance (jusqu'à 
         plusieurs mètres) d'une capacité de 64 caractères et peuvent se 
         reprogrammer plusieurs millions de fois. 
         L'alimentation et l'échange des données en lecture ou en écriture se 
         fait sur une porteuse inductive. La puce électronique étant «passive» 
         ne nécessite pas d'être connectée à une source d'énergie.
       - les :term:`bases de données` plus ou moins interconnectées, permettant de 
         suivre un grand nombre de références et d'effectuer des recoupements 
         de manière automatisée.

       
       
       
   
