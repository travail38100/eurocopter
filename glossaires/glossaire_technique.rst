

.. index::
   pair: Glossaire; Technique


.. _glossaire_technique:

=====================================
Glossaire technique
=====================================

.. glossary::


   bases de données
   base de données
   Base de données
   
       Une base de données (en anglais ``database``) est un conteneur informatique 
       permettant de stocker - le plus souvent dans un même lieu - l'intégralité 
       des informations en rapport avec une activité. 
       
       Une base de données permet de stocker et de retrouver un ensemble 
       d'informations de plusieurs natures ainsi que les liens qui existent 
       entre les différentes informations.
       
       .. seealso::
       
          - :ref:`db_eurocopter`
          - :ref:`db_paban`
       
   
   C♯
       .. image:: Logo_CSharp.png

       Le C♯ (prononcé [siː.ʃɑːp]) est un langage de programmation orienté objet
       à typage fort, créé par la société Microsoft, et notamment un de ses
       employés, `Anders Hejlsberg`_, le créateur du langage Delphi et TypeScript_.

       Il a été créé afin que la plate-forme Microsoft .NET soit dotée d'un
       langage permettant d'utiliser toutes ses capacités. Il est très proche
       du Java dont il reprend la syntaxe générale ainsi que les concepts
       (la syntaxe reste cependant relativement semblable à celles de langages
       tels que le C++ et le C). Un ajout notable à Java est la possibilité de
       surcharge des opérateurs, inspirée du C++. Toutefois, l'implémentation de
       la redéfinition est plus proche de celle du Pascal Objet.

       L'interface graphique des projets est écrit en C♯.

       .. seealso::

          - http://fr.wikipedia.org/wiki/C_sharp

       .. _TypeScript: http://en.wikipedia.org/wiki/TypeScript
       .. _`Anders Hejlsberg`:  _http://en.wikipedia.org/wiki/Anders_Hejlsberg

   CRC
       En informatique et dans certains appareils numériques, un contrôle de
       redondance cyclique ou CRC (Cyclic Redundancy Check) est un outil logiciel
       permettant de détecter les erreurs de transmission ou de transfert par
       ajout, combinaison et comparaison de données redondantes, obtenues grâce
       à une procédure de hachage. Ainsi, une erreur de redondance cyclique peut
       survenir lors de la copie d'un support (disque dur, CD-Rom, DVD-Rom,
       clé USB, etc.) vers un autre support de sauvegarde.

       .. seealso::  http://fr.wikipedia.org/wiki/Contr%C3%B4le_de_redondance_cyclique

   EPC
       An Electronic Product Code, or EPC, is an electronic tag that contains a
       unique number. This number identifies one product from another.
       EPC is often called the next generation of the standard bar code.
       Like the bar code, EPC uses a numerical system to identify a product.

       An EPC is a number that can be associated with specific product information,
       such as date of manufacture, origin and destination of shipment.

       This information provides significant advantages for businesses and
       consumers. EPCs do not carry personally identifiable information.

       .. seealso:: http://www.gs1.org/aboutepc/faqs
       
        
   PC (Protocol Control)
       LLLLLRRNNNNNNNNN

       LLLLL

           - 5 bits
           - Longueur en word (16 bits) de PC (Protocol Control) + code EPC (Electronic Product Code)
           - LLLLL = 0 => 16 bits
           - LLLLL = 1 => 32 bits
           - LLLLL = 2 => 48 bits   => EPC = 32 bits
           - ...
           - LLLLL = 8 => 144 bits  => EPC = 128 bits
             C'est le code qui nous intéresse

   Singulation
       Definition 1
           Singulation is a method by which an RFID reader identifies a tag with a
           specific serial number from a number of tags in its field.

           This is necessary because if multiple tags respond simultaneously to a
           query, they will jam each other.

           In a typical commercial application, such as scanning a bag of groceries,
           potentially hundreds of tags might be within range of the reader.


           .. seealso:: http://en.wikipedia.org/wiki/Singulation


       Definition 2
           The ability to encode or read an RFID tag without interfering with a
           tag nearby.


           .. seealso:: http://computer.yourdictionary.com/tag-singulation


   TID
       Transponder ID (TID) number, which identifies the chip type and any
       custom commands and optional features it supports, to authenticate the tag.

       .. seealso:: http://www.rfidjournal.com/article/view/4332
        
   Visual Studio 2012

        Environnement de développement utilisé pour le développement de
        l'interface graphique en C#.

        Un environnement alternatif pourrait être monodevelop ou sharpdevelop.

        .. seealso::

           - http://fr.wikipedia.org/wiki/Visual_Studio#Visual_Studio_2012
           - http://en.wikipedia.org/wiki/Microsoft_Visual_Studio#Visual_Studio_2012

        .. figure:: logo_vs_2012.png

           *Logo visual studio 2012*
