@ECHO OFF

REM Command file for Eurocopter Sphinx documentation

if "%SPHINXBUILD%" == "" (
	set SPHINXBUILD=sphinx-build
)

REM Eurocopter doc 
REM ===============

set DOC_CLIENT=eurocopter

make.bat %1
