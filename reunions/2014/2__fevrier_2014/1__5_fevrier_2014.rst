

.. index::
   pair: Réunions; Eurocopter (5 février 2014)



.. _reunion_5_fevrier_2014:

=============================================
Réunion interne du mercredi 5 février 2014
=============================================

.. contents::
   :depth: 3


Présents
========

- Christophe Candela
- Frédéric Gautier
- Patrick Vergain


Résumé
======

Eurocopter ne veut pas payer le projet si on ne fait pas certaines modifications
sur les PDAs:

- ajout d'alertes
- modifications de la saisie des OF.
- modifications concernant l'interface de gestion de la base de données.


