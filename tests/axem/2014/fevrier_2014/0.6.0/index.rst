

.. index::
   pair: Tests; Axem (Février 2014, 0.6.0)



.. _tests_axem_fevrier_2014_0.6.0:

======================================
Tests Axem février 2014 version 0.6.0
======================================


.. contents::
   :depth: 3


Bac mélange de type P81001
---------------------------

.. figure: bac_melange_type_p81001.jpg
   :align: center
   
   
.. warning:: la durée de vie de ce bac mélange est de 3 heures (180 minutes).
   C'est pour cette raison qu'il faut en fabriquer régulièrement.   


.. _type_melange_p81001:

Type de mélange P81001
=======================

.. figure: melange_P81001.png
   :align: center



Saisie des OFs
===============

.. figure: saisie_ofs.jpg
   :align: center

Lire tag
===============

.. figure: lire_tag.jpg
   :align: center
   
 
Saisie de l'ordre de fabrication 300
====================================


.. figure: of_300.jpg
   :align: center
   
 
Saisie de l'ordre de fabrication 303
====================================


.. figure: of_303.jpg
   :align: center
   
  
Résultat dans la base de données
=================================


.. figure: 3e_ouverture_bac_ofs_300.jpg
   :align: center
   

  


   
