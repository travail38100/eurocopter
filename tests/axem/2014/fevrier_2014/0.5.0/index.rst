

.. index::
   pair: Tests; Axem (Février 2014, 0.5.0)



.. _tests_axem_fevrier_2014_0.5.0:

===========================
Tests Axem février 2014
===========================


.. contents::
   :depth: 3



.. _preconditions_tests:

Préconditions
==============

.. figure:: 3_constituants_melange.JPG
   :align: center

Pour faire un bac_mélange, il faut 3 pots constituant un type de mélange possible:

- 2K HS Hardener (durcisseur)
- 2K THINNER (Diluant)
- Standofleet 1625  (base)

Ces 3 constituants appartiennent au type de mélange :ref:`P81001 <type_melange_p81001_5>`.



Composant 2K HS Hardener
-------------------------

Référence
++++++++++

.. figure:: 2k_hs_hardener.jpg
   :align: center


Dates
++++++


.. figure:: 2k_hs_hardener_dates.jpg
   :align: center




Pots périmés
------------

.. warning:: Ces pots ne doivent pas être périmés.

Dans le cas où ils sont périmés, il faut les réencoder au moyen
de :ref:`l'application d'encodage Paban <appli_encodage_paban>`.



Bac mélange de type P81001
---------------------------

.. figure:: bac_melange_type_p81001.jpg
   :align: center


.. warning:: la durée de vie de ce bac mélange est de 3 heures (180 minutes).
   C'est pour cette raison qu'il faut en fabriquer régulièrement.


.. _type_melange_p81001_5:

Type de mélange P81001
=======================

.. figure:: melange_P81001.png
   :align: center



Saisie des OFs
===============


Lire tag
===============

.. figure:: lire_tag.jpg
   :align: center


Saisie de l'ordre de fabrication 300
====================================


.. figure:: of_300.jpg
   :align: center


Saisie de l'ordre de fabrication 303
====================================


.. figure:: of_303.jpg
   :align: center


Résultat dans la base de données
=================================


.. figure:: 3e_ouverture_bac_ofs_300.jpg
   :align: center






