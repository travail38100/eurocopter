

.. index::
   pair: Versions; 0.6.0 (Eurocopter)


.. _version_eurocopter_0.6.0:

===========================================================
Changements dans le version Eurocopter 0.6.0 
===========================================================

.. contents::
   :depth: 3


3 tables de la base de données changent de nom
================================================

* CMelange -> CTypeMelange
* CMelange_composants -> CTypeMelange_composants
* COuvertureBac -> COrdreFabrication


Ajout d'un champ ``compteur_bac_melanges`` dans la table CTypeMelange
======================================================================


::

    class CTypeMelange(models.Model):
        u''' Un type de mélanges.
        '''
        nom_melange = models.CharField(max_length=7, unique=True)
        temps_de_murissement = models.IntegerField(help_text=u"Le temps nécessaire (en minutes) au mûrisssement du mélange <= 999 minutes")
        duree_de_vie = models.IntegerField(help_text=u"La durée de vie (en minutes) du mélange <= 999 minutes")
        composants = models.ManyToManyField(Ccomposant, help_text=u"Un mélange est constitué de n composants")
        compteur_bac_melanges = models.IntegerField(default=0, help_text=u"Le compteur de bacs mélange créé avec ce type de mélange")
        class Meta:
            db_table = u'CTypeMelange'
            verbose_name = u'Type de mélange'
            verbose_name_plural = u'Types de mélanges'
            
        def __unicode__(self):
            return self.nom_melange


Modification de noms de champs dans la table COrdreFabrication
===============================================================


- date_ouverture ->  date_saisie
- numero_ouverture -> numero



