

.. index::
   pair: Outils; Développement


.. _outils_dev:

=======================================
Outils utilisés pour le développement
=======================================


.. toctree::
   :maxdepth: 3

   bash/index
   django/index
   sphinx/index
   visual_studio/index





