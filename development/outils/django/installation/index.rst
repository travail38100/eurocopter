


.. _install_bdd_pc_paban:

============================================
Installation de la base de données PC Paban
============================================

.. seealso::

   - https://docs.djangoproject.com/en/1.5/intro/tutorial01/

Issu du livre "Two scoops of Django".


::

    (eurocopter) ...XLOG2Y057_IHMs_Paban_Eurocopter\0.1.0\projects\bd_paban>python C:\Users\pvergain\Envs\eurocopter\Scripts\django-admin.py startproject --template=https://github.com/twoscoops/django-twoscoops-project/zipball/master --extension=py,rst,html pc_paban_project


J'ai renommé:

- ``pc_paban_project`` en ``pc_paban_root`` (au plus haut niveau)
- ``pc_paban_project`` en ``project`` (2e niveau)
- ``pc_paban_project`` en ``conf`` (3e niveau)

::


    \---pc_paban_root
        |   README.rst
        |   requirements.txt
        |
        +---docs
        |       conf.py
        |       deploy.rst
        |       index.rst
        |       install.rst
        |       make.bat
        |       Makefile
        |       __init__.py
        |
        +---pc_paban_project
        |   |   manage.py
        |   |
        |   +---pc_paban_project
        |   |   |   urls.py
        |   |   |   wsgi.py
        |   |   |   __init__.py
        |   |   |
        |   |   \---settings
        |   |           base.py
        |   |           local.py
        |   |           production.py
        |   |           test.py
        |   |           __init__.py
        |   |
        |   +---static
        |   |   +---css
        |   |   |       bootstrap-responsive.min.css
        |   |   |       bootstrap.min.css
        |   |   |       project.css
        |   |   |
        |   |   +---img
        |   |   |       glyphicons-halflings-white.png
        |   |   |       glyphicons-halflings.png
        |   |   |
        |   |   \---js
        |   |           bootstrap.min.js
        |   |           project.js
        |   |
        |   \---templates
        |           404.html
        |           500.html
        |           base.html
        |
        \---requirements
                base.txt
                local.txt
                production.txt
                test.txt


python manage.py runserver
==========================

::

    (eurocopter) C:\projects_id3\P2X238\XLOG2Y057_IHMs_Paban_Eurocopter\0.1.0\projects\bd_paban\pc_paban_root\pc_paban_project>python manage.py runserver
    
  
::
    
    Validating models...
    0 errors found
    July 02, 2013 - 09:20:05
    Django version 1.5.1, using settings 'pc_paban_project.settings.local'
    Development server is running at http://127.0.0.1:8000/
    Quit the server with CTRL-BREAK.
    
    
Création de l'application ``livraisons``
=========================================

::

    python django_admin.py --settings=conf.settings.local startapp livraisons 

::

    (eurocopter) C:\projects_id3\P2X238\XLOG2Y057_IHMs_Paban_Eurocopter\0.1.0\projects\bd_paban\pc_paban_root\project>python django_admin.py startapp livraisons apps --settings=conf.settings.local
    
:: 
    
    CommandError: Destination directory 'C:\projects_id3\P2X238\XLOG2Y057_IHMs_Paban_Eurocopter\0.1.0\projects\bd_paban\pc_paban_root\project\apps' does not exist, please create it first. 

    
