

.. index::
   pair: Outils; Django


.. _django_tool:

==================================
Django
==================================

.. seealso::

   - :ref:`django`


.. toctree::
   :maxdepth: 3
   
   installation/index
