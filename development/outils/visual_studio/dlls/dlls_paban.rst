

.. index::
   pair: Dll; Paban
   pair: PIA; Primary Interop Assemblies



.. _dlls_paban_utilisees:

========================
Dlls utilisés par Paban
========================


.. contents::
   :depth: 3


.. _dlls_pia:

Primary Interop Assemblies
===========================


.. seealso::

   - http://www.microsoft.com/en-us/download/confirmation.aspx?id=3508


Pourquoi
========

Pour lire les fichiers excel.


Installation
-------------

To install the Office 2010 Primary Interop Assemblies, click the Download 
button, then click Save and select a location for PIARedist.exe on your 
computer. 

Run the executable to extract the O2010pia.msi file. 
Use one of the following installation methods to install O2010pia.msi:

- Double-click the O2010pia.msi file
- Execute msiexec.exe /i O2010pia.msi, or
- Wrap the O2010pia.msi file in another setup package through 
  Visual Studio or other Windows Installer aware setup editor. 
  Please note that the Office 2010 Primary Interop Assemblies setup does 
  not support the /a or /j options for MsiExec.

    
