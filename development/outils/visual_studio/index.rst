

.. index::
   pair: Outils; Visual Studio
   pair: Dll; Primary Interop Assemblies (PA)
   pair: PIA; Primary Interop Assemblies


.. _visual_studio:

==================================
Visual Studio 
==================================

.. seealso::

   - :term:`Visual Studio 2012`



Introduction
============

C'est l'environnment C# utilisé pour développer l'interface graphique
permettant de prendre en compte la livraison d'un pot de peinture.


Dlls utilisés
==============

.. toctree::
   :maxdepth: 3
   
   dlls/index
   
   
   
   
    
