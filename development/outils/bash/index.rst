

.. index::
   pair: Outils; Bash


.. _bash_commandes_on_windows:

==================================
Les commandes GNU/bash sur Windows
==================================

.. contents::
   :depth: 3


Introduction
============

Pour utiliser les commandes GNU telles que la commande :command:`tail` par exemple
il faut installer msysgit_ .

.. _msysgit: http://msysgit.github.com/


Commande ``tail``
=================

On utilise la commande :command:`tail` pour observer le déroulement du programme
d'acqusition en regardant l'évolution du fichier de logs.

.. figure:: commande_tail.png
   :align: center

   Observation du fichier de logs


