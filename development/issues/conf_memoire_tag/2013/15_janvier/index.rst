


.. _pb_memoire_tag_15_janvier_2013:

===================================================================
Problème de configuration de la mémoire tag Monza (15 janvier 2013)
===================================================================

.. contents::
   :depth: 3



Comment utiliser la commande Monza4QTWrite pour pouvoir écrire dans la mémoire publique d'un tag Monza4QT ?
============================================================================================================

::


    Sujet:  Comment utiliser la commande Monza4QTWrite pour pouvoir écrire dans la mémoire publique d'un tag Monza4QT
    Date :  Tue, 15 Jan 2013 14:52:13 +0100
    De :    Patrick VERGAIN <patrick.vergain@id3.eu>
    Organisation :  id3 Technologies
    Pour :  Nicolas.haran@nordicid.com



Bonjour Mr Haran,

Je suis un développeur de la société id3 Technologies et c'est Mr Marc Lavorel
qui m'a passé votre adresse en m'indiquant que vous étiez Français mais basé
en Finlande.

Nous utilisons la bibliothèque C# NurApiDotNet pour lire des tags Monza 4QT et
nous voudrions avoir des informations concernant la fonction Monza4QTWrite()
permettant d'utiliser soit la mémoire publique soit la mémoire privée.
dont le prototype est::

    public void Monza4QTWrite(uint passwd, bool reduce, bool pubmem, byte sBank, uint sAddress, int sMaskBitLength, byte[] sMask);


Nous avons vu que le rôle de cette fonction est d'écrire les bits QT_SR et
QT_MEM (doc NurApi .NET Documentation.chm).


::

    Monza4 QT command.

    Writes the QT_SR and QT_MEM bits.

    public:
    void Monza4QTWrite(
        unsigned int passwd,
        bool reduce,
        bool pubmem,
        unsigned char sBank,
        unsigned int sAddress,
        int sMaskBitLength,
        array<unsigned char>^ sMask
    )


    Parameters
    passwd
    Type: System..::..UInt32
    Password for secured operations. Password is always needed.
    reduce
    Type: System..::..Boolean
    The QT_SR bit (0/1) (range reduction).
    pubmem
    Type: System..::..Boolean
    The QT_MEM bit (0/1) (public private memory selection).
    sBank
    Type: System..::..Byte
    Memory bank used for tag singulation. 0=BANK_PASSWD 1=BANK_EPC 2=BANK_TID 3=BANK_USER
    sAddress
    Type: System..::..UInt32
    Singulation data address in bits.
    sMaskBitLength
    Type: System..::..Int32
    Length of the mask data in bits.
    sMask
    Type: array<System..::..Byte>[]()[][]
    Mask data buffer.


Situation actuelle
==================

Par défaut, le tag est est "private" et nous pouvons écrire dans les mémoires
128 bits et 512 bits.

Problème rencontré
==================

J'utilise la commande suivante pour passer en mode "public"::

    byte[] smask = new byte[1];
    smask[0] = 0x00;
    lecteurNordic.Monza4QTWrite(0
                                 , false  // reduce
                                 , !mem_is_private  // passer en mémoire public ou privé
                                 , NurApi.BANK_EPC
                                 , 0
                                 , 1
                                 , smask);

Pas de problème lorsque l'on passe en mode "privé"
---------------------------------------------------

On peut écrire dans les mémoires 128 et 512 bits quand on passe en mode "privé".


Problèmes rencontrés lorsque l'on passe en mode "public"
--------------------------------------------------------

Quand on passe en mode "public", il est impossible dans la mémoire
publique. Voici le message d'erreur que j'obtiens en utilisant le programme
de démonstration RFID demo::

  "NurApiWriteTag32; NUR API error 4111: The specified memory location is locked and/or permalocked and is either not writeable or not readable"


.. figure:: pb_erreur_ecriture_epc_public.png
   :align: center

.. figure:: pb_ecriture_epc_public.png
   :align: center


Voici ma question: Pourquoi ne peut-on pas écrire dans les mémoires quand on est en mode "public"


Je n'ai trouvé aucun exemple d'utilisation de cette fonction aussi bien sur
le web que dans les manuels décrivant la bibliothèque.

Auriez-vous une idée de ce qu'il faut faire pour que l'on puisse passer en mode
"public" pour pouvoir ensuite écrire dans les mémoires ?

L'utilisation des 3 derniers paramètres de la fonction est-il correct ?
Je n'ai pas vu d'exemple d'utilisation (c'est la première fois que je travaille sur une application RFID).


Cordialement.





