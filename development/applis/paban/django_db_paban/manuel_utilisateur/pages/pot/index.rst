
.. index::
   pair: Page; Pot (DB_Paban)


.. _page_django_paban_pot:

==============================================================
Page de gestion des Livraisons de pots de la base DB_Paban
==============================================================

.. seealso::

   - :ref:`table_livraison_pots`
   

.. contents::
   :depth: 3

Affichage des livraisons de pots
=================================


.. figure:: les_pots.png
   :align: center
  

Export de la table de livraison des pots
========================================


Confirmation de la création
============================

.. figure:: confirm_creation.png
   :align: center
   
   

