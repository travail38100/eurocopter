

.. index::
   pair: Base de données; Paban
   pair: Base de données; DB_Paban.sqlite


.. _db_paban:
.. _sqlite_paban:
.. _appli_db_paban:

==================================================================================
L'application de gestion de la base de données ``DB_Paban.sqlite`` 
==================================================================================

.. seealso::

   - :term:`bases de données`
   - :ref:`installation_db_paban`
   

.. contents::
   :depth: 3


Emplacement de la base de données ``DB_Paban.sqlite``
======================================================

La base de données sur PC est situé sous le répertoire :file:`$APPDATA$/Eurocopter`
et elle se nomme :file:`DB_Paban.sqlite`.

La base est mise à jour par l'application d'encodage.


Tables de la base ``DB_Paban.sqlite``
=====================================

.. toctree::
   :maxdepth: 3

   tables/index


Manuel utilisateur de la base de données ``DB_Paban.sqlite``
=============================================================

.. toctree::
   :maxdepth: 3

   manuel_utilisateur/index













