

.. index::
   pair: Table; CLivraisonPots
   pair: Pots; Livraison


.. _table_historique_tag_pot:
.. _table_livraison_pots:

=========================
Table "CLivraisonPots"
=========================

.. seealso::

   - :ref:`tag_pot`

.. contents::
   :depth: 3



Introduction
=============

La table ``CLivraison_pots`` contient les informations issues des fichiers 
excel concernant la livraison de pots de peinture.

Ces informations se retrouvent en partie sur un :ref:`tag pot <tag_pot>`.


Ajout d'enregistrements à partir de fichiers Excel
==================================================

Lorsqu'un fichier excel est copié dans le répertoire :file:`%APPDATA%/Eurocopter/ExcelFiles` 
il est automatiquement traité par le logiciel d'encodage qui se charge d'ajouter 
de nouvelles entrées dans la table ``CLivraison_pots`` de la base de données.


Après traitement, **le fichier fichier excel est supprimé**.


.. versionadded:: 0.4
   Si le fichier excel ne vient pas du répertoire :file:`%APPDATA%/Eurocopter/ExcelFiles` 
   alors **il n'est pas détruit.**


Liste des champs
=================

::

    class CLivraisonPots(models.Model):
        id = models.IntegerField(primary_key=True)
        nom_produit = models.CharField(max_length=15)
        reference_produit = models.CharField(max_length=20)
        fournisseur = models.CharField(max_length=15)
        numero_lot = models.CharField(max_length=30)
        teinte = models.CharField(max_length=15)
        quantite_produit = models.DecimalField(max_digits=8, decimal_places=2)
        unite_quantite = models.CharField(max_length=2)
        code_ecs = models.CharField(max_length=6)
        date_fabrication = models.DateTimeField()
        date_peremption = models.DateTimeField()
        date_livraison = models.DateTimeField()
        date_ouverture_pot = models.DateTimeField(null=True, blank=True)
        nb_jours_peremption_pot_ouvert = models.IntegerField()
        nb_total_pots = models.IntegerField()
        nb_pots_traites = models.IntegerField()
        temperature_stockage_min = models.DecimalField(max_digits=8, decimal_places=2)
        temperature_stockage_max = models.DecimalField(max_digits=8, decimal_places=2)
        lmp = models.IntegerField()
        livraison_traitee = models.IntegerField()
        class Meta:
            db_table = u'CLivraisonPots'
            verbose_name = _(u'Livraison des pots de peinture')
            verbose_name_plural = _(u'Livraisons des pots de peinture')
        def __unicode__(self):
            return self.nom_produit + self.code_ecs
            

Visualisation d'un enregistrement avec Django
=============================================

.. figure::  django_livraison_pots.png
   :align: center
   
   
