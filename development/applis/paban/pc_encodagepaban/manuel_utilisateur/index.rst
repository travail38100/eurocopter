

.. index::
   pair: Manuel utilisateur; Encodage Paban


.. _manu_util_appli_encodage_paban:

===============================================================================
Manuel utilisateur de l'application d'encodage des données ``PC_EncodagePaban``
===============================================================================


.. contents::
   :depth: 3
   

Introduction
============

Le but du poste d'encodage est de graver sur une radio-étiquette certaines
données issues de la :ref:`base de données Paban <db_paban>`.


.. figure:: encodage_paban.png
   :align: center


Choix de la livraison 
=====================

Le choix des informations à graver se fait au moyen de la combobox des produits.


.. figure:: combo_des_produits.png
   :align: center
   
 
 
Lecture de la radio-étiquette (tag)
===================================

.. figure:: bouton_lecture_tag.png
   :align: center


En appuyant sur le bouton ``Lecture tag``, on déclenche la lecture des informations
lues sur une radio-étiquette.


Si il n'y a pas de lecteur, la fenêtre suivante apparait:

.. figure:: lecteur_rfid_non_connecte.png
   :align: center
   
 
 
Ecriture des données dans la radio-étiquette
=============================================


Si une radio-étiquette a été détecté, on peut appuyer sur le bouton 
"Ecrire les informations de livraison dans le tag".


Si aucun tag n'est détecté alors le bouton est grisé

.. figure:: bouton_enregistrer_non_valide.png
   :align: center
   
   

Versions du Manuel Utilisateur
==============================

.. toctree::
   :maxdepth: 3
   
   versions/index
   
   
   

   
