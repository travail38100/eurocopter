

.. index::
   pair: Paban ; GUI
   pair: PC_EncodagePaban; Application


.. _appli_encodage_paban:
.. _gui_lecteur_nordic:
.. _gui_appli_paban:


=========================================================
L'application d'encodage des données ``PC_EncodagePaban``
=========================================================


.. seealso::

   - :ref:`specs_application_paban`

.. contents::
   :depth: 3


Interface graphique
===================


.. figure:: manuel_utilisateur/encodage_paban.png
   :align: center
   
   
Cas d'utilisation
==================

.. seealso::

   - :ref:`usecase_appli_paban`   

Spécification des fichiers Excel
================================

.. seealso::

   - :ref:`specs_fichiers_excel` 
   
  
Spécification du plan mémoire pour la puce RFID ``pot``
========================================================

.. seealso::

   - :ref:`plan_memoire_pot` 
   
   
Manuel utilisateur de l'application
===================================

.. toctree::
   :maxdepth: 3
   
   manuel_utilisateur/index
   
   

   

   
   

