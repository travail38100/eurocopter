

.. index::
   pair: Paban; Applications


.. _applis_paban:

================================
Les 2 applications ``Paban``
================================

.. seealso::

   - :ref:`specs_application_paban`


.. toctree::
   :maxdepth: 3
   
   pc_encodagepaban/index
   django_db_paban/index
