

.. index::
   pair: PDA_Eurocopter; Application
   pair: PDA ; GUI
   pair: Eurocopter ; GUI


.. _gui_eurocopter:
.. _gui_pda_eurocopter:
.. _gui_pda_atex:
.. _PDA_Eurocopter:

===========================================
L'application graphique ``PDA_Eurocopter``
===========================================


.. contents::
   :depth: 3

Manuel utilisateur
==================

.. toctree::
   :maxdepth: 3
   
   manuel_utilisateur/index
   
   

Onglets
=======   

.. toctree::
   :maxdepth: 3
   
   onglet_pot/index
   onglet_tag_melange/index
   onglet_tags/index   
   onglet_creer_melange/index   
   
   
Spécifications des plans mémoire des puces RFID
===============================================

Plan mémoire de la puce RFID ``pot``
-------------------------------------

.. seealso::

   - :ref:`plan_memoire_pot` 
   
   
Plan mémoire de la puce RFID ``bac mélange``
---------------------------------------------

.. seealso::

   - :ref:`plan_memoire_bac_melange` 
   
   
   
   
