

.. index::
   pair: Onglet ; Pot
   pair: Ouverture ; Pot


.. _onglet_pot:

=================================
Onglet pot (ouverture d'un pot)
=================================

.. seealso::

   - :ref:`table_pot`
   - :ref:`ouverture_pot`

.. contents::
   :depth: 3

Interface graphique
===================


.. figure:: lecture_pot.png
   :align: center







