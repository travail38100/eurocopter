

.. index::
   pair: Onglet ; Tag Mélange


.. _onglet_tag_melange:

=======================
Onglet tag mélange
=======================

.. seealso::

   - :ref:`table_bac_melange`
   - :ref:`table_composant`


.. contents::
   :depth: 3

Interface graphique
===================

Ouverture d'un mélange.


.. figure:: tag_melange.png
   :align: center







