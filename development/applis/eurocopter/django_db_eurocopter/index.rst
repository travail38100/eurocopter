

.. index::
   pair: Base de données; PDA
   pair: Base de données; Eurocopter
   pair: Base de données; DB_Eurocopter.sqlite
   pair: Windows Mobile Center; DB_Eurocopter.sqlite
   pair: WORKON_HOME; Environment
   pair: WORKON_HOME; DB_Eurocopter.sqlite


.. _db_eurocopter:
.. _sqlite_eurocopter:
.. _sqlite_pda:
.. _appli_gestion_base_eurocopter:

========================================================================
L'application de gestion de la base de données ``DB_Eurocopter.sqlite`` 
========================================================================

.. seealso::

   - :term:`bases de données`
   - :ref:`gestion_base_eurocopter`
   

.. contents::
   :depth: 3


Description de la base
======================

La base de données contient les tables suivantes:

#. la :ref:`table de des pots <table_pot>` 
#. la :ref:`table des bac mélange <table_bac_melange>`
#. la :ref:`table des ouvertures de bac mélange <table_ouverture_bac>`

#. la :ref:`table des mélanges <table_melange>`
#. la :ref:`table des types de composants <table_type_composant>`
#. la :ref:`table des composants <table_composant>`
#. la :ref:`table des associations entre un mélange et ses composants <table_melange_composants>`
#. la :ref:`table des types de vacation <table_type_vacation>`  
#. la :ref:`table des vacations <table_vacation>`
#. la :ref:`table de la vacation courante <table_vacation_courante>`
#. la :ref:`table des opérateurs <table_operateur>` 




Emplacement de la base de données DB_Eurocopter.sqlite
=======================================================


.. toctree::
   :maxdepth: 3
   
   emplacement/index
   


.. _var_python_workon_home:

La variable d'environnement WORKON_HOME
===============================================

.. seealso::

   - :ref:`workon_home`


Installation et gestion de la base de données Eurocopter
========================================================

.. seealso::

   - :ref:`gestion_base_eurocopter`


Tables (modèles)
================


.. toctree::
   :maxdepth: 3

   tables/index


Evolution de la base de données
================================

.. toctree::
   :maxdepth: 3

   evolution/index


Manuel utilisateur pour la gestion de la base de données
========================================================

.. toctree::
   :maxdepth: 3

   manuel_utilisateur/index
   


Droits d'administration sur la base de données
===============================================

.. toctree::
   :maxdepth: 3

   droits_base_de_donnees/index
   
   
   




