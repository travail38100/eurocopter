

.. index::
   pair: Table; CVcationCourante
   pair: Vacation; Courante
   pair: Compteur; Mélange


.. _table_vacation_courante:

=========================
Table "CVacationCourante"
=========================

.. contents::
   :depth: 3


.. figure:: cvacation_courante.png
   :align: center



Introduction
=============


Cette table contient les dernières informations concernant la vacation courante.

Cette table ne contient donc qu'un seul enregistrement.

Sa principale utilité est de récupérer le compteur mélange de la vacation
courante.

Liste des champs
=================


CVacationCourante.id
--------------------

- Identifiant vacation courante.
- est égal à 1


CVacationCourante.type_vacation_id (clé étrangère)
--------------------------------------------------

.. seealso::

   - :ref:`table_type_vacation`


La clé étrangère ``CVacationCourante.type_vacation_id`` référence la table :ref:`table_type_vacation`.


.. _cvacation_courante_compteur_melange:

CVacationCourante.compteur_melange
-----------------------------------

.. seealso::

   - :ref:`numero_melange`


Le compteur mélange est incrémenté chaque fois qu'un mélange est créé au cours
d'une vacation.

Il est remis à 0 en début de chaque nouvelle vacation.



CVacationCourante.date_dernier_melange
---------------------------------------

Date du dernier mélange.


Référence la table CTypeVacation
================================

La clé étrangère ``CVacationCourante.type_vacation_id`` référence la table :ref:`table_type_vacation`.





