

.. index::
   pair: Table; CVacation


.. _table_vacation:

===========================
Table "CVacation"
===========================

.. contents::
   :depth: 3


Introduction
=============


Cette table contient les vacations.


Liste des champs
=================


Voir la liste des champs de la table :ref:`CVacationCourante <table_vacation_courante>`.





