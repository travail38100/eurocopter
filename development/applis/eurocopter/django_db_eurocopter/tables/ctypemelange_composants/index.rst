

.. index::
   pair: Table; CMelange_composants
   pair: Composants; Mélange


.. _table_melange_composants:
.. _melange_composants:

===========================
Table "CMelange_composants"
===========================

.. contents::
   :depth: 3



Introduction
=============


Cette table est une table associative qui contient pour chaque :ref:`mélange <melange>` sa liste
des :ref:`composants <composant>`.

Cette table a été produite automatiquement par :ref:`Django <django>` à partir de la
classe suivante::

    class Cmelange(models.Model):
        nom_melange = models.CharField(max_length=6, unique=True)
        temps_murissement = models.IntegerField()
        temps_peremption = models.IntegerField()
        composants = models.ManyToManyField(Ccomposant)
        class Meta:
            db_table = u'CMelange'
        def __unicode__(self):
            return self.nom_melange




Structure de la table
=====================

::

    CREATE TABLE "CMelange_composants" (
        "id" integer NOT NULL PRIMARY KEY,
        "cmelange_id" integer NOT NULL,
        "ccomposant_id" integer NOT NULL REFERENCES "CComposant" ("id"),
        UNIQUE ("cmelange_id", "ccomposant_id")
    )


Liste des champs
=================


CMelange_composants.cmelange_id
-------------------------------

L'identifiant du :ref:`mélange <melange>`.


CMelange_composants.ccomposant_id
----------------------------------

L'identifiant du :ref:`composant <composant>`.


