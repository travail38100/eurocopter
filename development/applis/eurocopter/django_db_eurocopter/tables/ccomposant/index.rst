

.. index::
   pair: Composant ; Mélange
   pair: Table; CComposant
   ! Composant


.. _table_composant:
.. _composant:

==========================================
Table "CComposant" (de mélanges)
==========================================


.. seealso::

   - :ref:`table_type_composant`
   - :ref:`table_melange`
   - :ref:`table_bac_melange`
   - :ref:`page_django_eurocopter_composant`


.. contents::
   :depth: 3


Introduction
=============


Cette table contient les informations décrivant un composant d'un :ref:`mélange <table_melange>`.


Description
================

::

    class Ccomposant(models.Model):
        u''' Le composant constituant un mélange
        On le distingue grâce au nom de produit (voir pot.nom_produit)
        Modification du jeudi 12 septembre 2013 (avant c'était le code ECS)
        '''
        id = models.IntegerField(primary_key=True)
        nom_produit = models.CharField(max_length=15, help_text=u"Le nom du produit (désignation)")
        id_type_composant = models.ForeignKey(Ctypecomposant, db_column=u'id_type_composant', help_text=u"Le type de composant")
        description = models.CharField(max_length=100, help_text=u"La description du composant")
        class Meta:
            db_table = u'CComposant'
            verbose_name = u'Composant'
            verbose_name_plural = u'Composants'
        def __unicode__(self):
            u'''Le nom du composant (=> nom du produit)'''
            return self.nom_produit



