
.. index::
   pair: CPot; Octobre 2013



.. _cpot_octobre_2013:

=========================================================
Description de la table "CPot"  au 1er octobre 2013
=========================================================


.. seealso::

   - :ref:`cpot_avant_octobre_2013`
   

.. contents::
   :depth: 3




Définition "Django" de la table
===============================


.. literalinclude:: definition_django.txt
   :language: python
   :linenos:
        

Modifications Django par rapport à la version antérieure
--------------------------------------------------------

Passage de:

- DecimalField                                  -> IntegerField
- DateTimeField                                 -> DateField
- suppression du champ 'livraison_traitee'  
- date_ouverture_pot = models.DateTimeField(null=True, blank=True) ->  models.CharField(max_length=10, blank=True)  
   
Différences Django
------------------

::

    django_db_eurocopter\tables\cpot\evolution>diff octobre_2013\definition_django.txt avant_octobre_2013\definition_django.txt
    

::
    
    --- octobre_2013\definition_django.txt  Wed Oct  2 13:38:45 2013
    +++ avant_octobre_2013\definition_django.txt    Wed Oct  2 13:40:19 2013
    @@ -1,4 +1,4 @@
    -class Cpot(models.Model):
    +class CPot(models.Model):
         id = models.IntegerField(primary_key=True)
         nom_produit = models.CharField(max_length=15)
         reference_produit = models.CharField(max_length=20)
         fournisseur = models.CharField(max_length=15)
         numero_lot = models.CharField(max_length=30)
         teinte = models.CharField(max_length=15)
    -    quantite_produit = models.IntegerField()
    +    quantite_produit = models.DecimalField(max_digits=8, decimal_places=2)
         unite_quantite = models.CharField(max_length=2)
         code_ecs = models.CharField(max_length=6)
    -    date_fabrication = models.DateField()
    -    date_peremption = models.DateField()
    -    date_livraison = models.DateField()
    -    date_ouverture_pot = models.CharField(max_length=10, blank=True)
    +    date_fabrication = models.DateTimeField()
    +    date_peremption = models.DateTimeField()
    +    date_livraison = models.DateTimeField()
    +    date_ouverture_pot = models.DateTimeField(null=True, blank=True)
         nb_jours_peremption_pot_ouvert = models.IntegerField()
    -    temperature_stockage_min = models.IntegerField()
    -    temperature_stockage_max = models.IntegerField()
    +    temperature_stockage_min = models.DecimalField(max_digits=8, decimal_places=2)
    +    temperature_stockage_max = models.DecimalField(max_digits=8, decimal_places=2)
         lmp = models.IntegerField()
    +    livraison_traitee = models.IntegerField()
         class Meta:
             db_table = u'CPot'
             verbose_name = _(u'Pots')
         def __unicode__(self):
             return self.nom_produit + self.code_ecs


Définition sqlite
==================


.. literalinclude:: definition_sql.txt
   :linenos:
        

Modifications SQL par rapport à la version antérieure
-----------------------------------------------------

Passage de:

- DATETIME                                      -> DATE
- "date_ouverture_pot" DATETIME                 -> [date_ouverture_pot] VARCHAR(10)  NULL
- [temperature_stockage_min] INTEGER  NOT NULL  -> [temperature_stockage_min] INTEGER  NOT NULL,
- "temperature_stockage_max" decimal NOT NULL,  -> [temperature_stockage_max] INTEGER  NOT NULL,


Différences SQL
---------------


::

    tables\cpot\evolution>diff octobre_2013\definition_sql.txt avant_octobre_2013\definition_sql.txt
    

::
    
    --- octobre_2013\definition_sql.txt     Wed Oct  2 13:21:55 2013
    +++ avant_octobre_2013\definition_sql.txt       Wed Oct  2 13:23:21 2013
    @@ -1,21 +1,21 @@
    -CREATE TABLE [CPot] (
    -[id] integer  PRIMARY KEY NOT NULL,
    -[nom_produit] varchar(15)  NOT NULL,
    -[reference_produit] varchar(20)  NOT NULL,
    -[fournisseur] varchar(15)  NOT NULL,
    -[numero_lot] varchar(30)  NOT NULL,
    -[teinte] varchar(15)  NOT NULL,
    -[quantite_produit] INTEGER  NOT NULL,
    -[unite_quantite] varchar(2)  NOT NULL,
    -[code_ecs] varchar(6)  NOT NULL,
    -[date_fabrication] DATE  NOT NULL,
    -[date_peremption] DATE  NOT NULL,
    -[date_livraison] DATE  NOT NULL,
    -[date_ouverture_pot] VARCHAR(10)  NULL,
    -[nb_jours_peremption_pot_ouvert] integer  NOT NULL,
    -[temperature_stockage_min] INTEGER  NOT NULL,
    -[temperature_stockage_max] INTEGER  NOT NULL,
    -[lmp] integer  NOT NULL
    +CREATE TABLE "CPot" (
    +    "id" integer NOT NULL PRIMARY KEY,
    +    "nom_produit" varchar(15) NOT NULL,
    +    "reference_produit" varchar(20) NOT NULL,
    +    "fournisseur" varchar(15) NOT NULL,
    +    "numero_lot" varchar(30) NOT NULL,
    +    "teinte" varchar(15) NOT NULL,
    +    "quantite_produit" decimal NOT NULL,
    +    "unite_quantite" varchar(2) NOT NULL,
    +    "code_ecs" varchar(6) NOT NULL,
    +    "date_fabrication" datetime NOT NULL,
    +    "date_peremption" datetime NOT NULL,
    +    "date_livraison" datetime NOT NULL,
    +    "date_ouverture_pot" datetime,
    +    "nb_jours_peremption_pot_ouvert" integer NOT NULL,
    +    "temperature_stockage_min" decimal NOT NULL,
    +    "temperature_stockage_max" decimal NOT NULL,
    +    "lmp" integer NOT NULL
     )            
            
            
