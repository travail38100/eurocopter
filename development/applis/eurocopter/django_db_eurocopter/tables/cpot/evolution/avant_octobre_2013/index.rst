
.. index::
   pair: CPot; Avant octobre 2013


.. _cpot_avant_octobre_2013:

=========================================================
Description de la table "CPot" avant octobre 2013
=========================================================


.. seealso::

   - :ref:`cpot_octobre_2013`



.. contents::
   :depth: 3


Définition "Django" de la table
===============================


.. literalinclude:: definition_django.txt
   :language: python
   :linenos:
           
            
            
Définition sqlite
==================

.. literalinclude:: definition_sql.txt
   :linenos:
              
            
            
