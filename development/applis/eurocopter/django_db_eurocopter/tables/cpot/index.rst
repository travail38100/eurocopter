

.. index::
   pair: Table; CPot

.. _table_pot:

=========================
Table "CPot"
=========================


.. seealso::

   - :ref:`page_django_eurocopter_pot`

.. contents::
   :depth: 3


Introduction
=============


Cette table contient les informations concernant un pot de peinture.


::

    class CPot(models.Model):
        id = models.IntegerField(primary_key=True)
        nom_produit = models.CharField(max_length=15)
        reference_produit = models.CharField(max_length=20)
        fournisseur = models.CharField(max_length=15)
        numero_lot = models.CharField(max_length=30)
        teinte = models.CharField(max_length=15)
        quantite_produit = models.IntegerField()
        unite_quantite = models.CharField(max_length=2)
        code_ecs = models.CharField(max_length=6)
        date_fabrication = models.DateTimeField()
        date_peremption = models.DateTimeField()
        date_livraison = models.DateTimeField()
        date_ouverture_pot = models.CharField(max_length=10, blank=True)
        nb_jours_peremption_pot_ouvert = models.IntegerField()
        temperature_stockage_min = models.IntegerField()
        temperature_stockage_max = models.IntegerField()
        lmp = models.IntegerField()
        class Meta:
            db_table = u'CPot'
            verbose_name = _(u'Historique des Pots')
            verbose_name_plural = _(u'Historique des Pots')
        def __unicode__(self):
            return self.nom_produit + self.code_ecs
            
        def date_fab2(self):
            '''
            Retourne la date de fabrication sous la forme mois/annee
            >>> from datetime import date        
            >>> today=date.today()
            >>> today
            datetime.date(2013, 10, 9)
            >>> today.strftime("%m/%Y")
            '10/2013'
            '''
            date_mois_annee = self.date_fabrication.strftime("%m/%Y")       
            return date_mois_annee
        date_fab2.short_description = _(u"Date fabrication")
        date_fab2.admin_order_field = 'date_fabrication'        

        def date_liv2(self):
            '''
            Retourne la date de livraison sous la forme jour/mois/annee
            >>> from datetime import date        
            >>> today=date.today()
            >>> today
            datetime.date(2013, 10, 9)
            >>> today.strftime("%d/%m/%Y")
            '09/10/2013'
            '''
            try:        
                date_liv2 = self.date_livraison.strftime("%d/%m/%Y")       
            except:
                date_liv2 = "inconnue"
                
            return date_liv2
        date_liv2.short_description = _(u"Date de livraison")
        date_liv2.admin_order_field = 'date_livraison'     

        def date_peremp2(self):
            '''
            Retourne la date de péremption sous la forme mois/annee
            '''
            try:        
                date_peremp2 = self.date_peremption.strftime("%m/%Y")       
            except:
                date_peremp2 = "inconnue"            
            return date_peremp2
            
        date_peremp2.short_description = _(u"Date de péremption")
        date_peremp2.admin_order_field = 'date_peremption' 



Evolution
================


.. toctree::
   :maxdepth: 3
   
   evolution/index
   
            
            
        
            
            
            
