

.. index::
   pair: Table; CTypeVacation
   pair: Table; CTypeVacation
   pair: Code; Vacation


.. _table_cvacation:
.. _table_type_vacation:

======================
Table "CTypeVacation"
======================

.. seealso::

   - :ref:`code_vacations`

.. contents::
   :depth: 3


.. figure:: ctype_vacation.png
   :align: center



Introduction
=============


Cette table contient les informations concernant :ref:`les types de vacation <code_vacations>`.



Liste des champs
================


CTypeVacation.id
----------------


CTypeVacation.code_vacation
----------------------------




CTypeVacation.libelle
----------------------



Référencée par la table CVacationCourante
=========================================

.. seealso::

   - :ref:`table_vacation_courante`


Cette table est référencée par la table :ref:`vacation courante <table_vacation_courante>`.







