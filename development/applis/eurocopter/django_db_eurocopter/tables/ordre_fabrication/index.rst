

.. index::
   pair: Table; COrdreFabrication
   ! COrdreFabrication
   

.. _table_ouverture_bac:
.. _table_ordre_fabrication:


=============================
Table "COrdreFabrication"
=============================


.. seealso::

   - :ref:`specs_bdd_pda`
   - :ref:`table_bac_melange`

.. contents::
   :depth: 3


Introduction
=============


Cette table contient les informations concernant un ordre de fabrication
concernant un :ref:`mélange <table_melange>`.


.. contents::
   :depth: 3


Définition "Django" de la table
===============================

.. seealso::

   - :ref:`ordres_de_fabrication`

.. literalinclude:: definition_django.txt
   :language: python
   :linenos:
           
   
Ancienne définition COuvertureBac
----------------------------------
   
::

    from ..operateurs.models import Coperateur
    class COuvertureBac(models.Model):
        u''' Une ouverture de bac mélange.

        Une ouverture de bac est associée à un bac mélange.
        '''
        id = models.IntegerField(primary_key=True)
        bac_melange = models.ForeignKey(CBacMelange)
        ordre_fabrication = models.CharField(max_length=50)
        date_ouverture = models.DateTimeField()
        operateur = models.ForeignKey(Coperateur)
        numero_ouverture = models.IntegerField()
        class Meta:
            db_table = u'COuvertureBac'
            verbose_name = u'Ouverture bac mélange'
            verbose_name_plural = u'Ouvertures bac mélange'
        def __unicode__(self):
            return self.numero_ouverture   
                
                
Définition sqlite
==================

.. literalinclude:: definition_sql.txt
   :linenos:
              
 
Ancienne table
--------------
 
::

    CREATE TABLE "COuvertureBac" (
        "id" integer NOT NULL PRIMARY KEY,
        "bac_melange_id" integer NOT NULL REFERENCES "CBacMelange" ("id"),
        "ordre_fabrication" varchar(50) NOT NULL,
        "date_ouverture" datetime NOT NULL,
        "operateur_id" integer NOT NULL,
        "numero_ouverture" integer NOT NULL
    )         
