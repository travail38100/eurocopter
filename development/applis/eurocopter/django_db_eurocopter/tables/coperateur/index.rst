

.. index::
   pair: Table; COperateur
   pair: Matricule; Opérateur


.. _table_operateur:

======================
Table "COperateur"
======================


.. contents::
   :depth: 3


Introduction
=============


Cette table contient les informations concernant un opérateur.



Liste des champs
================


COperateur.nom
----------------


COperateur.prenom
-----------------


COperateur.matricule
---------------------

