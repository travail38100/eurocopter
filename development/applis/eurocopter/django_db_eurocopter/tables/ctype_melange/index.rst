

.. index::
   pair: Table; CMelange
   pair: Mélange ; Autorisé
   pair: Champ ; Autorisé

.. _table_melange:
.. _melange:

=========================
Table "CMelange"
=========================


.. seealso::

   - :ref:`table_composant`
   - :ref:`mem_table_melange_autorise`
   - :ref:`table_bac_melange`
   - :ref:`page_django_eurocopter_melange`


.. contents::
   :depth: 3



Introduction
=============


Cette table contient les informations décrivant :ref:`les mélanges <specs_bdd_pda>`.



.. _champ_compteur_bac_melanges:

Evolution de la table CMelange du mardi 25 février 2014
========================================================

.. seealso::

   - :ref:`specs_26_fevrier_2014`


Rajout du champ ``compteur_bac_melanges`` pour prendre en compte les :ref:`nouvelles
spécifications <specs_26_fevrier_2014>`.


Description
===========

::

    class Cmelange(models.Model):
        u''' Un mélange de composants.
        '''
        nom_melange = models.CharField(max_length=7, unique=True)
        temps_de_murissement = models.IntegerField(help_text=u"Le temps nécessaire (en minutes) au mûrisssement du mélange <= 999 minutes")
        duree_de_vie = models.IntegerField(help_text=u"La durée de vie (en minutes) du mélange <= 999 minutes")
        composants = models.ManyToManyField(Ccomposant, help_text=u"Un mélange est constitué de n composants")
        compteur_bac_melanges = models.IntegerField(default=0, help_text=u"Le compteur de bacs mélange créé avec ce type de mélange")
        class Meta:
            db_table = u'CMelange'
            verbose_name = u'Types de mélanges'
            verbose_name_plural = u'Types de mélanges'
            
        def __unicode__(self):
            return self.nom_melange
            
        def clean(self):
            _(u''' Validation des données
            https://docs.djangoproject.com/en/dev/ref/models/instances/#validating-objects

            On vérifie la cohérence des données

            - le temps de murissement doit être positif
            - La durée de vie doit être positive
            ''')
            from django.core.exceptions import ValidationError
            if (self.temps_de_murissement < 0):
                raise ValidationError(_(u'Le temps de murissement doit être positif'))
            if (self.temps_de_murissement > 999):
                raise ValidationError(_(u'Le temps de murissement doit être <= 999'))
            if (self.duree_de_vie < 0):
                raise ValidationError(_(u'La durée de vie doit être positive'))
            if (self.duree_de_vie > 999):
                raise ValidationError(_(u'La durée de vie doit être <= 999'))
            if (self.temps_de_murissement > self.duree_de_vie):
                raise ValidationError(_(u'La durée de vie du mélange doit être supérieure au temps de murissement'))

            super(Cmelange, self).clean()



