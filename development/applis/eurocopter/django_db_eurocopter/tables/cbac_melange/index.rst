

.. index::
   pair: Table; CHistoriqueTagMelange
   pair: Table; Melange


.. _table_historique_tag_melange:
.. _table_bac_melange:

=============================
Table "CBacMelange"
=============================


.. seealso::

   - :ref:`page_django_eurocopter_bac_melange`
   - :ref:`table_melange`
   - :ref:`table_composant`
   - :ref:`table_ouverture_bac`
   

.. contents::
   :depth: 3


Introduction
=============


Cette table contient les informations écrites dans un :ref:`tag "mélange" <tag_melange>`.


::

    class CBacMelange(models.Model):
        u'''
        Contient toutes les informations écrites dans un tag RFID de type 'mélange'
        '''
        id = models.IntegerField(primary_key=True, help_text = _(u"L'identifiant unique de l'enregistrement"))
        date_fabrication = models.DateTimeField(help_text = _(u'Date de fabrication du mélange'))
        minutes_date_fabrication = models.IntegerField()
        temps_de_murissement = models.IntegerField(help_text=_(u"Le temps nécessaire (en minutes) au mûrisssement du mélange <= 999 minutes"))
        duree_de_vie = models.IntegerField(help_text=u"La durée de vie (en minutes) du mélange <= 999 minutes")
        code_vacation = models.IntegerField(help_text=u"1=matin 2=après-midi 3=nuit")
        numero_melange_vacation = models.IntegerField(help_text = _(u'Le numéro du mélange pendant la vacation'))
        nom_melange = models.CharField(help_text =_(u'Le nom du mélange') ,   max_length=6)
        viscosite = models.IntegerField(help_text=u"La viscosité (en secondes) du mélange <= 99 secondes")
        temperature = models.IntegerField(help_text=u"La température en °C du mélange <= 99,9 secondes")
        matricule_operateur = models.CharField(max_length=10, help_text=u"Le matricule operateur")
        lmp = models.IntegerField(help_text = _(u'Indique si la durée de vie du mélange est prolongée par le LMP (Laboratoire Matériaux et Procédés)'))
        traite = models.IntegerField()
        class Meta:
            db_table = u'CBacMelange'
            verbose_name = u'Bac mélange'
            verbose_name_plural = u'Bacs mélange'
        def __unicode__(self):
            return self.nom_melange
        def minutes_fabrication(self):
            '''
            On a besoin des minutes par rapport à la date locale et non la date UTC
            '''
            current_tz = timezone.get_current_timezone()
            date_locale = current_tz.normalize(self.date_fabrication.astimezone(current_tz))
            minutes = date_locale.hour * 60 + date_locale.minute
            return minutes
        minutes_fabrication.short_description = _(u'Minutes fabrication')

        def date_fabrication_locale(self):
            '''
            Retourne la date locale et non la date UTC
            '''
            current_tz = timezone.get_current_timezone()
            date_locale = current_tz.normalize(self.date_fabrication.astimezone(current_tz))
            return date_locale
        date_fabrication_locale.short_description = _(u"Date fabrication locale")
        date_fabrication_locale.admin_order_field = 'date_fabrication'

        def clean(self):
            _(u''' Validation des données
            https://docs.djangoproject.com/en/dev/ref/models/instances/#validating-objects

            On vérifie la cohérence des données

            - le temps de murissement doit être positif
            - La durée de vie doit être positive
            ''')
            from django.core.exceptions import ValidationError
            if (self.viscosite < 0):
                raise ValidationError(_(u'Le temps viscosité doit être positif'))
            if (self.viscosite > 99):
                raise ValidationError(_(u'Le temps de viscosité doit être <= 99'))
            if (self.temperature < 0):
                raise ValidationError(_(u'La température du mélange doit être positive '))
            if (self.temperature > 99.9):
                raise ValidationError(_(u'La température du mélange doit <= 99.9 '))
            self.minutes_date_fabrication =  self.minutes_fabrication()

            super(CBacMelange, self).clean()



Evolution
================


.. toctree::
   :maxdepth: 3
   
   evolution/index
