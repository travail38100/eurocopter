
.. index::
   pair: CBacMelange; Avant octobre 2013


.. _cbac_melange_avant_octobre_2013:

=========================================================
Description de la table "CBacMelange" avant octobre 2013
=========================================================


.. seealso::

   - :ref:`cbac_melange_octobre_2013`



.. contents::
   :depth: 3


Définition "Django" de la table
===============================


.. literalinclude:: definition_django.txt
   :language: python
   :linenos:
           
            
            
Définition sqlite
==================

.. literalinclude:: definition_sql.txt
   :linenos:
              
            
            
