
.. index::
   pair: CBacMelange; Octobre 2013



.. _cbac_melange_octobre_2013:

============================================================
Description de la table "CBacMelange"  au 1er octobre 2013
============================================================


.. seealso::

   - :ref:`cbac_melange_avant_octobre_2013`
   

.. contents::
   :depth: 3




Définition "Django" de la table
===============================


.. literalinclude:: definition_django.txt
   :language: python
   :linenos:
        

Modifications Django par rapport à la version antérieure
--------------------------------------------------------

Passage de:

- DecimalField                                  -> IntegerField


Différences Django
------------------

::

    django_db_eurocopter\tables\cbac_melange\evolution>diff octobre_2013\definition_django.txt avant_octobre_2013\definition_django.txt
    
    --- octobre_2013\definition_django.txt  Wed Oct  2 14:11:37 2013
    +++ avant_octobre_2013\definition_django.txt    Wed Oct  2 14:14:47 2013
    @@ -11,7 +11,7 @@
         numero_melange_vacation = models.IntegerField(help_text = _(u'Le numéro du mélange pendant la vacation'))
         nom_melange = models.CharField(help_text =_(u'Le nom du mélange') ,   max_length=6)
         viscosite = models.IntegerField(help_text=u"La viscosité (en secondes) du mélange <= 99 secondes")
    -    temperature = models.IntegerField(help_text=u"La température en °C du mélange <= 99,9 secondes")
    +    temperature = models.DecimalField(max_digits=8, decimal_places=2, help_text=u"La température en °C du mélange <= 99,9 secondes")
         matricule_operateur = models.CharField(max_length=10, help_text=u"Le matricule operateur")
         lmp = models.IntegerField(help_text =  _(u'Indique si la durée de vie du mélange est prolongée par le LMP (Laboratoire Matériaux et Procédés)'))
         traite = models.IntegerField()

Définition sqlite
==================


.. literalinclude:: definition_sql.txt
   :linenos:
        

Modifications SQL par rapport à la version antérieure
-----------------------------------------------------

Passage de:

- "temperature" decimal NOT NULL,,  -> [temperature] INTEGER  NOT NULL,


Différences SQL
---------------


::
    
   django_db_eurocopter\tables\cbac_melange\evolution>diff octobre_2013\definition_sql.txt avant_octobre_2013\definition_sql.txt
   
   
::
   
    --- octobre_2013\definition_sql.txt     Wed Oct  2 14:12:17 2013
    +++ avant_octobre_2013\definition_sql.txt       Wed Oct  2 14:12:56 2013
    @@ -1,17 +1,17 @@
    -CREATE TABLE [CBacMelange] (
    -[id] integer  PRIMARY KEY NOT NULL,
    -[date_fabrication] datetime  NOT NULL,
    -[minutes_date_fabrication] integer  NOT NULL,
    -[temps_de_murissement] integer  NOT NULL,
    -[duree_de_vie] integer  NOT NULL,
    -[code_vacation] integer  NOT NULL,
    -[numero_melange_vacation] integer  NOT NULL,
    -[nom_melange] varchar(6)  NOT NULL,
    -[viscosite] integer  NOT NULL,
    -[temperature] INTEGER  NOT NULL,
    -[matricule_operateur] varchar(10)  NOT NULL,
    -[lmp] integer  NOT NULL,
    -[traite] integer  NOT NULL
    +CREATE TABLE "CBacMelange" (
    +    "id" integer NOT NULL PRIMARY KEY,
    +    "date_fabrication" datetime NOT NULL,
    +    "minutes_date_fabrication" integer NOT NULL,
    +    "temps_de_murissement" integer NOT NULL,
    +    "duree_de_vie" integer NOT NULL,
    +    "code_vacation" integer NOT NULL,
    +    "numero_melange_vacation" integer NOT NULL,
    +    "nom_melange" varchar(6) NOT NULL,
    +    "viscosite" integer NOT NULL,
    +    "temperature" decimal NOT NULL,
    +    "matricule_operateur" varchar(10) NOT NULL,
    +    "lmp" integer NOT NULL,
    +    "traite" integer NOT NULL
     )         
            
