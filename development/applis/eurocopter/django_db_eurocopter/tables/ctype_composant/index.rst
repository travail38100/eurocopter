

.. index::
   pair: Type ; Composant
   pair: Table ; CTypeComposant


.. _table_type_composant:
.. _type_composant:

==========================================
Table "CTypeComposant"
==========================================


.. seealso::

   - :ref:`table_composant`

.. contents::
   :depth: 3


.. figure:: ctype_composant.png
   :align: center


Introduction
=============


Cette table contient les informations décrivant les types de composant
entrant dans un mélange.


On distingue 3 types de composant:

- base
- durcisseur
- diluant


Liste des champs
================

CTypeComposant.id
------------------


CTypeComposant.description
---------------------------



Table Réferencé par
===================

Cette table est référencée par:

- la :ref:`la table des composants mélange <table_composant>`











