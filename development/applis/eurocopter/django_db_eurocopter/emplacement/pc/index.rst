

.. index::
   pair: PATH_DB_EUROCOPTER; DB_Eurocopter.sqlite
   pair: PATH_DB_EUROCOPTER; Environment


.. _emplacement_db_eurocopter_pc:

=================================================================
Emplacement de la base de données DB_Eurocopter.sqlite sur le PC
=================================================================

.. seealso::

   - :ref:`intro_synchronisation`
   - :ref:`synchro_bases_pc_pda`


.. contents::
   :depth: 3

Introduction
============

La base de données sur PC est situé sous le répertoire :file:`$PATH_DB_EUROCOPTER$/Eurocopter`
et elle se nomme ``DB_Eurocopter.sqlite``.


La :ref:`synchronisation des bases <synchro_bases_pc_pda>` se fait au moyen 
d'un :ref:`logiciel de synchronisation <installation_synchro_eurocopter>` (:ref:`ActiveSync <install_active_sync>` 
ou :ref:`Windows Mobile Center <install_windows_mobile_center>`).


.. _var_path_db_eurocopter:

La variable d'environnement PATH_DB_EUROCOPTER sur Windows PC
==============================================================


La variable d'environnment ``PATH_DB_EUROCOPTER`` permet de préciser 
le répertoire sous lequel se trouve le projet ``Eurocopter`` contenant la
base de données. 


.. figure:: path_db_eurocopter_1.png
   :align: center


.. figure:: path_db_eurocopter_2.png
   :align: center



