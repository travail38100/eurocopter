

.. index::
   pair: Bartec; DB_Eurocopter.sqlite
   pair: PDA (Bartec); DB_Eurocopter.sqlite
   pair: Emplacement; DB_Eurocopter.sqlite (Bartec)
   


.. _emplacement_db_eurocopter_bartec:

=========================================================================
Emplacement de la base de données DB_Eurocopter.sqlite sur le PDA Bartec
=========================================================================

.. seealso::

   - :ref:`intro_synchronisation`
   - :ref:`synchro_bases_pc_pda`


Sur le PDA Bartec, la base de données ``DB_Eurocopter.sqlite`` se trouve sous
le répertoire ``Ordinateur\PDA_Bartec\\\My Documents\Eurocopter``.


.. figure:: emplacement_bartec.png
   :align: center
   
   
