

.. index::
   pair: Axem; DB_Eurocopter.sqlite
   pair: PDA (Axem); DB_Eurocopter.sqlite
   pair: Emplacement; DB_Eurocopter.sqlite (Axem)


.. _emplacement_db_eurocopter_axem:

=======================================================================
Emplacement de la base de données DB_Eurocopter.sqlite sur le PDA Axem
=======================================================================

.. seealso::

   - :ref:`intro_synchronisation`
   - :ref:`synchro_bases_pc_pda`


.. contents::
   :depth: 3

Introduction
============

La base de données sur le PDA Axem est situé sous le répertoire :file:`/My Documnts/Eurocopter`



.. figure:: mydocuments_eurocopter.png
   :align: center



Programme C#
=============

::

    /// <summary>
    /// Sur le PDA Axem, on place la base DB_Eurocopter.sqlite sous le répertoire
    /// "\My Documents\Eurocopter"
    /// Voir http://msdn.microsoft.com/en-us/library/system.environment.specialfolder.aspx
    /// </summary>
    static string nom_base_de_donnees;
    public static string NomBaseDeDonnes
    {
        get
        {
            // Avec Windows Mobile Center, la synchronisation entre le PDA et le PC se fait sur le répertoire \My Documents\
            // qui correspond en C# " à Environment.GetFolderPath(Environment.SpecialFolder.Personal)
            // ATTENTION il existe bien une répertoire Personal sous \My Documents\ mais en C# il est accessible en faisant
            // Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Personal") !!!
            // Du côté PC le fichier se retrouve dans 
            string repertoire_my_documents = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            try
            {
                string message = string.Format("repertoire_my_documents: {0}", repertoire_my_documents);
                // MessageBox.Show(message);

                // On crée un répertoire Eurocopter sous \My Documents
                //==============================================================
                string dirMyDocumentsEurocopter = Path.Combine(repertoire_my_documents, "Eurocopter");
                if (!Directory.Exists(dirMyDocumentsEurocopter))
                {
                    Directory.CreateDirectory(dirMyDocumentsEurocopter);
                }

                nom_base_de_donnees = Path.Combine(dirMyDocumentsEurocopter, "DB_Eurocopter.sqlite");

                message = string.Format("La base a bien été trouvé: {0}", nom_base_de_donnees);
                // MessageBox.Show(message);
            }
            catch (Exception ex)
            {
                string message = string.Format("pb in {0}", ex.Message);
                MessageBox.Show("Problème d'initialisation de la base de données :" + message, "Erreur");
                Log.Write("Problème avec la base de données :" + message + Environment.NewLine + ex.StackTrace, false);
            }

            return nom_base_de_donnees;
        }      
    }
