#!/usr/bin/python
# -*- coding: UTF-8 -*-



from __future__ import unicode_literals

from django.db import models

"""
Le livre :ref:`Django 2 scoops <two_scoops>` Model inheritance in practice the TimedstampModel, p74, 75.
"""


from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _


class AutoCreatedField(models.DateTimeField):
    """
    A DateTimeField that automatically populates itself at
    object creation.
    By default, sets editable=False, default=now
    """
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('editable', False)
        kwargs.setdefault('default', now)
        super(AutoCreatedField, self).__init__(*args, **kwargs)



class AutoLastModifiedField(AutoCreatedField):
    """
    A DateTimeField that updates itself on each save() of
    the model.
    By default, sets editable=False and default=now.
    """
    def pre_save(self, model_instance, add):
        value = now()
        setattr(model_instance, self.attname, value)
        return value


class TimeStampedModel(models.Model):
    """
    An abstract base class model that provides selfupdating
    ``created`` and ``modified`` fields.
    """
    created = AutoCreatedField(_('created'))
    modified = AutoLastModifiedField(_('modified'))

    class Meta:
        abstract = True





