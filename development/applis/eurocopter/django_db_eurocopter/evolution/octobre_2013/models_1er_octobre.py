# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.db import models

class Cbacmelange(models.Model):
    id = models.IntegerField(primary_key=True)
    date_fabrication = models.DateTimeField()
    minutes_date_fabrication = models.IntegerField()
    temps_de_murissement = models.IntegerField()
    duree_de_vie = models.IntegerField()
    code_vacation = models.IntegerField()
    numero_melange_vacation = models.IntegerField()
    nom_melange = models.CharField(max_length=6)
    viscosite = models.IntegerField()
    temperature = models.IntegerField()
    matricule_operateur = models.CharField(max_length=10)
    lmp = models.IntegerField()
    traite = models.IntegerField()
    class Meta:
        db_table = 'CBacMelange'

class Ccomposant(models.Model):
    id = models.IntegerField(primary_key=True)
    nom_produit = models.CharField(max_length=15)
    id_type_composant = models.ForeignKey('Ctypecomposant', db_column='id_type_composant')
    description = models.CharField(max_length=100)
    class Meta:
        db_table = 'CComposant'

class Chistoriquevacation(models.Model):
    id = models.IntegerField(primary_key=True)
    type_vacation_id = models.IntegerField()
    compteur_melange = models.IntegerField()
    date_dernier_melange = models.DateTimeField(null=True, blank=True)
    compteur_ouverture = models.IntegerField()
    date_derniere_ouverture = models.DateTimeField(null=True, blank=True)
    date_login = models.DateTimeField()
    class Meta:
        db_table = 'CHistoriqueVacation'

class Cmelange(models.Model):
    id = models.IntegerField(primary_key=True)
    nom_melange = models.CharField(max_length=6, unique=True)
    temps_de_murissement = models.IntegerField()
    duree_de_vie = models.IntegerField()
    class Meta:
        db_table = 'CMelange'

class CmelangeComposants(models.Model):
    id = models.IntegerField(primary_key=True)
    cmelange_id = models.IntegerField()
    ccomposant = models.ForeignKey(Ccomposant)
    class Meta:
        db_table = 'CMelange_composants'

class Coperateur(models.Model):
    id = models.IntegerField(primary_key=True)
    nom = models.CharField(max_length=60)
    prenom = models.CharField(max_length=60)
    matricule = models.CharField(max_length=10, unique=True)
    class Meta:
        db_table = 'COperateur'

class Couverturebac(models.Model):
    id = models.IntegerField(primary_key=True)
    bac_melange_id = models.IntegerField()
    ordre_fabrication = models.CharField(max_length=50)
    date_ouverture = models.DateTimeField()
    date_fermeture = models.DateTimeField()
    operateur_id = models.IntegerField()
    numero_ouverture = models.IntegerField()
    class Meta:
        db_table = 'COuvertureBac'

class Cpot(models.Model):
    id = models.IntegerField(primary_key=True)
    nom_produit = models.CharField(max_length=15)
    reference_produit = models.CharField(max_length=20)
    fournisseur = models.CharField(max_length=15)
    numero_lot = models.CharField(max_length=30)
    teinte = models.CharField(max_length=15)
    quantite_produit = models.IntegerField()
    unite_quantite = models.CharField(max_length=2)
    code_ecs = models.CharField(max_length=6)
    date_fabrication = models.DateField()
    date_peremption = models.DateField()
    date_livraison = models.DateField()
    date_ouverture_pot = models.CharField(max_length=10, blank=True)
    nb_jours_peremption_pot_ouvert = models.IntegerField()
    temperature_stockage_min = models.IntegerField()
    temperature_stockage_max = models.IntegerField()
    lmp = models.IntegerField()
    class Meta:
        db_table = 'CPot'

class Ctypecomposant(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.CharField(max_length=200)
    class Meta:
        db_table = 'CTypeComposant'

class Ctypevacation(models.Model):
    id = models.IntegerField(primary_key=True)
    libelle = models.CharField(max_length=100)
    code = models.IntegerField()
    class Meta:
        db_table = 'CTypeVacation'

class Cvacation(models.Model):
    id = models.IntegerField(primary_key=True)
    type_vacation = models.ForeignKey(Ctypevacation)
    compteur_melange = models.IntegerField()
    date_dernier_melange = models.DateTimeField(null=True, blank=True)
    compteur_ouverture = models.IntegerField()
    date_derniere_ouverture = models.DateTimeField(null=True, blank=True)
    date_login = models.DateTimeField()
    class Meta:
        db_table = 'CVacation'

class Cvacationcourante(models.Model):
    id = models.IntegerField(primary_key=True)
    type_vacation = models.ForeignKey(Ctypevacation)
    compteur_melange = models.IntegerField()
    date_dernier_melange = models.DateTimeField(null=True, blank=True)
    compteur_ouverture = models.IntegerField()
    date_derniere_ouverture = models.DateTimeField(null=True, blank=True)
    date_login = models.DateTimeField(null=True, blank=True)
    class Meta:
        db_table = 'CVacationCourante'

class AuthGroup(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=80, unique=True)
    class Meta:
        db_table = 'auth_group'

class AuthGroupPermissions(models.Model):
    id = models.IntegerField(primary_key=True)
    group_id = models.IntegerField()
    permission = models.ForeignKey('AuthPermission')
    class Meta:
        db_table = 'auth_group_permissions'

class AuthPermission(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    content_type_id = models.IntegerField()
    codename = models.CharField(max_length=100)
    class Meta:
        db_table = 'auth_permission'

class AuthUser(models.Model):
    id = models.IntegerField(primary_key=True)
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField()
    is_superuser = models.BooleanField()
    username = models.CharField(max_length=30, unique=True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=75)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()
    class Meta:
        db_table = 'auth_user'

class AuthUserGroups(models.Model):
    id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    group = models.ForeignKey(AuthGroup)
    class Meta:
        db_table = 'auth_user_groups'

class AuthUserUserPermissions(models.Model):
    id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    permission = models.ForeignKey(AuthPermission)
    class Meta:
        db_table = 'auth_user_user_permissions'

class DjangoAdminLog(models.Model):
    id = models.IntegerField(primary_key=True)
    action_time = models.DateTimeField()
    user = models.ForeignKey(AuthUser)
    content_type = models.ForeignKey('DjangoContentType', null=True, blank=True)
    object_id = models.TextField(blank=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    class Meta:
        db_table = 'django_admin_log'

class DjangoContentType(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    class Meta:
        db_table = 'django_content_type'

class DjangoSession(models.Model):
    session_key = models.CharField(max_length=40, unique=True)
    session_data = models.TextField()
    expire_date = models.DateTimeField()
    class Meta:
        db_table = 'django_session'

class DjangoSite(models.Model):
    id = models.IntegerField(primary_key=True)
    domain = models.CharField(max_length=100)
    name = models.CharField(max_length=50)
    class Meta:
        db_table = 'django_site'

class SouthMigrationhistory(models.Model):
    id = models.IntegerField(primary_key=True)
    app_name = models.CharField(max_length=255)
    migration = models.CharField(max_length=255)
    applied = models.DateTimeField()
    class Meta:
        db_table = 'south_migrationhistory'

