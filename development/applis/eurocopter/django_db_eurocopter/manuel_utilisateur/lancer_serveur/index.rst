
.. index::
   pair: Lancer; Serveur Base de données (Eurocopter)
   pair: Lancer; Gestionnaire Base de données (Eurocopter)



.. _lancer_base_eurocopter_django:

==============================================================
Lancer serveur et interface de gestion de la base de données
==============================================================


.. contents::
   :depth: 3


Lancer le serveur de gestion de la base de données DB_Eurocopter
================================================================

Le système de gestion de la base de données nécessite l’exécution d’un 
``serveur virtuel`` sur le poste client (PC sur lequel sera connecté le PDA).

Ce service peut être démarré à partir du menu :menuselection:`Démarrer -> BD_Eurocopter -> LancerServeurLocalEurocopter`, 
ou à l’aide du raccourci bureau créé lors de l’installation.
 

- cliquer sur l'icône permettant le lancement du serveur local de la base
  de données
  
.. figure:: icone_lancer_serveur_web_local.png
   :align: center

  
.. figure:: lancement_serveur_base_locale.png
   :align: center
     
  
Lancer l'interface de gestion de la base de données DB_Eurocopter
=================================================================


- cliquer sur l'icône permettant de gérer la base de données  

.. figure:: lancement_appli_base.png
   :align: center  
  

   
   






