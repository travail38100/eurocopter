
.. index::
   pair: Page; Pot


.. _page_django_eurocopter_pot:

==============================================================
Page de gestion des Pots de la base DB_Eurocopter
==============================================================

.. seealso::

   - :ref:`table_pot`
   

.. contents::
   :depth: 3

Affichage des pots
============================


.. figure:: les_pots.png
   :align: center
  

Export de la table bac mélange
===============================


Confirmation de la création
============================

.. figure:: confirm_creation.png
   :align: center
   
      
  


