
.. index::
   pair: Page; Composants


.. _page_django_eurocopter_composant:

==============================================================
Page de gestion des Composants de la base DB_Eurocopter
==============================================================

.. seealso::

   - :ref:`table_composant`
   

.. contents::
   :depth: 3

Affichage des composants
=========================

L’ajout de nouveaux composants se fait dans la :ref:`table des composants <table_composant>`.


.. figure:: ajout_composants.png
   :align: center
  

Cliquer sur le bouton ``Ajouter Composant``.    

Création d'un composant
=======================

.. figure:: composant_doc.png
   :align: center
        

Confirmation de la création
============================

.. figure:: ajout_ok.png
   :align: center
   
      
  


