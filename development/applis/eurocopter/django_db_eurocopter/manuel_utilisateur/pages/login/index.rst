

.. index::
   pair: Page; Login 
   pair: DB_Eurocopter; Login


.. _page_login_eurocopter:

============================
Page de login DB_Eurocopter
============================


La première page affichée est la page de login. 

Introduire les noms d’utilisateur et mot de passe (par défaut admin – admin ; 
ces informations peuvent être modifiées ultérieurement). 



.. figure:: page_login.png
   :align: center
   

  
   






