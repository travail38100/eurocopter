
.. index::
   pair: Page; Bacs mélange


.. _page_django_eurocopter_bac_melange:

==============================================================
Page de gestion des Bacs Mélange de la base DB_Eurocopter
==============================================================

.. seealso::

   - :ref:`table_bac_melange`
   

.. contents::
   :depth: 3

Affichage des bacs mélanges
============================


.. figure:: les_bacs_melange.png
   :align: center
  

Export de la table bac mélange
===============================

.. figure:: export_bacs_melange.png
   :align: center
        

Confirmation de la création
============================

.. figure:: confirmer_export_xls.png
   :align: center
   
      
  


