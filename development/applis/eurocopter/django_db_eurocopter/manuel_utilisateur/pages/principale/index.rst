
.. index::
   pair: Page; Login 
   pair: DB_Eurocopter; Login


.. _page_admin_eurocopter:

========================================================
Page administration des tables de la base DB_Eurocopter
========================================================
   

.. seealso::

   - :ref:`db_tables_eurocopter`   
   
Une fois loggé, la page d'administration des :ref:`tables <db_tables_eurocopter>` 
de la :ref:`base de données DB_Eurocopter <db_eurocopter>` s'affiche.   

   
.. figure:: page_admin_tables_eurocopter.png   
   :align: center
   
   
   






