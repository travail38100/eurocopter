
.. index::
   pair: Page; Mélanges


.. _page_django_eurocopter_melange:

==============================================================
Page de gestion des Mélanges de la base DB_Eurocopter
==============================================================

.. seealso::

   - :ref:`table_melange`
   

.. contents::
   :depth: 3

Affichage des mélanges
=========================

L’ajout de nouveaux composants se fait dans la :ref:`table des composants <table_composant>`.


.. figure:: les_melanges.png
   :align: center
  

Cliquer sur le bouton ``Ajouter Mélange``.    

Création d'un mélange
=======================

.. figure:: creer_melange.png
   :align: center
        

Confirmation de la création
============================

.. figure:: creation_ok.png
   :align: center
   
      
  


