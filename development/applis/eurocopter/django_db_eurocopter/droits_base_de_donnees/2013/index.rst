

.. index::
   pair: Base de données; Droits (2013)



.. _droits_admin_bdd_2013:

========================================================================
Définition de droits d'administration sur la base de données
========================================================================


.. contents::
   :depth: 3
   

Introduction
=============

Il est possible de créer des groupes d’utilisateurs qui ont des droits 
d’écriture/lecture/suppression sur les tables de l’application.

Il est possible de créer des utilisateurs appartenant à certains groupes

  
Propositions pour les groupes
==============================

.. toctree::
   :maxdepth: 3
   
   groupes/index


Propositions pour les utilisateurs
===================================

Dans tous les cas l’utilisateur « admin » a tous les droits.

En plus de l’utilisateur « admin » on peut créer les utilisateurs appartenant 
à l’un des groupes situés ci-dessous.


.. figure:: les_utilisateurs.png
   :align: center
      

     
Document PDF
==============

:download:`Télécharger le document au format PDF <GestionUtilisateursBaseDeDonnees_XSPC3Y373.pdf>`
   

