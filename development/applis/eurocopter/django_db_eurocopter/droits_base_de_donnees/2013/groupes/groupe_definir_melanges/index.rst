
.. index::
   pair: Groupe; Définition des mélanges


.. _groupe_definir_melanges:

================================================
Groupe ayant les droits de définir des mélanges 
================================================

.. seealso::

   - :ref:`groupes_admin_bdd_2013`



.. contents::
   :depth: 3
   

Groupe ayant le droit de définir des mélanges ``groupe_definir_melanges``
==========================================================================   


Un utilisateur dans ce groupe a le droit de définir des mélanges.


.. figure:: groupe_definir_melanges.png
   :align: center
   
   
Utilisateur appartenant au groupe ``groupe_definir_melanges``
--------------------------------------------------------------

.. figure:: admin_melanges.png
   :align: center
   
      
