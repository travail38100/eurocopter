

.. index::
   pair: Groupe; Définition des opérateurs


.. _groupe_definir_operateurs:

=================================================
Groupe ayant les droits de définir des opérateurs  
=================================================

.. seealso::

   - :ref:`groupes_admin_bdd_2013`


.. contents::
   :depth: 3
   

Groupe ayant le droit de définir des opérateurs
=================================================   

.. figure:: admin_operateurs.png
   :align: center
   
   
   
