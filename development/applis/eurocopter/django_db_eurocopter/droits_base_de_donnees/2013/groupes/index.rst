
.. index::
   pair: Groupes; Base de données
   ! Groupes utilisateurs


.. _groupes_admin_bdd_2013:

==============================
Propositions pour les groupes
==============================

.. seealso::

   - :ref:`droits_admin_bdd`

.. contents::
   :depth: 3
   

Description des groupes
========================   


.. figure:: groupes.png
   :align: center


On peut définir 2 groupes:

- :ref:`Groupe <groupe_definir_melanges>` ayant les droits pour définir des mélanges (``groupe_definir_melanges``)
- :ref:`Groupe <groupe_definir_operateurs>` ayant les droits pour définir des opérateurs (``groupe_definir_operateurs``)



Groupes
========

.. toctree::
   :maxdepth: 3
   
   groupe_definir_melanges/index
   groupe_definir_operateurs/index
   
   

 

