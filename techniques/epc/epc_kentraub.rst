

.. index::
   pair: Ken Traub; EPC


.. _epc_kentraub_faq:

===========================================
EPC (Electronic Product Code) KenTraub FAQ
===========================================

.. seealso::

   - http://www.kentraub.net/tools/tagxlate/FAQ.html#userMemory


.. contents::
   :depth: 3


What is "user memory" ?
========================

Gen 2 RFID tags have four different memory banks, each of which holds data for
a different purpose.

The "user memory" bank, also called "MB 11", is intended to hold application data,
other than the unique identifier for the physical object to which the tag is
affixed. (The unique identifier for the physical object to which the tag is
affixed is usually held in a different memory bank.)


What is a "user memory encoder" ?
=================================

A user memory encoder is software that converts application data into a form
suitable for storing into the user memory bank of a Gen 2 RFID tag.


Why is a user memory encoder or decoder needed ? How is what's stored on the tag different from application data ?
==================================================================================================================


Application data usually consists of a number of different pieces of data.

The user memory of a tag is a single string of bits, and so the data stored in
the tag contains extra "framing" information to separate one piece of data from
another and to identify what each piece of data is.

Also, application data is compressed before storing on the tag so that as
few bits as possible are required.

This allows less expensive tags to be used in any given application, and also
improves performance because fewer bits need to be communicated between the RFID
tag and the RFID interrogator (reader).

The user memory encoder knows how to compress the data values and add the extra
"framing" information.

The user memory encoder knows how to parse the "framing" information and uncompress
the data values.


Are there standards for the encoding and decoding of user memory ?
==================================================================

Yes, there are standards that govern how application data is stored in user
memory.

**The standards ensure that when one application writes data to the user
memory of a tag, all applications that read the user memory will get the same
data that was written**.

All user memory encoders and decoders must faithfully implement these standards
in order to ensure interoperability.


What are the standards for the encoding and decoding of user memory ?
=====================================================================

.. seealso:: http://www.iso.org/iso/catalogue_detail.htm?csnumber=30529

The main standard that governs the encoding and decoding of user memory is
`ISO/IEC 15962:2004`_ , which specifies a general purpose framework for encoding
and decoding of RFID tag memory.

The EPC Tag Data Standard Version 1.6 specifies how the ISO/IEC 15962 standard
applies to Gen 2 RFID tags, and also extends ISO/IEC 15962 with an additional
compression and framing mechanism called "packed objects".

Packed Objects offers a number of performance and functional advantages over
the mechanisms available in the 2004 version of ISO/IEC 15962.

Packed Objects is expected to be incorporated into a new version of ISO/IEC 15962,
whose expected publication is some time in 2011.


.. _`ISO/IEC 15962:2004`:  http://www.iso.org/iso/catalogue_detail.htm?csnumber=30529



What is "TID memory" ?
======================

Gen 2 RFID tags have four different memory banks, each of which holds data for
a different purpose.

The "TID" bank, also called "MB 10", holds information about the tag itself,
as opposed to information about the object to which the tag is affixed.


What information is available in TID memory ?
=============================================

At a minimum, TID memory indicates the make and model of the silicon chip that
is the heart of the RFID tag.

- A 12-bit number called the "Mask Designer ID" (MDID) indicates the manufacturer
  (make) of the chip,
- and a second 12-bit number called the "Tag Model Number"  (TMN) indicates the
  model of the chip.

MDID numbers are assigned to manufacturers by EPCglobal, and then each
manufacturer assigns its own TMN values for its own products.


What else is in TID memory ?
=============================

Optionally, TID memory may contain a unique serial number that is programmed
into the RFID tag when the RFID tag is manufactured.

TID memory may also contain additional information about the capabilities of the
RFID tag, such as how much memory it has, which optional commands it supports,
and so on.

The tag manufacturer determines whether either of these additional pieces of
information is programmed into TID memory.

You said that TID memory contains a unique identifier. Is that the same as an Electronic Product Code (EPC) ?
=============================================================================================================

No it is not. The unique TID serial number, if a tag has it at all, identifies
the tag, whereas the EPC identifies the object to which the tag is affixed.

The EPC is generally programmed at the time the tag is physically attached to a
specific object, product, or asset, and is usually selected to be a meaningful
identifier to a software application.

For example, when an RFID tag is affixed to a trade item, the EPC is often
derived from the Global Trade Item Number (GTIN) for that product.

The TID serial number, in contrast, is programmed at the time the tag is
manufactured, before anybody knows to what it will be affixed.

So the TID serial number is not usually as meaningful to an RFID software
application as the EPC is.

If the TID serial number is not as meaningful as an EPC, why is it useful ?
===========================================================================

Because the TID memory is programmed at tag manufacture time, and usually cannot
be changed afterwards, the TID serial number can be a useful way to detect
whether one RFID tag has been replaced by another.

This is sometimes helpful in anti-counterfeiting or similar applications where
tamper detection is important. On the other hand, an adversary with sufficient
resources might be able to make his own tag that has a writable TID, and so the
TID serial number is not a guarantee of authenticity.

What is an STID URI ?
======================

An STID URI (Uniform Resource Identifier) is the unique TID serial number,
converted to a form that is convenient for software applications to process.

The STID URI has a syntax similar to the Electronic Product Code (EPC).

Software applications may use the STID URI as a unique identifier for a tag
without having to worry about the bit-level structure of the TID memory.

What is a "TID decoder" ?
=========================

A TID decoder is software that converts the data stored in the TID memory bank
of a Gen 2 RFID tag into a form that an application can understand and process.


What are the standards for the decoding of TID memory ?
=======================================================

The standard that governs the encoding and decoding of TID memory for Gen 2
RFID tags for EPCglobal applications is the EPC Tag Data Standard Version 1.6.

The TID decoder on this web site fully implements this standard.









