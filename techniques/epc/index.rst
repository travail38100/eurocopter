

.. index::
   pair: Techniques; EPC
   pair: EPC ; Tag Data Standard Version 1.6
   pair: User; Memory
   ! Electronic Product Code


.. _epc:

=======================================
EPC (Electronic Product Code)
=======================================

.. contents::
   :depth: 3


FAQ Kentraub
============


.. toctree::
   :maxdepth: 3

   epc_kentraub



ISO/IEC 15962:2004
===================

.. seealso::

   - http://www.iso.org/iso/catalogue_detail.htm?csnumber=30529

The data protocol used to exchange information in a radio-frequency identification
(RFID) system for item management is specified in ISO/IEC 15961:2004 and in
ISO/IEC 15962:2004.

Both are required for a complete understanding of the data protocol in its
entirety; but each focuses on one particular interface:

- ISO/IEC 15961:2004 addresses the interface with the application system.
- ISO/IEC 15962:2004 deals with the processing of data and its presentation to
  the RF tag, and the initial processing of data captured from the RF tag.

ISO/IEC 15962:2004 focuses on encoding the transfer syntax, as defined in
ISO/IEC 15961:2004 according to the application commands defined in that
International Standard.

The encodation is in a Logical Memory as a software analogue of the physical
memory of the RF tag being addressed by the interrogator.


Caractéristiques de la norme ISO/IEC 15962:2004
------------------------------------------------

- defines the encoded structure of object identifiers;
- specifies the data compaction rules that apply to the encoded data;
- specifies a Precursor for encoding syntax features efficiently;
- specifies formatting rules for the data, e.g. depending on whether a directory
  is used or not;
- defines how application commands, e.g. to lock data, are transferred to the
  Tag Driver;
- defines other communication to the application.


EPC Tag Data Standard Version 1.6
==================================

.. seealso::

   - http://www.gs1.org/gsmp/kc/epcglobal/tds

:download:`Télécharger le document EPC Tag Data Standard Version 1.6 <tds_1_6-RatifiedStd-20110922.pdf>`

This standard defines EPC tag data, including how key GS1 key identifiers are
encoded on the tag and how they are encoded for use in the information systems
layers of the EPC Systems Network.

Version 1.6 further defines a new header for the Aerospace and Defense Identifier
(ADI) scheme. It also adds a new section on what it means to conform to the
GS1 EPC Tag Data Standard.

For a more detailed summary of other changes since TDS 1.5 was published, consult
the beginning of the section in TDS 1.6 titled, “Differences from EPC Tag Data Standard Version 1.5.”


Introduction
------------

The EPC Tag Data Standard defines the Electronic Product Code™, and also
specifies 31 the memory contents of Gen 2 RFID Tags.

Audience for this document
--------------------------

The target audience for this specification includes:

- EPC Middleware vendors
- RFID Tag users and encoders
- Reader vendors
- Application developers
- System integrators





