

.. index::
   pair: Techniques; Bases de données
   pair: Database ; Normalization


.. _bdd_Eurocopter:

=======================================
Techniques bases de données Eurocopter
=======================================

.. seealso::

   - :term:`base de données`
   - http://en.wikipedia.org/wiki/Database_normalization
   - http://fr.wikipedia.org/wiki/Forme_normale_%28bases_de_donn%C3%A9es_relationnelles%29
   - http://laurent-audibert.developpez.com/Cours-BD/html/Cours-BD015.php



Sqlite implementation
=====================

.. toctree::
   :maxdepth: 3
   
   sqlite
