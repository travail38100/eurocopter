

.. index::
   pair: Database ; Sqlite
   pair: ARM; Sqlite


.. _sqlite:

=======================================
Sqlite
=======================================

.. seealso::

   - :term:`base de données`
   - https://www.sqlite.org/
   - http://fr.wikipedia.org/wiki/Sqlite


.. contents::
   :depth: 3
   
   
Description
============

SQLite est une bibliothèque écrite en C qui propose un moteur de base de données 
relationnelle accessible par le langage SQL. 

SQLite implémente en grande partie le standard SQL-92 et des propriétés ACID.



Sqlite .NET
===========

.. seealso::

   - https://github.com/praeclarum/sqlite-net


For ARM architecture
---------------------

.. seealso::

   - http://www.parmaja.com/downloads/sqlite3/
   
   
   
