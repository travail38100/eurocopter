

.. index::
   pair: Book; ProDjango
   pair: ISBN ; 1-4302-0331-5

.. _prodjango:

===============================
ProDjango
===============================


.. seealso::

   - http://prodjango.com

.. contents::
   :depth: 3

Références
==========


:ISBN13: 978-1-4302-1047-4
:User Level: Beginner to Advanced
:Publication Date: December 17, 2008


Introduction
============

Thank you for your interest in Pro Django by Marty Alchin, a new book from Apress,
scheduled for release on December 18, 2008.

It promises to be a guide through the lesser-known, under-documented, incredibly
useful aspects of the Django Web framework you probably never knew existed.


Who’s it for ?
==============

I’ll be honest, Pro Django isn’t for the faint of heart.
It’s not for people who are new to Django, and especially not those who are new
to Python.

There are plenty of other resources out there to help you get started with Python
and Django, so if that’s what you need, might I suggest Dive Into Python and
The Definitive Guide to Django.

Intermediate users may also be more interested in James Bennett’s Practical
Django Projects, which provides a more real-world introduction to working Django
from idea through implementation.

Unlike these other books, Pro Django, is for those developers who’ve reached what
they perceive the be the limits of Django, and are looking ways to push beyond,
into applications that might seem unsuitable for such a framework.

If you’re working with Django currently and have run up against a situation you
can’t find a way out of, or if you’ve considered Django, but aren’t sure if
it’s up the challenge of an upcoming project, this book is for you.


