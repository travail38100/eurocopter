

.. index::
   pair: Django; Definitive Guide
   pair: Book; The Definitive Guide
   pair: ISBN ; 1-4302-0331-5

.. _definitive_guide:

===============================
The definitive guide to Django
===============================


.. seealso::

   - http://www.amazon.fr/The-Definitive-Guide-Django-Development/dp/143021936X

Références
==========


:Broché: 536 pages
:Editeur: APress; Édition 2008
:Langue: Anglais
:ISBN-10 (electronic): 1-4302-0331-5



Biographie de l'auteur
======================

Adrian Holovaty, a web developer/journalist, is one of the creators and core
developers of Django.

He works at washingtonpost.com, where he builds database web applications and
does "journalism as computer programming".

Previously, he was lead developer for World Online in Lawrence, Kansas, where
Django was created.

When not working on Django improvements, Adrian hacks on side projects for
the public good, such as chicagocrime.org, which won the 2005 Batten Award for
Innovations in Journalism.

He lives in Chicago and maintains a weblog at http://www.Holovaty.com.

