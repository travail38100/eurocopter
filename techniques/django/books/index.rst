

.. index::
   pair: Django; Books

.. _django_books:

=============
Django books
=============



.. toctree::
   :maxdepth: 3

   definitive_guide/index
   prodjango/index
   the_django_book/index
   two_scoops/index


