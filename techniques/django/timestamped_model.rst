

.. index::
   pair: Time; Stamp
   pair: TimeStamped ; Model




.. _timestamp_model:

================
TimeStampedModel
================

.. seealso::

   - http://code.google.com/p/django-command-extensions/source/browse/trunk/django_extensions/db/models.py?r=164
   - http://code.google.com/p/django-command-extensions/source/browse/trunk/django_extensions/db/models.py?r=164
   - https://docs.djangoproject.com/en/dev/topics/db/models/#model-inheritance


Source
=======

Le livre :ref:`Django 2 scoops <two_scoops>` Model inheritance in practice the TimedstampModel, p49.


It’s very common in Django projects to include a created and modified timestamp
$eld on all your models. We could manually add those $elds to each and every model, but
that's a lot of work and adds the risk of human error. A better solution is to write a


::

    # Code taken with permission from Carl Meyer's
    # very useful django-model-utils

    from django.db import models
    from django.utils.timezone import now
    from django.utils.translation import ugettext_lazy as _


    class AutoCreatedField(models.DateTimeField):
        """
        A DateTimeField that automatically populates itself at
        object creation.
        By default, sets editable=False, default=now
        """
        def __init__(self, *args, **kwargs):
            kwargs.setdefault('editable', False)
            kwargs.setdefault('default', now)
            super(AutoCreatedField, self).__init__(*args, **kwargs)



    class AutoLastModifiedField(AutoCreatedField):
        """
        A DateTimeField that updates itself on each save() of
        the model.
        By default, sets editable=False and default=now.
        """
        def pre_save(self, model_instance, add):
        value = now()
        setattr(model_instance, self.attname, value)
        return value


    class TimeStampedModel(models.Model):
        """
        An abstract base class model that provides selfupdating
        ``created`` and ``modified`` fields.
        """
        created = AutoCreatedField(_('created'))
        modified = AutoLastModifiedField(_('modified'))

        class Meta:
            abstract = True

By designing TimeStampedModel as an abstract base class when we define a new class
that inherits from it, Django doesn’t create a model_utils.time_stamped_model table
when syncdb is run.

Let’s put it to the test::

    # flavors/models.py
    from django.db import models
    from model_utils import TimeStampedModel

    class Flavor(TimeStampedModel):
        title = models.CharField(max_length=200)


This only creates one table: the flavors_flavor database table. that's exactly the
behavior we wanted.


Further reading:

- https://docs.djangoproject.com/en/dev/topics/db/models/#model-inheritance
