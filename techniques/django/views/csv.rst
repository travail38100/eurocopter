

.. index::
   pair: Django; Views
   pair: Django; Import/Export

.. _django_outputting:

========================================================
Outputting CSV, xls, json and other formats with Django
========================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/howto/outputting-csv/
   - https://github.com/django/django/blob/master/docs/howto/outputting-csv.txt


.. contents::
   :depth: 3

Introduction
=============


This document explains how to output CSV (Comma Separated Values) dynamically
using Django views.

To do this, you can either use the Python CSV library or the Django template system.


django-import-export  package
=============================

.. seealso::

   - :ref:`django_import_export`

