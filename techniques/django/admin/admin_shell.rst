

.. index::
   pair: Django; Read only


.. _django_admin_shell:

=====================================
Django admin shell
=====================================

.. contents::
   :depth: 3




.. seealso::

   - https://docs.djangoproject.com/en/1.4/intro/tutorial01/


Playing with the API
====================

Now, let’s hop into the interactive Python shell and play around with the free
API Django gives you. To invoke the Python shell, use this command::

    python django-admin.py shell --settings=conf.settings.local


Example 1
----------

- from apps.historiques.models import CHistoriqueTagMelange
- from django.utils import timezone
- all = CHistoriqueTagMelange.objects.all()
- p81 = all[1]
- date_fabrication = p81.date_fabrication
- current_tz = timezone.get_current_timezone()
- date_locale = current_tz.normalize(self.date_fabrication.astimezone(current_tz))


Example 1
----------

- from apps.historiques.admin import CHistoriqueTagMelangeResouce
- dataset = CHistoriqueTagMelangeResouce().export()
- print dataset.csv

::

    In [3]: from apps.historiques.admin import CHistoriqueTagMelangeResouce

    In [4]: dataset = CHistoriqueTagMelangeResouce().export()

    In [5]: dataset
    Out[5]: <dataset object>

    In [6]: print dataset.csv
    id,date_fabrication,minutes_date_fabrication,temps_de_murissement,duree_de_vie,c
    ode_vacation,numero_melange_vacation,nom_melange,viscosite,temperature,matricule
    _operateur,delta_ouverture_1,delta_ouverture_2,delta_ouverture_3,delta_ouverture
    _4,delta_ouverture_5,delta_ouverture_6,delta_ouverture_7,delta_ouverture_8,delta
    _ouverture_9,delta_ouverture_10,lmp,traite
    2,2013-02-07 14:22:40,500,50,900,2,4,P34,9,32,MATRICULE3,0,0,0,0,0,0,0,0,0,0,0,0

    3,2013-02-13 16:32:31,1052,20,500,2,3,P81,20,25,OPERATEUR1,0,0,0,0,0,0,0,0,0,0,0
    ,0
