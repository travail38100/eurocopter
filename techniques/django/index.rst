

.. index::
   pair: Django ; Framework
   ! Django

.. _django:

=========
Django
=========


.. seealso::

   - http://en.wikipedia.org/wiki/Django_%28web_framework%29
   - https://docs.djangoproject.com/en/1.5/intro/tutorial01/
   - https://docs.djangoproject.com
   - https://docs.djangoproject.com/en/dev/


.. contents::
   :depth: 3


Introduction
=============

Django est un framework python permettant l'administration de bases de
données.


Django code
===============

.. seealso::

   - https://github.com/django/django


Django doc
==========

.. seealso::

   - https://github.com/django/django/tree/master/docs


Mise en oeuvre
==============

.. toctree::
   :maxdepth: 3

   install/index
   admin
   admin_doc
   models/models
   orm/index
   customize_admin
   i18n/index
   multi_db
   timestamped_model
   admin_validation
   admin_readonly
   admin/index
   views/index
   packages/index


Infos Django
==============

.. toctree::
   :maxdepth: 3

   books/index


