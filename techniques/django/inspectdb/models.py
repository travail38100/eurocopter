# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.

from django.db import models

class Cbacmelange(models.Model):
    id = models.IntegerField(primary_key=True)
    date_fabrication = models.DateTimeField(null=True, blank=True)
    minutes_date_fabrication = models.IntegerField(null=True, blank=True)
    delta_date_murissement = models.IntegerField(null=True, blank=True)
    delta_date_peremption = models.IntegerField(null=True, blank=True)
    code_vacation = models.IntegerField(null=True, blank=True)
    numero_melange_vacation = models.IntegerField(null=True, blank=True)
    nom_melange = models.TextField(blank=True) # This field type is a guess.
    viscosite = models.IntegerField(null=True, blank=True)
    temperature = models.IntegerField(null=True, blank=True)
    matricule_operateur = models.TextField(blank=True) # This field type is a guess.
    delta_ouverture_1 = models.IntegerField(null=True, blank=True)
    delta_ouverture_2 = models.IntegerField(null=True, blank=True)
    delta_ouverture_3 = models.IntegerField(null=True, blank=True)
    delta_ouverture_4 = models.IntegerField(null=True, blank=True)
    delta_ouverture_5 = models.IntegerField(null=True, blank=True)
    delta_ouverture_6 = models.IntegerField(null=True, blank=True)
    delta_ouverture_7 = models.IntegerField(null=True, blank=True)
    delta_ouverture_8 = models.IntegerField(null=True, blank=True)
    delta_ouverture_9 = models.IntegerField(null=True, blank=True)
    delta_ouverture_10 = models.IntegerField(null=True, blank=True)
    lmp = models.IntegerField(null=True, blank=True)
    traite = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'CBacMelange'

class Ccomposantmelange(models.Model):
    id = models.IntegerField(primary_key=True)
    code_ecs = models.TextField(blank=True) # This field type is a guess.
    id_type_composant = models.IntegerField(null=True, blank=True)
    description = models.TextField(blank=True) # This field type is a guess.
    class Meta:
        db_table = u'CComposantMelange'

class Clivraisonpots(models.Model):
    id = models.IntegerField(primary_key=True)
    nom_produit = models.CharField(max_length=15, blank=True)
    reference_produit = models.CharField(max_length=20, blank=True)
    fournisseur = models.CharField(max_length=15, blank=True)
    numero_lot = models.CharField(max_length=30, blank=True)
    teinte = models.CharField(max_length=15, blank=True)
    quantite_produit = models.TextField(blank=True) # This field type is a guess.
    unite_quantite = models.CharField(max_length=2, blank=True)
    code_ecs = models.CharField(max_length=6, blank=True)
    date_fabrication = models.DateTimeField(null=True, blank=True)
    date_peremption = models.DateTimeField(null=True, blank=True)
    date_livraison = models.DateTimeField(null=True, blank=True)
    date_ouverture_pot = models.DateTimeField(null=True, blank=True)
    nb_jours_peremption_pot_ouvert = models.IntegerField(null=True, blank=True)
    nb_total_pots = models.IntegerField(null=True, blank=True)
    nb_pots_traites = models.IntegerField(null=True, blank=True)
    temperature_stockage_min = models.TextField(blank=True) # This field type is a guess.
    temperature_stockage_max = models.TextField(blank=True) # This field type is a guess.
    lmp = models.IntegerField(null=True, blank=True)
    livraison_traitee = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'CLivraisonPots'

class Cmelangeautorise(models.Model):
    id = models.IntegerField(primary_key=True)
    nom_melange = models.TextField(blank=True) # This field type is a guess.
    temps_murissement = models.IntegerField(null=True, blank=True)
    temps_peremption = models.IntegerField(null=True, blank=True)
    nb_composants = models.IntegerField(null=True, blank=True)
    liste_id_composants = models.TextField(blank=True) # This field type is a guess.
    class Meta:
        db_table = u'CMelangeAutorise'

class Coperateur(models.Model):
    id = models.IntegerField(primary_key=True, db_column=u'Id') # Field name made lowercase.
    nom = models.TextField(db_column=u'Nom', blank=True) # Field name made lowercase. This field type is a guess.
    prenom = models.TextField(db_column=u'Prenom', blank=True) # Field name made lowercase. This field type is a guess.
    matricule = models.TextField(db_column=u'Matricule', blank=True) # Field name made lowercase. This field type is a guess.
    class Meta:
        db_table = u'COperateur'

class Ctypecomposant(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.TextField(blank=True) # This field type is a guess.
    class Meta:
        db_table = u'CTypeComposant'

class Ctypevacation(models.Model):
    id = models.IntegerField(primary_key=True)
    libelle = models.TextField(blank=True) # This field type is a guess.
    code = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'CTypeVacation'

class Cvacationcourante(models.Model):
    id = models.IntegerField(primary_key=True)
    type_vacation_id = models.IntegerField(null=True, blank=True)
    compteur_melange = models.IntegerField(null=True, blank=True)
    date_dernier_melange = models.DateTimeField(null=True, blank=True)
    class Meta:
        db_table = u'CVacationCourante'

