

.. index::
   pair: Django; Admin


.. _django_admin:

=====================================
Django admin
=====================================

.. seealso::

   - https://docs.djangoproject.com/en/1.4/intro/tutorial02/
   - http://www.elao.com/blog/python/django-2nde-partie-le-modele-et-ladmin.html
   - http://www.djangobook.com/en/2.0/chapter06.html


.. contents::
   :depth: 3

Introduction
============


The Django admin site is not activated by default – it’s an opt-in thing.

To activate the admin site for your installation, do these three things:

- Uncomment "django.contrib.admin" in the INSTALLED_APPS setting.
- Run python manage.py syncdb. Since you have added a new application to
  INSTALLED_APPS, the database tables need to be updated.
- Edit your mysite/urls.py file and uncomment the lines that reference the admin
  there are three lines in total to uncomment. This file is a URLconf; we’ll dig
  into URLconfs in the next tutorial. For now, all you need to know is that it
  maps URL roots to applications. In the end, you should have a urls.py file
  that looks like this


Start the development server
============================

.. seealso::

   - http://127.0.0.1:8000/admin/

Let’s start the development server and explore the admin site.

Recall from Tutorial 1 that you start the development server like so::

    python manage.py runserver

Now, open a Web browser and go to "/admin/" on your local domain -- e.g.,

- http://127.0.0.1:8000/admin/. You should see the admin's login screen:


Make the 'admintables' app modifiable in the admin
===================================================

But where's our 'admintables' app? It's not displayed on the admin index page.

Just one thing to do: We need to tell the admin that Poll objects have an admin
interface.

admin.py
--------

To do this, create a file called admin.py in your 'admintables' directory, and edit it
to look like this::

    from django.contrib import admin


    from admintables.models import Cbacmelange
    class CbacmelangeAdmin(admin.ModelAdmin):
        pass

    admin.site.register(Cbacmelange, CbacmelangeAdmin)

    from admintables.models import Ctypecomposant
    from admintables.models import Ccomposantmelange


    admin.site.register(Ccomposantmelange)
    admin.site.register(Ctypecomposant)


You'll need to restart the development server to see your changes.

Normally, the server auto-reloads code every time you modify a file, but the
action of creating a new file doesn't trigger the auto-reloading logic.


Relationships, Many to many
===========================

.. seealso::

   - https://docs.djangoproject.com/en/dev/topics/db/models/#relationships
   - https://docs.djangoproject.com/en/dev/topics/db/examples/many_to_many/
   - https://docs.djangoproject.com/en/dev/ref/contrib/admin/#django.contrib.admin.InlineModelAdmin


Book, Author examples
=====================

.. seealso::

   - http://stackoverflow.com/questions/8048449/django-adding-related-books-to-the-author


list_filter
===========

.. seealso::

   - http://www.djangobook.com/en/2.0/chapter06.html


::


    # CHistoriqueTagMelange
    from .models import CHistoriqueTagMelange
    class CHistoriqueTagMelangeAdmin(admin.ModelAdmin):
        fieldsets = [
                (None, {'fields': ['nom_melange', ('code_vacation', 'numero_melange_vacation'), 'matricule_operateur']}),
                (u'Date de Fabrication', {'fields': [('date_fabrication', 'minutes_date_fabrication')]}),
                (u'Dates de Mûrissement et péremption', {'fields': [('delta_date_murissement' , 'delta_date_peremption')]}),
                (u'Caractéristiques', {'fields': [('viscosite', 'temperature')]}),
                (None, {'fields': ['lmp']}),
                (u'Dates d''ouverture', {'fields': [('delta_ouverture_1', 'delta_ouverture_2' , 'delta_ouverture_3', 'delta_ouverture_4', 'delta_ouverture_5')
                                                  ,(('delta_ouverture_6'), 'delta_ouverture_7', 'delta_ouverture_8', 'delta_ouverture_9', 'delta_ouverture_10')
                                   ]}),
            ]
        list_display = ('nom_melange', 'date_fabrication', 'minutes_date_fabrication', 'matricule_operateur', 'delta_ouverture_1')
        search_fields = ('nom_melange', 'date_fabrication')
        list_filter = ('date_fabrication',)

