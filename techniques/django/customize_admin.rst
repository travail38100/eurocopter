

.. index::
   pair: Customize ; Admin


.. _customize_admin:

=====================================
Customize admin
=====================================



Overview
=========

Voir page 93, customizing the Admin's interface Look and Feel, du livre
:ref:`The Definitive Guide to Django <definitive_guide>`


Fichier base_site.html
=======================

Le fichier original se trouve sous le répertoire:

- C:\Python27\Lib\site-packages\django\contrib\admin\templates\admin


Pour utiliser le nouveau fichier:


::

    {% extends "admin/base.html" %}
    {% load i18n %}

    {% block title %}{{ title }} | {% trans 'Administration de la base Eurocopter' %}{% endblock %}

    {% block branding %}
    <h1 id="site-name">{% trans 'Administration de la base Eurocopter' %}</h1>
    {% endblock %}

    {% block nav-global %}{% endblock %}


Il faut paramétrer le fichier base.py::


    # Absolute filesystem path to the templates root:
    TEMPLATES_ROOT = normpath(join(SITE_ROOT, 'templates'))

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
    TEMPLATE_DIRS = (TEMPLATES_ROOT,
                     normpath(join(TEMPLATES_ROOT, 'admin')),



Le fichier base.py se trouve sous le répertoire:

- bd_eurocopter_root\eurocopter_project\eurocopter_conf\settings

