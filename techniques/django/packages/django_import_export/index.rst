
.. index::
   pair: Import; Export
   pair: Export   ; JSON
   pair: Export   ; Excel
   pair: Export   ; HTML
   pair: Export   ; ODS
   pair: Export   ; CSV
   pair: Export   ; YAML
   pair: Export   ; TSV
   ! Django import export


.. _django_import_export:

=============================
Django import-export package
=============================

.. seealso::

   - https://www.djangopackages.com/packages/p/django-import-export/
   - http://source.mihelac.org/


.. contents::
   :depth: 3

Introduction
=============


``django-import-export`` is a Django application and library for importing
and exporting data with included admin integration.

Features
--------

- support multiple formats (Excel, CSV, JSON, ...
  and everything else that `tablib`_ support)

- admin integration for importing

- preview import changes

- admin integration for exporting

- export data **respecting admin filters**


.. _`tablib`: https://github.com/kennethreitz/tablib


Code
====

.. seealso::

   - https://github.com/bmihelac/django-import-export



Documentation
=============

.. seealso::

   - https://django-import-export.readthedocs.org/en/latest/


Tablib
======

.. seealso::

   - https://github.com/kennethreitz/tablib



