
.. index::
   pair: Django; Packages


.. _django_packages:

=====================================
Django packages installation
=====================================

.. seealso:: http://www.djangopackages.com/


.. toctree::
   :maxdepth: 3

   django_import_export/index

