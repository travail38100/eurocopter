

.. index::
   pair: Freeze; Distributions


.. _install_freeze_distributions:


============================================================================
Installation des versions utilisées pendant le développement chez le client
============================================================================


.. seealso::

   - :ref:`installation_de_django`
