


.. _django_distribution_installation:

=====================================
Installation des distributions python
=====================================


Installation du framework Django
================================

::

    (eurocopter) C:\Users\pvergain>pip install django
    
::

    
    Downloading/unpacking django
    Downloading Django-1.5.1.tar.gz (8.0MB): 8.0MB downloaded
    Running setup.py egg_info for package django

    warning: no previously-included files matching '__pycache__' found under directory '*'
    warning: no previously-included files matching '*.py[co]' found under directory '*'
    Installing collected packages: django
    Running setup.py install for django

    warning: no previously-included files matching '__pycache__' found under directory '*'
    warning: no previously-included files matching '*.py[co]' found under directory '*'
    Successfully installed django
    Cleaning up...


Installation du framework Sphinx 
=================================

Description
-----------

Utilities for general- and special-purpose documentation, including
autodocumentation of Python modules.

Includes reStructuredText, the easy to read, easy to use, what-you-see-is-what-you-get
plaintext markup language.


::

    (eurocopter) C:\Users\pvergain>pip install sphinx
    
    
::
    
    Downloading/unpacking sphinx
    Downloading Sphinx-1.2b1.tar.gz (3.0MB): 3.0MB downloaded
    Running setup.py egg_info for package sphinx

    warning: no files found matching 'README'
    no previously-included directories found matching 'doc\_build'
    Downloading/unpacking Pygments>=1.2 (from sphinx)
    Downloading Pygments-1.6.tar.gz (1.4MB): 1.4MB downloaded
    Running setup.py egg_info for package Pygments

    Downloading/unpacking Jinja2>=2.3 (from sphinx)
    Downloading Jinja2-2.7.tar.gz (377kB): 377kB downloaded
    Running setup.py egg_info for package Jinja2

    warning: no files found matching '*' under directory 'custom_fixers'
    warning: no previously-included files matching '*' found under directory 'docs\_build'
    warning: no previously-included files matching '*.pyc' found under directory 'jinja2'
    warning: no previously-included files matching '*.pyc' found under directory 'docs'
    warning: no previously-included files matching '*.pyo' found under directory 'jinja2'
    warning: no previously-included files matching '*.pyo' found under directory 'docs'
    Downloading/unpacking docutils>=0.7 (from sphinx)
    Downloading docutils-0.10.tar.gz (1.6MB): 1.6MB downloaded
    Running setup.py egg_info for package docutils

    warning: no files found matching 'MANIFEST'
    warning: no files found matching '*' under directory 'extras'
    warning: no previously-included files matching '.cvsignore' found under directory '*'
    warning: no previously-included files matching '*.pyc' found under directory '*'
    warning: no previously-included files matching '*~' found under directory '*'
    warning: no previously-included files matching '.DS_Store' found under directory '*'
    Downloading/unpacking markupsafe (from Jinja2>=2.3->sphinx)
    Downloading MarkupSafe-0.18.tar.gz
    Running setup.py egg_info for package markupsafe

    Installing collected packages: sphinx, Pygments, Jinja2, docutils, markupsafe
    Running setup.py install for sphinx

    warning: no files found matching 'README'
    no previously-included directories found matching 'doc\_build'
    Installing sphinx-apidoc-script.py script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Installing sphinx-apidoc.exe script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Installing sphinx-apidoc.exe.manifest script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Installing sphinx-build-script.py script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Installing sphinx-build.exe script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Installing sphinx-build.exe.manifest script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Installing sphinx-quickstart-script.py script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Installing sphinx-quickstart.exe script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Installing sphinx-quickstart.exe.manifest script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Installing sphinx-autogen-script.py script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Installing sphinx-autogen.exe script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Installing sphinx-autogen.exe.manifest script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Running setup.py install for Pygments

    Installing pygmentize-script.py script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Installing pygmentize.exe script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Installing pygmentize.exe.manifest script to C:\Users\pvergain\Envs\eurocopter\Scripts
    Running setup.py install for Jinja2

    warning: no files found matching '*' under directory 'custom_fixers'
    warning: no previously-included files matching '*' found under directory 'docs\_build'
    warning: no previously-included files matching '*.pyc' found under directory 'jinja2'
    warning: no previously-included files matching '*.pyc' found under directory 'docs'
    warning: no previously-included files matching '*.pyo' found under directory 'jinja2'
    warning: no previously-included files matching '*.pyo' found under directory 'docs'
    Running setup.py install for docutils

    warning: no files found matching 'MANIFEST'
    warning: no files found matching '*' under directory 'extras'
    warning: no previously-included files matching '.cvsignore' found under directory '*'
    warning: no previously-included files matching '*.pyc' found under directory '*'
    warning: no previously-included files matching '*~' found under directory '*'
    warning: no previously-included files matching '.DS_Store' found under directory '*'
    Running setup.py install for markupsafe

    building 'markupsafe._speedups' extension
    C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\BIN\cl.exe /c /nologo /Ox /MD /W3 /GS- /DNDEBUG -IC:\Python27_32\include -IC:\Users\pvergain\Envs\eurocopter\PC /Tcmarkupsafe/_speedups.c /Fobuild\temp.win32-2.7\Release\markupsafe/_speedups.obj
    _speedups.c
    C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\BIN\link.exe /DLL /nologo /INCREMENTAL:NO /LIBPATH:C:\Python27_32\Libs /LIBPATH:C:\Users\pvergain\Envs\eurocopter\libs /LIBPATH:C:\Users\pvergain\Envs\eurocopter\PCbuild /EXPORT:init_speedups build\temp.win32-2.7\Release\markupsafe/_speedups.obj /OUT:build\lib.win32-2.7\markupsafe\_speedups.pyd /IMPLIB:build\temp.win32-2.7\Release\markupsafe\_speedups.lib /MANIFESTFILE:build\temp.win32-2.7\Release\markupsafe\_speedups.pyd.manifest
    Creating library build\temp.win32-2.7\Release\markupsafe\_speedups.lib and object build\temp.win32-2.7\Release\markupsafe\_speedups.exp
    Successfully installed sphinx Pygments Jinja2 docutils markupsafe
    Cleaning up...



Installation du package django-debug-toolbar
=============================================

::


    (eurocopter) C:\Users\pvergain>pip install django-debug-toolbar

::

    Downloading/unpacking django-debug-toolbar
    Downloading django-debug-toolbar-0.9.4.tar.gz (150kB): 150kB downloaded
    Running setup.py egg_info for package django-debug-toolbar

    no previously-included directories found matching 'example'
    Installing collected packages: django-debug-toolbar
    Running setup.py install for django-debug-toolbar

    no previously-included directories found matching 'example'
    Successfully installed django-debug-toolbar
    Cleaning up...


Installation du package South
==============================

.. seealso::

   - http://south.aeracode.org/
   - hg clone https://bitbucket.org/andrewgodwin/south
   - https://bitbucket.org/andrewgodwin/south/


This is South, a Django application to provide schema and data migrations.

Documentation on South is currently available on our project site;
you can find it at http://south.aeracode.org/docs/

South is compatable with Django 1.2 and higher, and Python 2.4 and higher.

::

    (eurocopter) C:\Users\pvergain>pip install south
    
::
    
    Downloading/unpacking south
    Downloading South-0.8.1.tar.gz (94kB): 94kB downloaded
    Running setup.py egg_info for package south

    Installing collected packages: south
    Running setup.py install for south

    Successfully installed south
    Cleaning up...
    
   
Installation du package Unipath
================================
    
::
    
    (eurocopter) C:\Users\pvergain>pip install unipath
    
::
    
    Downloading/unpacking unipath
    Downloading Unipath-1.0.tar.gz
    Running setup.py egg_info for package unipath

    Installing collected packages: unipath
    Running setup.py install for unipath

    Successfully installed unipath
    Cleaning up...   
     

Installation du package django-import-export
============================================
    
::
    
    (eurocopter) C:\Users\pvergain> pip install django-import-export

::

    (eurocopter) C:\Users\pvergain> pip install django-import-export
    Downloading/unpacking django-import-export
    Downloading django-import-export-0.1.3.tar.gz
    Running setup.py egg_info for package django-import-export

    Downloading/unpacking tablib (from django-import-export)
    Downloading tablib-0.9.11.tar.gz (571kB): 571kB downloaded
    Running setup.py egg_info for package tablib

    Requirement already satisfied (use --upgrade to upgrade): Django>=1.4.2 in c:\users\pvergain\envs\eurocopter\lib\site-packages (from django-import-export)
    Downloading/unpacking diff-match-patch (from django-import-export)
    Downloading diff-match-patch-20120106.tar.gz (54kB): 54kB downloaded
    Running setup.py egg_info for package diff-match-patch

    Installing collected packages: django-import-export, tablib, diff-match-patch
    Running setup.py install for django-import-export

    Running setup.py install for tablib

    Running setup.py install for diff-match-patch

    Successfully installed django-import-export tablib diff-match-patch
    Cleaning up...


Installation du package pytz
================================
   
::
   
    (eurocopter) C:\Users\pvergain>pip install pytz
    
::
    
    Downloading/unpacking pytz
    Downloading pytz-2013b.tar.gz (331kB): 331kB downloaded
    Running setup.py egg_info for package pytz

    warning: no files found matching '*.pot' under directory 'pytz'
    warning: no previously-included files found matching 'test_zdump.py'
    Installing collected packages: pytz
    Running setup.py install for pytz

    warning: no files found matching '*.pot' under directory 'pytz'
    warning: no previously-included files found matching 'test_zdump.py'
    Successfully installed pytz
    Cleaning up...
    


