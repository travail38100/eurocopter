
.. index::
   pair: Python; Django
   
   
.. _django_installation:

=====================================
Installation des packages python
=====================================


.. seealso::

   - :ref:`installation_python`
   

.. contents::
   :depth: 3


Installation dans un environnement virtuel
===========================================

L'installation des packages python se fait dans un environnement virtuel.


mkvirtualenv eurocopter
-----------------------


::

    mkvirtualenv eurocopter
    
::
    
    New python executable in eurocopter\Scripts\python.exe
    Installing setuptools................done.
    Installing pip...................done.



Installation des distributions au moment du développement
==========================================================

.. toctree::
   :maxdepth: 3
   
   devel_distributions/index


Installation des versions de développement chez le client
==========================================================

.. toctree::
   :maxdepth: 3
   
   client_distributions/index

    
django-admin.py versus manage.py
================================

.. seealso::

   - https://docs.djangoproject.com/en/1.5/ref/django-admin/

As described in the official Django documentation, you should use django-admin.py
rather than manage.py when working with multiple settings.



