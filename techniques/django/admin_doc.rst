

.. index::
   pair: Django ; Admin doc


.. _django_admin_docs:

=====================================
Django admin doc
=====================================

.. seealso::

   - https://docs.djangoproject.com/en/1.5/ref/contrib/admin/admindocs/
   - http://docutils.sourceforge.net/


Fichier settings.py du projet
=============================

::

    INSTALLED_APPS = (
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.sites',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.admindocs',
        # Uncomment the next line to enable the admin:
        'django.contrib.admin',
        'admintables'
    )















