

.. index::
   pair: Django ; Translations
   pair: Local time zone ; Europe/Paris
   pair: Language code ; fr-FR


.. _django_translations:

=====================================
Django translations
=====================================

.. seealso::

   - https://docs.djangoproject.com/en/1.5/topics/i18n/translation/
   - https://github.com/django/django/blob/master/docs/topics/i18n/translation.txt


Fichier settings.py du projet
=============================

::

    # Local time zone for this installation. Choices can be found here:
    # http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
    # although not all choices may be available on all operating systems.
    # In a Windows environment this must be set to your system time zone.
    TIME_ZONE = 'Europe/Paris'

    # Language code for this installation. All choices can be found here:
    # http://www.i18nguy.com/unicode/language-identifiers.html
    LANGUAGE_CODE = 'fr-FR'


Local time zone : Europe/Paris
-------------------------------

.. seealso::

   - http://en.wikipedia.org/wiki/Europe/Paris

Europe/Paris is a time zone identifier from zone file of the IANA time zone
database. The data is as follows::

    FR
    +4852+00220
    Europe/Paris


Language code: fr-FR
---------------------

.. seealso::

   - http://www.i18nguy.com/unicode/language-identifiers.html













