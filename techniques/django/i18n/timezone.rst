

.. index::
   pair: Django ; Timezone
   pair: Django ; i18n
   pair: Django ; UTC


.. _django_timezone:

=====================================
Django i18n timezone
=====================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/topics/i18n/timezones/
   - https://github.com/django/django/blob/master/docs/topics/i18n/timezones.txt


Overview
=========

When support for time zones is enabled, Django stores date and time information
in UTC in the database, uses time-zone-aware datetime objects internally, and
translates them to the end user’s time zone in templates and forms.

This is handy if your users live in more than one time zone and you want to
display date and time information according to each user’s wall clock.

**Even if your Web site is available in only one time zone, it’s still good practice
to store data in UTC in your database**.

One main reason is Daylight Saving Time (DST). Many countries have a system of DST,
where clocks are moved forward in spring and backward in autumn.

If you’re working in local time, you’re likely to encounter errors twice a year,
when the transitions happen. (The pytz documentation discusses these issues in
greater detail.)

This probably doesn’t matter for your blog, but it’s a problem if you over-bill
or under-bill your customers by one hour, twice a year, every year.

The solution to this problem is to use UTC in the code and use local time only
when interacting with end users.

Time zone support is disabled by default. To enable it, set USE_TZ = True in
your settings file.

Installing pytz is highly recommended, but not mandatory.
It’s as simple as:













