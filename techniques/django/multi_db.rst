

.. index::
   pair: Django ; Multi-db


.. _django_multibd:

=====================================
Django multi_db
=====================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/topics/db/multi-db/


Overview
=========

This topic guide describes Django’s support for interacting with multiple databases.

Most of the rest of Django’s documentation assumes you are interacting with a
single database.

If you want to interact with multiple databases, you’ll need to take some
additional steps.

Defining your databases
=======================

The first step to using more than one database with Django is to tell Django
about the database servers you’ll be using.

This is done using the DATABASES setting. This setting maps database aliases,
which are a way to refer to a specific database throughout Django, to a dictionary
of settings for that specific connection.

The settings in the inner dictionaries are described fully in the DATABASES documentation.

Databases can have any alias you choose. However, the alias default has special
significance.

Django uses the database with the alias of default when no other database has
been selected. If you don’t have a default database, you need to be careful to
always specify the database that you want to use.

The following is an example settings.py snippet defining two databases – a default
PostgreSQL database and a MySQL database called users::

    DATABASES = {
        'default': {
            'NAME': 'app_data',
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'USER': 'postgres_user',
            'PASSWORD': 's3krit'
        },
        'users': {
            'NAME': 'user_data',
            'ENGINE': 'django.db.backends.mysql',
            'USER': 'mysql_user',
            'PASSWORD': 'priv4te'
        }
    }















