

.. index::
   pair: Django; Models
   pair: Django; sqlite3
   pair: Django; DB_Eurocopter.sqlite



.. _django_models:

=====================================
Django models
=====================================


.. seealso::

   - https://docs.djangoproject.com/en/1.4/


.. contents::
   :depth: 3


Mise en place du projet django
==============================

.. seealso::

   - https://docs.djangoproject.com/en/1.4/intro/tutorial01/

- python C:\Python27\Scripts\django-admin.py  startproject eurocopter


- python manage.py  runserver


Paramétrage de la base sqlite3 , legacy databases
==================================================

.. seealso::

   - https://docs.djangoproject.com/en/1.4/howto/legacy-databases/
   - https://docs.djangoproject.com/en/1.4/ref/settings/#std:setting-DATABASES


Dans le fichier settings.py
---------------------------



::


    C:\DOCUMENTS AND SETTINGS\PVERGAIN\APPLICATION DATA\EUROCOPTER
    |   .hgignore
    |   CERTIFICAT_CONFORMITE_test_RFID.xls
    |   DB_Eurocopter.sqlite
    |   DB_Paban.sqlite
    |   Eurocopter.ini
    |
    +---Excel
    |       CERTIFICAT_CONFORMITE_test_RFID.trt
    |       test_2_sheet_2_lines.trt
    |       test_ref_produit_2_bytes.trt
    |       test_temperatures_negatives.trt
    |
    \---Logs


Dans le fichier :file:`settings.py` on donne les informations concernant la base
de données employée:

- le type de la base est: ``sqlite3``.
- le nom complet de la base est: file:``C:/Documents and Settings/pvergain/Application Data/Eurocopter/DB_Eurocopter.sqlite``


::


    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': 'C:/Documents and Settings/pvergain/Application Data/Eurocopter/DB_Eurocopter.sqlite',  # Or path to database file if using sqlite3.
        }
    }



Production du fichier models.py
--------------------------------

A partir di fichier settings.py qui contient le pointeur sur la base de données
on peut produire le fichier models.py qui contient les classes repésentant
les tables de la base de données::


    python manage.py inspectdb  > models.py
    ou
    inspectdb.bat



Le fichier :file:`models.py` est produit sous le répertoire courant::


    C:\PROJECTS_ID3\P2X238_EUROCOPTER\CONCEPT\SOFT\PC\XLOG2Y057_IHMS_PABAN_EUROCOPTER\0.1.0\PROJECTS\BD_EUROCOPTER\EUROCOPTER
    |   create_sql_admin_tables.bat
    |   inspectdb.bat
    |   manage.py
    |   manage_shell.bat
    |   models.py
    |   README.rst
    |   run_server.bat
    |   startapp.bat
    |   syncdb.bat
    |   validate.bat
    |
    \---eurocopter
            settings.py
            urls.py
            wsgi.py
            __init__.py


This feature is meant as a shortcut, not as definitive model generation.
See the documentation of inspectdb for more information.

Once you've cleaned up your models, name the file models.py and put it in the
Python package that holds your app.

Then add the app to your INSTALLED_APPS_ setting.
Install the core Django tables

Next, run the syncdb command to install any extra needed database records such
as admin permissions and content types:

.. _INSTALLED_APPS:  https://docs.djangoproject.com/en/1.4/ref/settings/#std:setting-INSTALLED_APPS


::

    INSTALLED_APPS = (
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.sites',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        # Uncomment the next line to enable the admin:
        'django.contrib.admin',
        'admintables',
    )



Creating models
================

.. seealso::

   - https://docs.djangoproject.com/en/dev/topics/db/models/


Now that your environment -- a "project" -- is set up, you're set to start
doing work.

Each application you write in Django consists of a Python package, somewhere
on your Python path, that follows a certain convention.

Django comes with a utility that automatically generates the basic directory
structure of an app, so you can focus on writing code rather than creating
directories.

Projects vs. apps
-----------------

What's the difference between a project and an app ?

An app is a Web application that does something -- e.g., a Weblog system, a
database of public records or a simple poll app.

A project is a collection of configuration and apps for a particular Web site.

A project can contain multiple apps. An app can be in multiple projects.

Your apps can live anywhere on your Python path. In this tutorial, we'll create
our poll app right next to your manage.py file so that it can be imported as
its own top-level module, rather than a submodule of mysite.

To create your app, make sure you're in the same directory as manage.py and type
this command::

    python manage.py startapp admintables


That'll create a directory :file:`admintables`, which is laid out like this::

    +---admintables
    |       admin.py
    |       models.py
    |       tests.py
    |       views.py
    |       __init__.py



