

.. index::
   pair: Django; Validation
   pair: Django; Overriding the save() method


.. _django_validation:

=====================================
Django validation
=====================================

.. contents::
   :depth: 3

Overriding the save() method
============================

.. seealso::

   - http://stackoverflow.com/questions/4574367/django-overriding-save-method-in-model
   - https://docs.djangoproject.com/en/dev/topics/db/models/#overriding-model-methods


While overriding the specific model's save() method, Is it possible determine
whether it's a new record or an update ?

if self.pk is None it is a new record

::


    def save(self):
        if self.pk is None:
            self.created = datetime.today()
        self.modified = datetime.today()
        super(ProjectCost, self).save()


Validating objects
==================

.. seealso::

   - https://docs.djangoproject.com/en/1.2/ref/models/instances/#validating-objects



Model.clean()
-------------

This method should be used to provide custom model validation, and to modify
attributes on your model if desired.

For instance, you could use it to automatically provide a value for a field, or
to do validation that requires access to more than a single field::

    def clean(self):
        from django.core.exceptions import ValidationError
        # Don't allow draft entries to have a pub_date.
        if self.status == 'draft' and self.pub_date is not None:
            raise ValidationError('Draft entries may not have a publication date.')
        # Set the pub_date for published items if it hasn't been set already.
        if self.status == 'published' and self.pub_date is None:
            self.pub_date = datetime.datetime.now()


Any ValidationError exceptions raised by Model.clean() will be stored in a special
key error dictionary key, NON_FIELD_ERRORS, that is used for errors that are
tied to the entire model instead of to a specific field::

    from django.core.exceptions import ValidationError, NON_FIELD_ERRORS
    try:
        article.full_clean()
    except ValidationError as e:
        non_field_errors = e.message_dict[NON_FIELD_ERRORS]

Finally, full_clean() will check any unique constraints on your model.

