

.. index::
   pair: Fournisseur; Paban
   pair: Entreprise; Peinture

.. _paban:
.. _entreprise_paban:

===============================
L'entreprise Paban SAS
===============================

.. figure:: paban_logo.png
   :align: right


.. seealso::

   - http://www.paban.com/
   - :ref:`specs_application_paban`


.. contents::
   :depth: 3


Description Paban
==================

.. figure:: paban.jpg
   :align: center


Créée le 17.12.1973, la société PABAN SAS distribue des produits et des prestations
pour le secteur de la carrosserie, de l’aéronautique, de l’automobile ou de
l’industrie, en matière de :

- Pentures et produits annexes
- Elaboration de teintes
- Equipements
- Services et conseils associés

Sur toute la région Provence Alpes Côte d’Azur sauf les Alpes Maritimes.

PABAN accompagne aussi ses clients sur les différents aspects de leur métier,
que ce soit :

- Installation, SAV et maintenance matériels (sous-traités)
- Colorimétrie
- Appui technique peinture
- Formations peinture agréées GNFA (groupement national de formation pour
  l’automobile)
- Formations pluridisciplinaires, gestion atelier
- Conseil opérationnel : conception d’ateliers de carrosserie, gestion informatique...


Mais PABAN est aussi une PME qui a pour atouts :

- L’expérience et le savoir-faire de ses équipes,
- La puissance de la centrale d’achats : STAREXCEL,
- Un réseau de carrossiers PRECISIUM,
- Des stocks en magasin et une logistique des plus opérationnelle,
- Un laboratoire de colorimétrie intégré,
- Un centre de formation intégré avec des formateurs certifiés par le GNFA,
- Des services adaptés : conseil, assistance peinture et matériels, maintenance.


Adresse Paban
=============

::

    ZAC Les Feuillantines
    Allée de la Rouguière
    13011 Marseille
    Téléphone: 04 91 35 73 57
    Fax: 04 91 35 73 58
    E-mail : paban@paban.com





