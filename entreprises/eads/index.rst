
.. index::
   pair: Entreprises; Airbus Group
   ! Airbus Group


.. _eads:
.. _airbus_group:

========================================================================
Airbus Group (ex European Aeronautic Defence and Space Company (EADS))
========================================================================


.. seealso::

   - http://fr.wikipedia.org/wiki/EADS
   - http://fr.wikipedia.org/wiki/Airbus
   - http://www.airbus-group.com/airbusgroup/int/en.html


.. contents::
   :depth: 3


Introduction
=============


.. seealso::

   - http://fr.wikipedia.org/wiki/European_Aeronautic_Defence_and_Space_Company

European Aeronautic Defence and Space company (EADS) est un groupe industriel 
européen présent sur le secteur aéronautique et spatial civil et militaire. 

Le 30 juillet 2013, il est annoncé un changement du nom du groupe pour prendre 
celui de sa filiale la plus importante, Airbus_ et bénéficier de la popularité 
de sa marque vedette, mondialement connue. 

Ce changement accompagné d'une restructuration des filiales interviendra le 
1er juillet 2014.

Il est l'un des premiers groupes de défense en Europe ; le septième mondial en
2008 d'après le Sipri_ (Stockholm International Peace Research Institute).


.. _Sipri:  http://fr.wikipedia.org/wiki/Stockholm_International_Peace_Research_Institute
.. _Airbus: http://fr.wikipedia.org/wiki/Airbus




Divisions et filiales
=====================

En 2012, EADS est organisée en quatre divisions et neuf unités opérationelles.


Airbus Helicopters
-------------------

.. toctree::
   :maxdepth: 3

   airbus_helicopters/index


