
.. index::
   pair: Entreprises; Airbus Helicopters (ex Eurocopter)
   ! Eurocopter


.. _eurocopter:
.. _airbus_helicopters:


==============================================================
Airbus Helicopters (ex ``Eurocopter``)
==============================================================


.. seealso::

   - http://fr.wikipedia.org/wiki/Eurocopter
   - http://www.eurocopter.com/site/en/ref/home.html
   - http://www.airbushelicopters.com/site/en/ref/home.html
   - http://fr.wikipedia.org/wiki/Eurocopter
   - http://fr.wikipedia.org/wiki/Lutz_Bertling



.. figure:: ah_2014.png
   :align: center

   Site web d'Eurocopter


.. contents::
   :depth: 3
   
   
Description
===========

Le Groupe Eurocopter est le premier fabricant d'hélicoptères civil au monde.

Il a été créé en 1992 à partir de la fusion des divisions hélicoptères du
français Aérospatiale et de l'allemand DaimlerChrysler Aerospace AG (DASA).

:Chiffre d’affaires:  5,4 milliards d'€ (2011)



Logos
=====


.. toctree::
   :maxdepth: 3

   2014/index   
   2010/index


Projets avec Id3 Technologies
=============================

.. seealso::

   - :ref:`pdev2x238_eurocopter`


Secteurs
=========

.. toctree::
   :maxdepth: 3

   secteurs/index






