
.. index::
   pair: Aline; Barbera
   pair: Téléphone ; Aline BARBERA (04.42.85.72.62)


.. _aline_barbera:

=================================================
Aline BARBERA (04.42.85.72.62)
=================================================


.. figure:: ref_aline_barbera.png
   :align: center


.. contents::
   :depth: 3

Contact Eurocopter
==================

Laboratoire Matériaux et Procédés - EDDLD

:Adresse courriel:   Aline.Barbera@eurocopter.com
:Tel: 04.42.85.72.62


Projet id3-Eurocopter
=====================


- :ref:`pdev2x238_eurocopter`
