
.. index::
   pair: Laure; Pujol

.. _laure_pujol:

=================================================
Laure Pujol
=================================================


.. contents::
   :depth: 3

Contact Eurocopter
==================

Laboratoire Matériaux et Procédés - EDDLD

:Adresse courriel:   Laure.Pujol@eurocopter.com


Projet id3-Eurocopter
=====================


- :ref:`pdev2x238_eurocopter`
