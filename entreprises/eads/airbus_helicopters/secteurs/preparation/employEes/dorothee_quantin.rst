
.. index::
   pair: Dorothée; Quantin

.. _dorothee_quantin:

=================================================
Dorothée Quantin
=================================================



.. contents::
   :depth: 3

Contact Eurocopter
==================

Laboratoire Matériaux et Procédés - EDDLD

:Adresse courriel:   Dorothee.Quantin@eurocopter.com
:Tel: 04.42.85.72.62


Projet id3-Eurocopter
=====================


- :ref:`pdev2x238_eurocopter`
