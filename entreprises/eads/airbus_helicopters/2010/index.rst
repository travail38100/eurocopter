

.. _logos_2010:

============
Logos 2010
============


.. contents::
   :depth: 3
   
   
Avant le 17 septembre 2010
===========================

Logo Eurocopter avant le 17 septembre 2010

.. figure:: Logo_Eurocopter_avant_17_septembre_2010.JPG
   :align: center

   Logo Eurocopter avant le 17 septembre 2010


Après le 17 septembre 2010
==========================

.. warning:: Le nouveau logo ne semble pas adopté, puisque c'est l'ancien logo
   qui est en première page du site http://www.eurocopter.com/site/en/ref/home.html

.. figure:: 120px-Eurocopter_apres_17_septembre_2010.png
   :align: center

   Logo Eurocopter après le 17 septembre 2010





